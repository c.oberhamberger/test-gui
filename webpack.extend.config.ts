import type { Configuration } from 'webpack';

export const node = (currentConfig: Configuration): Configuration => {
    //@ts-ignore
    currentConfig.output.libraryTarget = 'commonjs2';
    return currentConfig;
};
