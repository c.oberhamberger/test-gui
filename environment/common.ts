/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { DefaultMemWatchActivation, defineCommonConfig } from '@archibald/core';

import { Language, Platform, Tenant } from 'shop/client/constants/config';

import type { ProjectConfig } from 'shop/client/interfaces/base/config';

export default defineCommonConfig<ProjectConfig>({
    general: {
        useSecureCookies: true,
        memwatch: {
            active: DefaultMemWatchActivation.UNINITIALIZED,
            print: ''
        }
    },
    app: {
        // Api Config
        api: {
            // api schema
            schema: '/{base}/{version}/{url}',

            // api hostname/port
            protocol: '{{API_PROTOCOL|null}}',
            host: '{{API_HOST|null}}',
            port: '{{API_PORT|null}}',
            version: 'v2',

            // Api base url
            base: 'jsapi',
            // use the nodejs for request from the nodejs
            useMiddleware: '{{USE_MIDDLEWARE|true}}',
            // Global retry configuration
            retry: {
                times: 1,
                delay: 100
            }
        },
        // should language be fixed or not
        fixLanguage: true,
        // name of the language param in <Route> pattern ('/:language?')
        languageParamName: 'language',
        // available levels: error|warn|info|debug|trace
        logLevel: 'debug',
        // available languages in shop (used on both server side and frontend)
        supportedLanguages: [Language.DE],
        // default language for shop (used for seo hreflang link attribute)
        defaultLanguage: Language.DE,
        // the canonicalBaseUrl for the shop - to make it configurable different to current target
        canonicalBaseUrl: 'http://localhost:3100',
        route: {
            prefix: '/'
        }
    },

    // tenant specific overrides to global config
    tenants: {
        [Platform.SHOP]: {
            [Tenant.NETCONOMY_NET]: {
                app: {}
            },
        },
    },
    hybris: {
        // API Config
        api: {
            // hybris hostname/port
            schema: '/{base}/{version}/{baseSite}/{url}',
            // should be same for all environments
            protocol: '{{HYBRIS_PROTOCOL|https}}',
            host: '{{HYBRIS_HOST|localhost}}',
            port: '{{HYBRIS_PORT|9001}}',
            version: 'v2',
            // REST base url, should be same for all environments
            base: 'rest',
            // mock the requests directly on the jsapi level
            mocked: true,
            // Global retry configuration
            retry: {
                times: 1,
                delay: 100
            }
        },
        oauth: {
            client_id: 'admin_static',
            client_secret: 'nimda'
        }
    },

    server: {
        host: '{{SERVER_HOST|null}}',
        port: '{{SERVER_PORT|null}}',
        connection: {
            keepAliveTimeout: '{{API_KEEPALIVE_TIMEOUT|4000}}',
            keepAliveMaxTimeout: '{{API_KEEPALIVE_MAX_TIMEOUT|30000}}',
            pipelining: '{{API_KEEPALIVE_CONCURRENT_REQUESTS|1}}',
            rejectUnauthorized: true
        },
        credentials: {
            token: {
                secret: '{{TOKEN_SECRET|zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI}}'
            }
        }
    }
    });
