/*
* The Initial Developer of the content of this file is NETCONOMY.
* All portions of the code written by NETCONOMY are property of
* NETCONOMY. All Rights Reserved.
*
* NETCONOMY Software & Consulting GmbH
* Hilmgasse 4, A-8010 Graz (Austria)
* FN 204360 f, Landesgericht fuer ZRS Graz
* Tel: +43 (316) 815 544
* Fax: +43 (316) 815544-99
* www.netconomy.net
*
* (c) 2019 by NETCONOMY Software & Consulting GmbH
*/

import { defineConfig } from '@archibald/core';

import { Platform, Tenant, Environment } from 'shop/client/constants/config';

import type { ProjectConfig } from 'shop/client/interfaces/base/config';

export default defineConfig<ProjectConfig>({
    app: {
        environment: Environment.LOCAL,
        api: {
            host: '{{PUBLIC_IP|null}}'
        },
        image: {
            protocol: '{{PROTOCOL|http}}',
            host: '{{PUBLIC_IP|null}}',
            port: '{{SERVER_PORT|3100}}'
        }
        },

    // tenant specific overrides to global config
    tenants: {
        [Platform.SHOP]: {
            [Tenant.NETCONOMY_NET]: {
                app: {}
            },
        },
    },

    server: {
        connection: {
            rejectUnauthorized: false
        }
    }
});
