/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

import { join } from 'path';
import { rm, mkdir, writeFile, readdir, link, stat } from 'fs/promises';
import { copy } from 'fs-extra';

// constants
const WORK_DIR = process.cwd();
const OUT_DIR = join(WORK_DIR, 'dist');
const VERCEL_OUT_DIR = join(WORK_DIR, '.vercel', 'output');

// recursively copy static files using hard links
async function copyStatic(src, dest) {
  const files = await readdir(src);
  for (const file of files) {
    const child = join(src, file);
    const s = await stat(child);
    if (s.isDirectory()) {
      await mkdir(join(dest, file));
      await copyStatic(child, join(dest, file));
    } else {
      await link(join(src, file), join(dest, file));
    }
  }
}

// turn it into a vercel-specific build
const vercelBuildStart = Date.now();
await rm(VERCEL_OUT_DIR, { recursive: true, force: true });
await mkdir(join(VERCEL_OUT_DIR, 'functions', 'index.func'), {
  recursive: true,
});
await mkdir(join(VERCEL_OUT_DIR, 'static/static'), { recursive: true });


// save config
await writeFile(
  join(VERCEL_OUT_DIR, 'config.json'),
  JSON.stringify({ version: 3 }, null, 2)
);


const targetDirectory = join(VERCEL_OUT_DIR, 'functions', 'index.func', 'dist');

await copy(OUT_DIR, targetDirectory);

// create function
await writeFile(
  join(VERCEL_OUT_DIR, 'functions', 'index.func', '.vc-config.json'),
  JSON.stringify(
    {
      runtime: 'nodejs20.x',
      handler: 'dist/server/index.js',
      launcherType: 'Nodejs',
      shouldAddHelpers: true,
    },
    null,
    2
  )
);

await copyStatic(
  join(OUT_DIR, 'client'),
  join(VERCEL_OUT_DIR, 'static', 'static')
);

console.log(
  '✓ Transformed into Vercel build output', Date.now() - vercelBuildStart
);

