/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useFetch } from '@archibald/client';
import { selectCmsTicketId } from '@archibald/storefront';

import { useSelector } from 'react-redux';

import { actionGetSmarteditPreviewTicket } from 'shop/client/actions';

export function usePreviewTicket() {
    const cmsTicketId = useSelector(selectCmsTicketId);

    const { data } = useFetch<{ redirectUrl: string }>(['SMART_EDIT_PREVIEW', cmsTicketId], () => {
        if (!cmsTicketId) {
            return null;
        }

        return actionGetSmarteditPreviewTicket(cmsTicketId);
    });

    return data?.redirectUrl ?? null;
}
