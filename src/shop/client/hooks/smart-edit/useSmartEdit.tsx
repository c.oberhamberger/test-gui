/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useRequest, useSSR } from '@archibald/core';
import { actionSetCmsTicketId, CMSParam, selectCmsTicketId } from '@archibald/storefront';

import { useDispatch, useSelector } from 'react-redux';

import { getAttributes, SmartEditComponentOrSlot } from 'shop/client/components/support/smart-edit/wrapper/SmartEditWrapper';

export function useSmartEdit() {
    const request = useRequest();
    const { isServer } = useSSR();
    const dispatch = useDispatch();
    const cmsTicketId = useSelector(selectCmsTicketId);

    function getDomContractAttributes(props: SmartEditComponentOrSlot, page) {
        return getAttributes(props, page, cmsTicketId);
    }

    function getCMSTicketId() {
        const cmsTicketIdParam = request?.query?.[CMSParam.CMS_TICKET_ID];
        if (Array.isArray(cmsTicketIdParam) && cmsTicketIdParam.length) {
            return cmsTicketIdParam[0];
        }

        if (typeof cmsTicketIdParam === 'string') {
            return cmsTicketIdParam;
        }

        return null;
    }

    function setCMSTicketId() {
        const cmsTicketId = getCMSTicketId();
        if (cmsTicketId) {
            dispatch(actionSetCmsTicketId(cmsTicketId));
        }
    }

    if (isServer) {
        if (!cmsTicketId) {
            setCMSTicketId();
        }
    }

    return {
        getDomContractAttributes,
        setCMSTicketId,
        getCMSTicketId,
        cmsTicketId
    };
}
