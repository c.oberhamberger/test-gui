/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useFetch } from '@archibald/client';
import { useLocation } from '@archibald/core';

import { actionGetProducts } from 'shop/client/actions';

export function useProductsByCategory(categoryId: string, facets = false) {
    const { search } = useLocation() ?? {};
    const searchParams = new URLSearchParams(search);
    const sort = facets ? searchParams.get('sort') ?? '' : '';

    return useFetch([{ key: 'products-by-category', group: 'category' }, categoryId, sort], () => actionGetProducts(categoryId, sort), {
        enabled: !!categoryId,
        enableOnlyWhenAllKeysTruthy: false
    });
}
