/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useLocation, useNavigate } from '@archibald/core';
import { useTranslate } from '@archibald/storefront';

import type { DropdownOption } from 'shop/client/interfaces/base';

export function useProductSorting() {
    const translate = useTranslate();
    const navigate = useNavigate();
    const { search, pathname } = useLocation();

    const sortingOptions: DropdownOption[] = [
        { label: translate('category.sortby.relevance'), value: 'relevance' },
        { label: translate('category.sortby.nameasc'), value: 'name:asc' },
        { label: translate('category.sortby.namedesc'), value: 'name:desc' },
        { label: translate('category.sortby.priceasc'), value: 'price:asc' },
        { label: translate('category.sortby.pricedesc'), value: 'price:desc' }
    ];
    const currentParams = new URLSearchParams(search);
    const currentSorting = currentParams.get('sort') || sortingOptions[0].value;

    function changeSorting(value: string) {
        value === 'relevance' ? currentParams.delete('sort') : currentParams.set('sort', value);
        navigate(`${pathname}?${currentParams.toString()}`);
    }

    return {
        currentSorting,
        changeSorting,
        sortingOptions
    };
}
