/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useFetch } from '@archibald/client';
import { MESSAGES_KEY, useLanguage } from '@archibald/core';

import { actionGetMessages } from 'shop/client/actions';

export function useMessages() {
    const language = useLanguage();
    return useFetch([MESSAGES_KEY, language], () => actionGetMessages(), { refetchAfterHydrate: true });
}
