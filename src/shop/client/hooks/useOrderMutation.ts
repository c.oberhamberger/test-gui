/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useMutate } from '@archibald/client';

import { actionPlaceOrder } from 'shop/client/actions';

import { useCartId } from 'shop/client/hooks/cart';

export function useOrderMutation() {
    const { cartId } = useCartId();
    const { mutate } = useMutate(['order', cartId], { suspense: false });

    async function placeOrder() {
        await mutate(() => actionPlaceOrder(cartId));
    }

    return {
        placeOrder
    };
}
