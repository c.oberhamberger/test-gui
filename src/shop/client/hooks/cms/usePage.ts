/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useFetch } from '@archibald/client';
import { selectCmsTicketId, useCMSContext } from '@archibald/storefront';

import { useSelector } from 'react-redux';

import type { PageLabel } from 'shop/client/constants';

import { actionGetCMSPage } from 'shop/client/actions';

import type { CMSContextInterface } from 'shop/client/interfaces/base';

import { useLayout } from 'shop/client/hooks/cms/useLayout';

export function usePageFetch(pageLabelOrId: PageLabel | string) {
    const cmsTicketId = useSelector(selectCmsTicketId);
    return useFetch(['cms-page', pageLabelOrId], () => actionGetCMSPage({ pageLabel: pageLabelOrId, cmsTicketId }));
}

export function usePage() {
    const layoutPage = useLayout();
    const cmsPage = useCMSContext<CMSContextInterface>();
    return cmsPage.page ?? layoutPage;
}

export function usePageLoading() {
    const cmsPage = useCMSContext<CMSContextInterface>();
    return cmsPage.loading;
}

export function usePageError() {
    const cmsPage = useCMSContext<CMSContextInterface>();
    return cmsPage.error;
}

export function usePageErrors() {
    const cmsPage = useCMSContext<CMSContextInterface>();
    return cmsPage.errors;
}
