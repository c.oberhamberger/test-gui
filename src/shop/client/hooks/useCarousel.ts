/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { EmblaOptionsType } from 'embla-carousel';
import useEmblaCarousel from 'embla-carousel-react';
import { useCallback, useEffect, useState } from 'react';

interface CarouselOptions {
    useArrows?: boolean;
    useDots?: boolean;
}

export function useCarousel(options: EmblaOptionsType & CarouselOptions) {
    const { useArrows = false, useDots = false, ...emblaOptions } = options ?? {};
    const [ref, api] = useEmblaCarousel(emblaOptions);

    const [isPreviousButtonDisabled, setIsPreviousButtonDisabled] = useState<boolean>(true);
    const [isNextButtonDisabled, setIsNextButtonDisabled] = useState<boolean>(true);
    const [selectedSlideIndex, setSelectedSlideIndex] = useState<number>(0);

    const scrollPrevious = useCallback(() => api && api.scrollPrev(), [api]);
    const scrollNext = useCallback(() => api && api.scrollNext(), [api]);
    const scrollTo = useCallback((index: number) => api && api.scrollTo(index), [api]);

    const onSelect = useCallback(() => {
        if (api) {
            if (useDots) {
                setSelectedSlideIndex(api.selectedScrollSnap());
            }
            if (useArrows) {
                setIsPreviousButtonDisabled(!api.canScrollPrev());
                setIsNextButtonDisabled(!api.canScrollNext());
            }
        }
    }, [api, useArrows, useDots]);

    useEffect(() => {
        if (api) {
            onSelect();
            api.on('select', onSelect);
        }
    }, [api, onSelect]);

    return {
        api,
        isNextButtonDisabled,
        isPreviousButtonDisabled,
        ref,
        scrollNext,
        scrollPrevious,
        scrollTo,
        selectedSlideIndex,
        setSelectedSlideIndex
    };
}
