/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ANONYMOUS_USER_ID } from '@archibald/auth';
import { useMutate } from '@archibald/client';
import { DefaultAddress } from '@archibald/storefront';

import { actionAddToCart, actionCreateDeliveryAddress, actionRemoveFromCart } from 'shop/client/actions';

import type { Cart, CartEntry, Product } from 'shop/client/interfaces/base';

import { useCartId } from 'shop/client/hooks/cart/useCartId';

export function useCartMutation(suspense = false) {
    const { cartId, setCartId, clearCartId } = useCartId();
    const { mutate } = useMutate<Cart>(['cart', cartId], {
        after: async ({ response }) => {
            if (response?.totalItems === 0) {
                await clearCartId();
            } else {
                await setCartId(response?.user.uid === ANONYMOUS_USER_ID ? response?.guid : response?.code);
            }
        },
        suspense
    });

    async function removeFromCart(entry: CartEntry) {
        if (cartId) {
            return await mutate(() => actionRemoveFromCart(cartId, entry));
        }
        return null;
    }

    async function addToCart(product: Product, quantity = 1) {
        return await mutate(() => actionAddToCart(cartId!, product, quantity));
    }

    async function createDeliveryAddress(address: DefaultAddress) {
        return await mutate(() => actionCreateDeliveryAddress(cartId, address));
    }

    return {
        addToCart,
        removeFromCart,
        createDeliveryAddress
    };
}
