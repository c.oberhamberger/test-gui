/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Bahnhofgürtel 77-79 / 7, A-8020 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useCookie } from '@archibald/server';

import { CommonConstants } from 'shop/client/constants';

export function useCartId() {
    const [cartId, set, clear] = useCookie(CommonConstants.CART_ID);

    async function setCartId(cartId: string | undefined) {
        set(cartId);
    }

    async function clearCartId() {
        clear();
    }

    return {
        cartId,
        setCartId,
        clearCartId
    };
}
