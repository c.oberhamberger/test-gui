/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

export enum ErrorType {
    INFO = 'INFO',
    ERROR = 'ERROR',
    ABORTED = 'ABORTED',
    CONNECTION_ABORTED = 'ECONNABORTED',
    CONNECTION_REFUSED = 'ECONNREFUSED',
    CART_ERROR = 'CartError',
    CART_ENTRY_ERROR = 'CartEntryError',
    TOKEN_ERROR = 'InvalidTokenError'
}

export enum ErrorReason {
    NOT_FOUND = 'notFound'
}

export const NOT_FOUND_REDIRECT_URL = '/404';
