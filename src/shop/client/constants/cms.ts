/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
export const enum CMSPosition {
    TOP_HEADER = 'TopHeader',
    HEADER = 'Header',
    BOTTOM_HEADER = 'BottomHeader',
    TOP_FOOTER = 'TopFooter',
    FOOTER = 'Footer',
    BOTTOM_FOOTER = 'BottomFooter',
    CONTENT = 'Content',
    TOP_CONTENT = 'TopContent',
    LEFT_CONTENT = 'LeftContent',
    RIGHT_CONTENT = 'RightContent',
    BOTTOM_CONTENT = 'BottomContent',
    NAVIGATION = 'Navigation'
}

export const enum CMSComponentTypeCode {
    IMAGE = 'CMSImageComponent',
    HEADER = 'CMSHeaderComponent',
    LINK = 'CMSLinkComponent',
    FOOTER = 'CMSFooterComponent',
    PROMO_BAR = 'CMSPromoBarComponent',
    TEXT = 'CMSTextComponent',
    TEXT_IMAGE = 'CMSTextImageComponent',
    BANNERS = 'CMSBannersComponent',
    PRODUCT_CAROUSEL = 'CMSProductCarouselComponent',
    PRODUCT_DETAIL = 'CMSProductDetailComponent',
    PRODUCT_LISTING = 'CMSProductListingComponent',
    CART = 'CMSCartComponent',
    CHECKOUT = 'CMSCheckoutComponent',
    SEARCH_RESULT = 'CMSSearchResultComponent'
}

export const enum CMSTemplate {
    LANDING_PAGE = 'LandingPageTemplate',
    CONTENT_PAGE = 'ContentPageTemplate',
    NOT_FOUND_PAGE = 'NotFoundPageTemplate',
    FATAL_ERROR_PAGE = 'FatalErrorPageTemplate',
    TWO_COLUMNS_PAGE = 'Content2ColumnsPageTemplate',
    PRODUCT_DETAIL_PAGE = 'ProductDetailPageTemplate',
    PRODUCT_LISTING_PAGE = 'ProductListingPageTemplate',
    CART_PAGE = 'CartPageTemplate',
    CHECKOUT_PAGE = 'CheckoutPageTemplate',
    THANKYOU_PAGE = 'ThankYouPageTemplate',
    SEARCH_PAGE = 'SearchPageTemplate',
    HEADER = 'Header',
    FOOTER = 'Footer'
}

export const enum CMSParam {
    CREATE = 'create',
    EDIT = 'edit',
    CMS_TICKET_ID = 'cmsTicketId',
    LANGUAGE = 'lang'
}

export const enum PageType {
    CATEGORY = 'category',
    CONTENT = 'content'
}

export const enum PageLabel {
    FRONTPAGE = 'home-page',
    NOT_FOUND = 'not-found-page',
    FATAL_ERROR = 'fatal-error-page',
    PRODUCT_DETAIL = 'product-details-page',
    PRODUCT_LISTING_PAGE = 'product-listing-page',
    CART_PAGE = 'cart-page',
    CHECKOUT_PAGE = 'checkout-page',
    THANKYOU_PAGE = 'thankyou-page',
    SEARCH_PAGE = 'search-page'
}
