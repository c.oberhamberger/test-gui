/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DefaultActionLocal } from '@archibald/core';

import { combineReducers } from 'redux';

import { CustomActionTypes } from '../constants';

export interface ExampleState {
    data: boolean;
}

function data(state = false, action: DefaultActionLocal) {
    switch (action.type) {
        case CustomActionTypes.SET_EXAMPLE_ON:
            return true;
        case CustomActionTypes.SET_EXAMPLE_OFF:
            return false;
        default:
            return state;
    }
}

const exampleReducer = combineReducers({
    data
});
export default exampleReducer;
