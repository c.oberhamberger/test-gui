/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CustomActionTypes } from 'shop/client/constants';

import exampleReducer, { ExampleState } from '../example';

const initialState: ExampleState = {
    data: false
};

describe('example reducer', () => {
    it(`should set data in example reducer with action type ${CustomActionTypes.SET_EXAMPLE_ON}`, () => {
        const result = exampleReducer(initialState, { type: CustomActionTypes.SET_EXAMPLE_ON });
        expect(result.data).toEqual(true);
    });

    it(`should set data in example reducer with action type ${CustomActionTypes.SET_EXAMPLE_OFF}`, () => {
        const result = exampleReducer(initialState, { type: CustomActionTypes.SET_EXAMPLE_OFF });
        expect(result.data).toEqual(false);
    });

    it('should do nothing in example reducer with action type null', () => {
        const result = exampleReducer({ data: true }, { type: null });
        expect(result.data).toEqual(true);
    });

    it('should toggle data in example reducer', () => {
        let result = exampleReducer(initialState, { type: CustomActionTypes.SET_EXAMPLE_OFF });
        expect(result.data).toEqual(false);
        result = exampleReducer(initialState, { type: CustomActionTypes.SET_EXAMPLE_ON });
        expect(result.data).toEqual(true);
        result = exampleReducer(initialState, { type: CustomActionTypes.SET_EXAMPLE_OFF });
        expect(result.data).toEqual(false);
    });
});
