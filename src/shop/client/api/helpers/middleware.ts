/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import {
    createMiddleware,
    actionSetCSRF,
    actionSetHttpStatus,
    DefaultResponse,
    HeadersHelper,
    HttpCode,
    HttpHeader,
    Logger,
    selectLastHttpStatus
} from '@archibald/core';

/**
 * This method return true or false if an error is part of the response
 * @param response - error object
 * @return true / false
 */
function hasError(response: DefaultResponse) {
    return response?.status >= HttpCode.CODE_400;
}

export const errorMiddleware = createMiddleware((response, store) => {
    if (hasError(response)) {
        const currentStatusCode = selectLastHttpStatus(store?.getState());
        const status = response?.status ?? 0;
        if (status >= HttpCode.CODE_400 && status > currentStatusCode) {
            store?.dispatch(actionSetHttpStatus(status));
        }
    }
});

// Update store after response is received
export const storeMiddleware = createMiddleware((response, store) => {
    if (HeadersHelper.areHeaders(response?.headers)) {
        const csrfToken = response.headers.get(HttpHeader.CSRF);

        if (csrfToken) {
            Logger.debug(`*** Store Middleware: CSRF token ${csrfToken} is set`);

            store?.dispatch(actionSetCSRF(csrfToken));
        }
    }
});
