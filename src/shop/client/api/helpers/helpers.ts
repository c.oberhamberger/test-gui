/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type DefaultApiConfig, selectCsrfToken, selectLanguage, createAPIOptions, HttpMimeType, RequestHelper } from '@archibald/core';
import { getCookies, getHeaders } from '@archibald/server';
import { selectCmsTicketId } from '@archibald/storefront';

import type { Store } from 'redux';

import { CMSParam, HttpHeader } from 'shop/client/constants';

/**
 * Returns request headers tied to specific template
 *
 * @param {DefaultApiConfig} config
 * @param {Store} store
 */
export async function getRequestHeaders(config: DefaultApiConfig, store?: Store) {
    const headers = new Headers();

    const state = store?.getState();
    const language = selectLanguage(state);
    const csrf = selectCsrfToken(state);

    headers.set(HttpHeader.CSRF, csrf);
    headers.set(HttpHeader.CONTENT_TYPE, HttpMimeType.APPLICATION_JSON);
    headers.set(HttpHeader.ACCEPT_LANGUAGE, language);

    if (process.env.APP_CODE === 'server') {
        const cookies = getCookies();
        const contextHeaders = getHeaders();
        const userAgent = contextHeaders.get(HttpHeader.USER_AGENT);
        headers.set(HttpHeader.HOST, config?.host ?? '0.0.0.0');
        headers.set(HttpHeader.ACCEPT_ENCODING, 'identity');
        headers.set(HttpHeader.CONNECTION, 'keep-alive');
        headers.set(HttpHeader.COOKIE, RequestHelper.getCookieString(cookies?.getAll()));
        headers.set(HttpHeader.USER_AGENT, `${userAgent || 'EMPTY'}--NodeJS`);
    }

    return headers;
}

/**
 * Returns API default options like headers, URL search params etc.
 *
 * @param {DefaultApiConfig} config
 * @returns {boolean}
 */
export const getAPIDefaultOptions = createAPIOptions(async (config, store) => {
    const state = store?.getState();
    const cmsTicketId = selectCmsTicketId(state);
    const language = selectLanguage(state);
    const params = new URLSearchParams({ [CMSParam.CMS_TICKET_ID]: cmsTicketId ?? '', [CMSParam.LANGUAGE]: language });
    const headers = await getRequestHeaders(config, store);

    return {
        headers,
        params
    };
});
