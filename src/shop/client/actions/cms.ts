/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ApiTarget } from '@archibald/core';
import { CMSPageRequestExtras, PageType } from '@archibald/storefront';

import { createEntity } from 'shop/client/api';

import type { CMSPage, SmarteditPreviewTicket } from 'shop/client/interfaces/base';

export function actionGetCMSPage(params: CMSPageRequestExtras = {}) {
    const { pageType = PageType.CONTENT, pageLabel = null, code = null, fields = 'FULL' } = params;
    return createEntity<CMSPage>({
        target: ApiTarget.API,
        url: 'cms/pages',
        params: { pageLabelOrId: pageLabel, code, pageType, fields }
    });
}

export function actionGetSmarteditPreviewTicket(cmsTicketId: string) {
    return createEntity<SmarteditPreviewTicket>({
        target: ApiTarget.API,
        url: `previewTicket/${cmsTicketId}`
    });
}
