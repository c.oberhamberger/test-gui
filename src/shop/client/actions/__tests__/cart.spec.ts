/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpMethod } from '@archibald/core';
import { DefaultAddress } from '@archibald/storefront';

import * as api from 'shop/client/api';

import { actionCreateDeliveryAddress } from 'shop/client/actions/cart';

const createEntitySpy = jest.spyOn(api, 'createEntity').mockImplementation();

describe('actions/cart', () => {
    beforeEach(() => {
        createEntitySpy.mockClear();
    });

    it('should create entity with cartId and address', () => {
        const cartId = 'cart-id';
        const address: DefaultAddress = {
            country: {
                isocode: 'at'
            },
            firstName: 'John',
            lastName: 'Smith',
            line1: 'address line 1',
            line2: 'address line 2',
            postalCode: '1010',
            town: 'Vienna'
        };
        actionCreateDeliveryAddress(cartId, address);
        expect(createEntitySpy).toHaveBeenCalledWith({
            url: expect.stringContaining(cartId),
            method: HttpMethod.POST,
            body: address
        });
    });
});
