/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import * as api from 'shop/client/api';

import { actionGetPaymentModes } from 'shop/client/actions';

const createEntitySpy = jest.spyOn(api, 'createEntity').mockImplementation();

describe('actions/payment', () => {
    beforeEach(() => {
        createEntitySpy.mockClear();
    });

    it('should create entity with payment modes url', () => {
        actionGetPaymentModes();
        expect(createEntitySpy).toHaveBeenCalledWith({
            url: 'paymentmodes'
        });
    });
});
