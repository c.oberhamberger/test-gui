/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpMethod } from '@archibald/core';
import { DefaultAddress } from '@archibald/storefront';

import { createEntity } from 'shop/client/api';

import type { Cart, CartEntry, CartModification, Product } from 'shop/client/interfaces/base';

export function actionGetCart(cartId: string) {
    return createEntity<Cart>({
        url: `users/current/carts/${cartId}`
    });
}

export function actionAddToCart(cartId: string, product: Product, quantity: number) {
    return createEntity<Cart>({
        url: cartId ? `users/current/carts/${cartId}/entries` : 'users/current/carts',
        method: HttpMethod.POST,
        body: {
            product,
            quantity
        }
    });
}

export function actionRemoveFromCart(cartId: string, entry: CartEntry) {
    return createEntity<Cart>({
        method: HttpMethod.DELETE,
        url: `users/current/carts/${cartId}/entries/${entry.entryNumber}`
    });
}

export function actionPatchCartEntry(cartId: string, entryNumber: number, patch: Partial<CartEntry>) {
    return createEntity<CartModification>({
        url: `users/current/carts/${cartId}/entries/${entryNumber}`,
        method: HttpMethod.PATCH,
        body: patch
    });
}

export function actionDeleteCartEntry(cartId: string, entryNumber: number) {
    return createEntity<void>({
        url: `users/current/carts/${cartId}/entries/${entryNumber}`,
        method: HttpMethod.DELETE
    });
}

export function actionCreateDeliveryAddress(cartId: string, address: DefaultAddress) {
    return createEntity<DefaultAddress>({
        url: `users/current/carts/${cartId}/addresses/delivery`,
        method: HttpMethod.POST,
        body: address
    });
}
