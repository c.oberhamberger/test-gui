/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { FieldSet } from '@archibald/storefront';

import { createEntity } from 'shop/client/api';

import type { Product } from 'shop/client/interfaces/base';

export function actionGetProduct(code: string, fieldSet: FieldSet = 'DEFAULT') {
    return createEntity<Product>({
        url: `products/${code}`,
        params: { fields: fieldSet }
    });
}
