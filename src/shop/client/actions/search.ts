/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { UrlHelper } from '@archibald/core';
import type { FieldSet } from '@archibald/storefront';

import { createEntity } from 'shop/client/api';

import type { Search } from 'shop/client/interfaces/base';

export function actionGetProducts(categoryId: string, sort?: string) {
    return createEntity<Search>({
        url: `products/search/category/${categoryId}`,
        params: { sort }
    });
}

export function actionExecuteSearch(search: string, sort = '', fieldSet: FieldSet = 'FULL') {
    const { q: query = '', page = 0, sort: sortParam = '' } = UrlHelper.parseSearch(search);
    const currentPage = Number(page) > 0 ? Number(page) - 1 : page;

    return createEntity<Search>({
        url: 'products/search',
        params: { query, sort: sort || sortParam, currentPage: currentPage, fields: fieldSet }
    });
}
