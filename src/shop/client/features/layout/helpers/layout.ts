/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { withBreakpoints } from 'shop/client/features/layout/helpers/breakpoints';
import { layoutPropDefs, LayoutProps, marginPropDefs, MarginProps, PaddingProps } from 'shop/client/features/layout/interfaces';

function withPaddingProps(props: PaddingProps) {
    return [
        withBreakpoints(props.p, 'rt-r-p'),
        withBreakpoints(props.px, 'rt-r-px'),
        withBreakpoints(props.py, 'rt-r-py'),
        withBreakpoints(props.pt, 'rt-r-pt'),
        withBreakpoints(props.pr, 'rt-r-pr'),
        withBreakpoints(props.pb, 'rt-r-pb'),
        withBreakpoints(props.pl, 'rt-r-pl')
    ]
        .filter(Boolean)
        .join(' ');
}

export function withMarginProps(props: MarginProps) {
    return [
        withBreakpoints(props.m, 'rt-r-m'),
        withBreakpoints(props.mx, 'rt-r-mx'),
        withBreakpoints(props.my, 'rt-r-my'),
        withBreakpoints(props.mt, 'rt-r-mt'),
        withBreakpoints(props.mr, 'rt-r-mr'),
        withBreakpoints(props.mb, 'rt-r-mb'),
        withBreakpoints(props.ml, 'rt-r-ml')
    ]
        .filter(Boolean)
        .join(' ');
}

export function withLayoutProps(props: LayoutProps) {
    return [
        withPaddingProps(props),
        withBreakpoints(props.position, 'rt-r-position'),
        withBreakpoints(props.shrink, 'rt-r-fs'),
        withBreakpoints(props.grow, 'rt-r-fg'),
        withBreakpoints(props.width, 'rt-r-w'),
        withBreakpoints(props.height, 'rt-r-h'),
        withBreakpoints(props.inset, 'rt-r-inset'),
        withBreakpoints(props.top, 'rt-r-top'),
        withBreakpoints(props.bottom, 'rt-r-bottom'),
        withBreakpoints(props.left, 'rt-r-left'),
        withBreakpoints(props.right, 'rt-r-right')
    ]
        .filter(Boolean)
        .join(' ');
}

function extractPaddingProps<T extends PaddingProps>(props: T) {
    const {
        // eslint-disable-next-line id-length
        p = layoutPropDefs.p.default,
        px = layoutPropDefs.px.default,
        py = layoutPropDefs.py.default,
        pt = layoutPropDefs.pt.default,
        pr = layoutPropDefs.pr.default,
        pb = layoutPropDefs.pb.default,
        pl = layoutPropDefs.pl.default,
        ...rest
    } = props;
    // eslint-disable-next-line id-length
    return { p, px, py, pt, pr, pb, pl, rest };
}

export function extractMarginProps<T extends MarginProps>(props: T) {
    const {
        // eslint-disable-next-line id-length
        m = marginPropDefs.m.default,
        mx = marginPropDefs.mx.default,
        my = marginPropDefs.my.default,
        mt = marginPropDefs.mt.default,
        mr = marginPropDefs.mr.default,
        mb = marginPropDefs.mb.default,
        ml = marginPropDefs.ml.default,
        ...rest
    } = props;
    // eslint-disable-next-line id-length
    return { m, mx, my, mt, mr, mb, ml, rest };
}

export function extractLayoutProps<T extends LayoutProps>(props: T) {
    const { rest: paddingRest, ...paddingProps } = extractPaddingProps(props);
    const {
        position = layoutPropDefs.position.default,
        width = layoutPropDefs.width.default,
        height = layoutPropDefs.height.default,
        inset = layoutPropDefs.inset.default,
        top = layoutPropDefs.top.default,
        bottom = layoutPropDefs.bottom.default,
        left = layoutPropDefs.left.default,
        right = layoutPropDefs.right.default,
        shrink = layoutPropDefs.shrink.default,
        grow = layoutPropDefs.grow.default,
        ...rest
    } = paddingRest;
    return {
        ...paddingProps,
        position,
        width,
        height,
        inset,
        top,
        bottom,
        left,
        right,
        shrink,
        grow,
        rest
    };
}

export function parseGridValue(value: string | undefined) {
    return value?.match(/^\d+$/) ? `repeat(${value}, minmax(0, 1fr))` : value;
}
