/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { ResponsiveMappingType } from 'shop/client/features/layout/interfaces';

import Classes from 'shop/client/features/layout/components/column/Column.scss';

export function mapResponsiveMappingToColumns(responsive?: ResponsiveMappingType, name = 'columns'): string {
    if (!responsive) {
        return '';
    }

    let mapping = '';
    for (const [key, value] of Object.entries(responsive)) {
        if (key === 'offsets') {
            mapping += mapResponsiveMappingToColumns(value, 'offsets');
        } else {
            mapping += ` ${Classes[`${name}${value}${key.toLowerCase()}`]}`;
        }
    }
    return mapping;
}
