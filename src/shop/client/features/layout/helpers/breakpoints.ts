/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Breakpoints, Responsive } from 'shop/client/features/layout/interfaces';

/**
 * A helper to generate CSS classes that include breakpoints.
 *
 * Examples:
 * ```
 * const marginTop = '1'
 * withBreakpoints(marginTop, 'mt') // returns 'mt-1'
 *
 * const padding = { initial: '1', xs: '2', md: '3' }
 * withBreakpoints(padding, 'p') // returns 'p-1 xs:p-1 md:p-3'
 *
 * const justifyContent = { initial: 'start', md: 'space-between' }
 * withBreakpoints(justifyContent, 'jc', { 'space-between': 'sb' }) // returns 'jc-start md:jc-sb'
 * ```
 */
export function withBreakpoints(
    value: Responsive<string | boolean> | undefined, // Value to check
    classPrefix = '', // CSS class prefix, e.g. "px" in "px-1" class
    valueMap?: Record<string, string> // Optionally, an object to map prop values to a different CSS suffix
) {
    const classes: string[] = [];

    if (typeof value === 'object') {
        for (const bp of Object.keys(value) as Breakpoints[]) {
            if (bp in value) {
                const str = value[bp]?.toString();
                const isNegative = str?.startsWith('-');
                const delimiter = classPrefix === '' ? '' : '-';
                const prefix = isNegative ? `-${classPrefix}` : classPrefix;
                const matchedValue = isNegative ? str?.substring(1) : str;

                if (matchedValue === undefined) {
                    continue;
                }

                const suffix = valueMap?.[matchedValue] ?? matchedValue;

                const className = bp === 'initial' ? `${prefix}${delimiter}${suffix}` : `${bp}:${prefix}${delimiter}${suffix}`;

                classes.push(className);
            }
        }
    }

    if (typeof value === 'string') {
        const isNegative = value.startsWith('-');
        const delimiter = classPrefix === '' ? '' : '-';
        const prefix = isNegative ? `-${classPrefix}` : classPrefix;
        const matchedValue = isNegative ? value.substring(1) : value;
        const suffix = valueMap?.[matchedValue] ?? matchedValue;
        classes.push(`${prefix}${delimiter}${suffix}`);
    }

    if (typeof value === 'boolean') {
        const delimiter = classPrefix === '' ? '' : '-';
        const matchedValue = value.toString();
        const suffix = valueMap?.[matchedValue] ?? matchedValue;
        classes.push(`${classPrefix}${delimiter}${suffix}`);
    }

    return classes.join(' ');
}

export function isBreakpointsObject<V extends string>(obj: Responsive<V | Omit<string, V>> | undefined): obj is Record<Breakpoints, string> {
    return typeof obj === 'object';
}
