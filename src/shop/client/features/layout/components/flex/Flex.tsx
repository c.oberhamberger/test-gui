/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { Slot } from '@radix-ui/react-slot';

import { type ComponentPropsWithoutRef, type ElementRef, forwardRef } from 'react';
import { extractLayoutProps, extractMarginProps, withBreakpoints, withLayoutProps, withMarginProps } from 'shop/client/features/layout/helpers';
import { flexPropDefs, GetPropDefTypes, LayoutProps, MarginProps } from 'shop/client/features/layout/interfaces';

type FlexElement = ElementRef<'div'>;
type FlexOwnProps = GetPropDefTypes<typeof flexPropDefs>;

interface FlexProps extends ComponentPropsWithoutRef<'div'>, MarginProps, LayoutProps, FlexOwnProps {
    asChild?: boolean;
}

const Flex = forwardRef<FlexElement, FlexProps>((props, forwardedRef) => {
    const { rest: marginRest, ...marginProps } = extractMarginProps(props);
    const { rest: layoutRest, ...layoutProps } = extractLayoutProps(marginRest);
    const {
        className,
        asChild,
        display = flexPropDefs.display.default,
        direction = flexPropDefs.direction.default,
        align = flexPropDefs.align.default,
        justify = flexPropDefs.justify.default,
        wrap = flexPropDefs.wrap.default,
        gap = flexPropDefs.gap.default,
        ...flexProps
    } = layoutRest;
    const Component = asChild ? Slot : 'div';

    return (
        <Component
            {...flexProps}
            ref={forwardedRef}
            className={classNamesHelper([
                'rt-Flex',
                className,
                withBreakpoints(display, 'rt-r-display'),
                withBreakpoints(direction, 'rt-r-fd'),
                withBreakpoints(align, 'rt-r-ai'),
                withBreakpoints(justify, 'rt-r-jc', { between: 'space-between' }),
                withBreakpoints(wrap, 'rt-r-fw'),
                withBreakpoints(gap, 'rt-r-gap'),
                withLayoutProps(layoutProps),
                withMarginProps(marginProps)
            ])}
        />
    );
});

Flex.displayName = 'Flex';

export default Flex;
