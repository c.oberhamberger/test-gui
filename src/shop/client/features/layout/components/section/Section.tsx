/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import { type ComponentPropsWithoutRef, type ElementRef, forwardRef } from 'react';
import { extractLayoutProps, extractMarginProps, withBreakpoints, withLayoutProps, withMarginProps } from 'shop/client/features/layout/helpers';
import { GetPropDefTypes, LayoutProps, MarginProps, sectionPropDefs } from 'shop/client/features/layout/interfaces';

type SectionElement = ElementRef<'div'>;
type SectionOwnProps = GetPropDefTypes<typeof sectionPropDefs>;
interface SectionProps extends ComponentPropsWithoutRef<'div'>, MarginProps, LayoutProps, SectionOwnProps {}

const Section = forwardRef<SectionElement, SectionProps>((props, forwardedRef) => {
    const { rest: marginRest, ...marginProps } = extractMarginProps(props);
    const { rest: layoutRest, ...layoutProps } = extractLayoutProps(marginRest);
    const { className, size = sectionPropDefs.size.default, display = sectionPropDefs.display.default, ...sectionProps } = layoutRest;

    return (
        <section
            {...sectionProps}
            ref={forwardedRef}
            className={classNamesHelper([
                'rt-Section',
                className,
                withBreakpoints(size, 'rt-r-size'),
                withBreakpoints(display, 'rt-r-display'),
                withLayoutProps(layoutProps),
                withMarginProps(marginProps)
            ])}
        />
    );
});

Section.displayName = 'Section';

export default Section;
