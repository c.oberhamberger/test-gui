/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import { ComponentPropsWithoutRef, ElementRef, forwardRef } from 'react';
import { extractMarginProps, withBreakpoints, withLayoutProps, withMarginProps } from 'shop/client/features/layout/helpers';
import { extractLayoutProps } from 'shop/client/features/layout/helpers/layout';
import { containerPropDefs, GetPropDefTypes, LayoutProps, MarginProps } from 'shop/client/features/layout/interfaces';

type ContainerElement = ElementRef<'div'>;
type ContainerOwnProps = GetPropDefTypes<typeof containerPropDefs>;

interface ContainerProps extends ComponentPropsWithoutRef<'div'>, MarginProps, LayoutProps, ContainerOwnProps {
    withPaddings?: boolean;
}

const Container = forwardRef<ContainerElement, ContainerProps>(({ withPaddings = true, ...props }, forwardedRef) => {
    const { rest: marginRest, ...marginProps } = extractMarginProps(props);
    const { rest: layoutRest, ...layoutProps } = extractLayoutProps(marginRest);
    const { children, className, size = containerPropDefs.size.default, display = containerPropDefs.display.default, ...containerProps } = layoutRest;
    const classNames = classNamesHelper([
        'rt-ContainerInner',
        'rt-r-size-4',
        (withPaddings ?? true) && 'rt-r-pl-3 sm:rt-r-pl-5 md:rt-r-pl-6',
        (withPaddings ?? true) && 'rt-r-pr-3 sm:rt-r-pr-5 md:rt-r-pr-6'
    ]);

    return (
        <div
            {...containerProps}
            ref={forwardedRef}
            className={classNamesHelper([
                'rt-Container',
                className,
                withBreakpoints(size, 'rt-r-size'),
                withBreakpoints(display, 'rt-r-display'),
                withLayoutProps(layoutProps),
                withMarginProps(marginProps)
            ])}
        >
            <div className={classNames}>{children}</div>
        </div>
    );
});

Container.displayName = 'Container';

export default Container;
