/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import type { ColumnsSizeType, OffsetsSizeType, ResponsiveMappingType } from 'shop/client/features/layout/interfaces';

import Column from 'shop/client/features/layout/components/column/Column';

describe('<Column /> component:', () => {
    const content = 'Content';

    it('should render children', async () => {
        await renderAsync(<Column>{content}</Column>);
        const column = screen.getByText(content);
        expect(column).toHaveTextContent(content);
    });

    it('should render all columns options from 1 to 12', async () => {
        for (const columns of ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'] as ColumnsSizeType[]) {
            await renderAsync(<Column columns={columns}>{`content ${columns}`}</Column>);
            const column = screen.getByText(`content ${columns}`);
            expect(column).toHaveClass(`columns${columns}`);
        }
    });

    it('should render all columns offset options from 1 to 11', async () => {
        for (const offset of ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'] as OffsetsSizeType[]) {
            await renderAsync(
                <Column offsets={offset} columns="3">
                    {`offsets ${offset}`}
                </Column>
            );
            const column = screen.getByText(`offsets ${offset}`);
            expect(column).toHaveClass(`columns3 offsets${offset}`);
        }
    });

    it('should render custom responsive options', async () => {
        const responsive = { sm: '8', md: '6', lg: '4', xl: '2', offsets: { sm: '4', md: '6', lg: '8', xl: '10' } } as ResponsiveMappingType;
        await renderAsync(
            <Column columns="2" offsets="10" responsive={responsive}>
                {content}
            </Column>
        );
        const column = screen.getByText(content);
        expect(column).toHaveClass('columns8sm columns6md columns4lg columns2xl offsets4sm offsets6md offsets8lg offsets10xl');
        expect(column).not.toHaveClass('columns2');
        expect(column).not.toHaveClass('offsets10');
    });
});
