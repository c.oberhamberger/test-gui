/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import type { CSSProperties, PropsWithChildren } from 'react';
import { mapResponsiveMappingToColumns } from 'shop/client/features/layout/helpers';
import type { ColumnsSizeType, OffsetsSizeType, ResponsiveMappingType } from 'shop/client/features/layout/interfaces';

import type { ISmartEditAttributes } from 'shop/client/interfaces/base';

import Box from 'shop/client/features/layout/components/box/Box';

import Classes from 'shop/client/features/layout/components/column/Column.scss';

interface Props extends PropsWithChildren {
    columns?: ColumnsSizeType;
    offsets?: OffsetsSizeType;
    responsive?: ResponsiveMappingType;
    className?: string;
    style?: CSSProperties;
    attributes?: ISmartEditAttributes;
}

function Column({ attributes, children, className, columns = '12', offsets, style, responsive }: Props) {
    const responsiveMapping = mapResponsiveMappingToColumns(responsive);
    const classNames = classNamesHelper([
        !responsiveMapping && Classes[`columns${columns}`],
        !responsiveMapping && offsets && [Classes[`offsets${offsets}`]],
        responsiveMapping,
        className
    ]);

    return (
        <Box className={classNames} style={style} data-testid="column" {...attributes}>
            {children}
        </Box>
    );
}

export default Column;
