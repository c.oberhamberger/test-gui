/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { Slot } from '@radix-ui/react-slot';

import { type ComponentPropsWithoutRef, type CSSProperties, type ElementRef, forwardRef } from 'react';
import {
    extractLayoutProps,
    extractMarginProps,
    isBreakpointsObject,
    parseGridValue,
    withBreakpoints,
    withLayoutProps,
    withMarginProps
} from 'shop/client/features/layout/helpers';
import { GetPropDefTypes, gridPropDefs, LayoutProps, MarginProps } from 'shop/client/features/layout/interfaces';

type GridElement = ElementRef<'div'>;
type GridOwnProps = GetPropDefTypes<typeof gridPropDefs>;

interface GridProps extends ComponentPropsWithoutRef<'div'>, MarginProps, LayoutProps, GridOwnProps {
    asChild?: boolean;
}

const Grid = forwardRef<GridElement, GridProps>((props, forwardedRef) => {
    const { rest: marginRest, ...marginProps } = extractMarginProps(props);
    const { rest: layoutRest, ...layoutProps } = extractLayoutProps(marginRest);
    const {
        className,
        asChild,
        display = gridPropDefs.display.default,
        columns = gridPropDefs.columns.default,
        rows = gridPropDefs.rows.default,
        flow = gridPropDefs.flow.default,
        align = gridPropDefs.align.default,
        justify = gridPropDefs.justify.default,
        gap = gridPropDefs.gap.default,
        gapX = gridPropDefs.gapX.default,
        gapY = gridPropDefs.gapY.default,
        style: propStyle,
        ...gridProps
    } = layoutRest;
    const Component = asChild ? Slot : 'div';

    let style: CSSProperties | Record<string, string | undefined> = {
        ...propStyle
    };

    if (typeof columns === 'string') {
        style = {
            '--grid-template-columns': parseGridValue(columns),
            ...style
        };
    }

    if (typeof rows === 'string') {
        style = {
            '--grid-template-rows': parseGridValue(rows),
            ...style
        };
    }

    if (isBreakpointsObject(columns)) {
        for (const breakpoint in columns) {
            if (Object.prototype.hasOwnProperty.call(columns, breakpoint)) {
                const customProperty = `--grid-template-columns-${breakpoint}`;

                style = {
                    [customProperty]: parseGridValue(columns[breakpoint]),
                    ...style
                };
            }
        }
    }

    if (isBreakpointsObject(rows)) {
        for (const breakpoint in rows) {
            if (Object.prototype.hasOwnProperty.call(rows, breakpoint)) {
                const customProperty = `--grid-template-rows-${breakpoint}`;

                style = {
                    [customProperty]: parseGridValue(rows[breakpoint]),
                    ...style
                };
            }
        }
    }

    return (
        <Component
            {...gridProps}
            ref={forwardedRef}
            className={classNamesHelper([
                'rt-Grid',
                className,
                withBreakpoints(display, 'rt-r-display'),
                withBreakpoints(flow, 'rt-r-gaf'),
                withBreakpoints(align, 'rt-r-ai'),
                withBreakpoints(justify, 'rt-r-jc', { between: 'space-between' }),
                withBreakpoints(gap, 'rt-r-gap'),
                withBreakpoints(gapX, 'rt-r-cg'),
                withBreakpoints(gapY, 'rt-r-rg'),
                withLayoutProps(layoutProps),
                withMarginProps(marginProps)
            ])}
            style={Object.keys(style).length ? style : undefined}
        />
    );
});

Grid.displayName = 'Grid';

export default Grid;
