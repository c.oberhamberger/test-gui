/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { Slot } from '@radix-ui/react-slot';

import { type ComponentPropsWithoutRef, type ElementRef, forwardRef } from 'react';
import { extractLayoutProps, extractMarginProps, withBreakpoints, withLayoutProps, withMarginProps } from 'shop/client/features/layout/helpers';
import { boxPropDefs, GetPropDefTypes, LayoutProps, MarginProps } from 'shop/client/features/layout/interfaces';

type BoxElement = ElementRef<'div'>;
type BoxOwnProps = GetPropDefTypes<typeof boxPropDefs>;

interface BoxProps extends ComponentPropsWithoutRef<'div'>, MarginProps, LayoutProps, BoxOwnProps {
    asChild?: boolean;
}

const Box = forwardRef<BoxElement, BoxProps>((props, forwardedRef) => {
    const { rest: marginRest, ...marginProps } = extractMarginProps(props);
    const { rest: layoutRest, ...layoutProps } = extractLayoutProps(marginRest);
    const { className, asChild, display = boxPropDefs.display.default, ...boxProps } = layoutRest;
    const Component = asChild ? Slot : 'div';

    return (
        <Component
            {...boxProps}
            ref={forwardedRef}
            className={classNamesHelper([
                'rt-Box',
                className,
                withBreakpoints(display, 'rt-r-display'),
                withLayoutProps(layoutProps),
                withMarginProps(marginProps)
            ])}
        />
    );
});

Box.displayName = 'Box';

export default Box;
