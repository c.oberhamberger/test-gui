/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import Row from 'shop/client/features/layout/components/row/Row';

describe('<Row /> component:', () => {
    const content = 'Content';

    it('should render 12 columns', async () => {
        await renderAsync(<Row>{content}</Row>);
        const row = screen.getByText(content);
        expect(row).toHaveStyle('--grid-template-columns: repeat(12, 1fr);');
    });

    it('should have default column and row gap of 32px', async () => {
        await renderAsync(<Row>{content}</Row>);
        const row = screen.getByText(content);
        expect(row).toHaveClass('rt-r-rg-6 rt-r-cg-6');
    });

    it('should have margin top and bottom', async () => {
        await renderAsync(
            <Row mt="2" mb="2">
                {content}
            </Row>
        );
        const row = screen.getByText(content);
        expect(row).toHaveClass('rt-r-mt-2 rt-r-mb-2');
    });
});
