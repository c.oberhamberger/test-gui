/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CSSProperties, PropsWithChildren } from 'react';
import { Responsive } from 'shop/client/features/layout/interfaces';

import Grid from 'shop/client/features/layout/components/grid/Grid';

interface Props extends PropsWithChildren {
    style?: CSSProperties;
    className?: string;
    mt?: Responsive<'0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'>;
    mb?: Responsive<'0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'>;
    rowGap?: Responsive<'0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'>;
    columnsGap?: Responsive<'0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'>;
}

function Row({ children, className, columnsGap = '6', mb, mt, rowGap = '6', style }: Props) {
    return (
        <Grid columns="repeat(12, 1fr)" gapX={columnsGap} gapY={rowGap} mt={mt} mb={mb} className={className} style={style}>
            {children}
        </Grid>
    );
}

export default Row;
