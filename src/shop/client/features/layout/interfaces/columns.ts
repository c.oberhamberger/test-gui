/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { ResponsiveColumn } from 'shop/client/features/layout/interfaces/layout';

export type ColumnsSizeType = ResponsiveColumn<'1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12'>;
export type OffsetsSizeType = ResponsiveColumn<Omit<ColumnsSizeType, '12'>>;
interface OffsetsMappingInterface {
    offsets?: OffsetsSizeType;
}

export type ResponsiveMappingType = ColumnsSizeType & OffsetsMappingInterface;
