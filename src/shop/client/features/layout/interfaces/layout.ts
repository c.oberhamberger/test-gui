/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ReactNode } from 'react';

export type Breakpoints = 'initial' | 'xs' | 'sm' | 'md' | 'lg' | 'xl';
export type Responsive<T> = T | Partial<Record<Breakpoints, T>>;
export type ResponsiveColumn<T> = T | Partial<Omit<Record<Breakpoints, T>, 'initial' | 'xs'>>;

type BooleanPropDef = {
    type: 'boolean';
    default?: boolean;
    required?: boolean;
};
type StringPropDef = {
    type: 'string';
    default?: string;
    required?: boolean;
};
type StringOrNumberPropDef = {
    type: 'string | number';
    default?: string | number;
    required?: boolean;
};
type ReactNodePropDef = {
    type: 'ReactNode';
    default?: ReactNode;
    required?: boolean;
};
type EnumPropDef<T> = {
    type: 'enum';
    values: readonly T[];
    default?: T;
    required?: boolean;
};

type RegularPropDef<T> = BooleanPropDef | StringPropDef | StringOrNumberPropDef | ReactNodePropDef | EnumPropDef<T>;
type ResponsivePropDef<T = any> = RegularPropDef<T> & {
    responsive: true;
};

export type PropDef<T = any> = RegularPropDef<T> | ResponsivePropDef<T>;

type GetPropDefType<Def> = Def extends BooleanPropDef
    ? Def extends ResponsivePropDef
        ? Responsive<boolean>
        : boolean
    : Def extends StringPropDef
    ? Def extends ResponsivePropDef
        ? Responsive<string>
        : string
    : Def extends StringOrNumberPropDef
    ? Def extends ResponsivePropDef
        ? Responsive<string | number>
        : string | number
    : Def extends ReactNodePropDef
    ? Def extends ResponsivePropDef
        ? Responsive<ReactNode>
        : ReactNode
    : Def extends EnumPropDef<infer Type>
    ? Def extends ResponsivePropDef<infer Type>
        ? Responsive<Type>
        : Type
    : never;

export type GetPropDefTypes<P> = {
    [K in keyof P]?: GetPropDefType<P[K]>;
};

// prettier-ignore
const marginValues = ['auto', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-1', '-2', '-3', '-4', '-5', '-6', '-7', '-8', '-9'] as const;

export const marginPropDefs = {
    // eslint-disable-next-line id-length
    m: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    mx: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    my: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    mt: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    mr: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    mb: { type: 'enum', values: marginValues, default: undefined, responsive: true },
    ml: { type: 'enum', values: marginValues, default: undefined, responsive: true }
} satisfies {
    m: PropDef<(typeof marginValues)[number]>;
    mx: PropDef<(typeof marginValues)[number]>;
    my: PropDef<(typeof marginValues)[number]>;
    mt: PropDef<(typeof marginValues)[number]>;
    mr: PropDef<(typeof marginValues)[number]>;
    mb: PropDef<(typeof marginValues)[number]>;
    ml: PropDef<(typeof marginValues)[number]>;
};

export type MarginProps = GetPropDefTypes<typeof marginPropDefs>;

const paddingValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;

const paddingPropDefs = {
    // eslint-disable-next-line id-length
    p: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    px: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    py: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    pt: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    pr: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    pb: { type: 'enum', values: paddingValues, default: undefined, responsive: true },
    pl: { type: 'enum', values: paddingValues, default: undefined, responsive: true }
} satisfies {
    p: PropDef<(typeof paddingValues)[number]>;
    px: PropDef<(typeof paddingValues)[number]>;
    py: PropDef<(typeof paddingValues)[number]>;
    pt: PropDef<(typeof paddingValues)[number]>;
    pr: PropDef<(typeof paddingValues)[number]>;
    pb: PropDef<(typeof paddingValues)[number]>;
    pl: PropDef<(typeof paddingValues)[number]>;
};

export type PaddingProps = GetPropDefTypes<typeof paddingPropDefs>;

const positionValues = ['static', 'relative', 'absolute', 'fixed', 'sticky'] as const;
const positionEdgeValues = ['auto', '0', '50%', '100%'] as const;
// prettier-ignore
const widthHeightValues = ['auto', 'min-content', 'max-content', '100%', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;
const flexShrinkValues = ['0', '1'] as const;
const flexGrowValues = ['0', '1'] as const;

export const layoutPropDefs = {
    ...paddingPropDefs,
    position: { type: 'enum', values: positionValues, default: undefined, responsive: true },
    inset: { type: 'enum', values: positionEdgeValues, default: undefined, responsive: true },
    top: { type: 'enum', values: positionEdgeValues, default: undefined, responsive: true },
    right: { type: 'enum', values: positionEdgeValues, default: undefined, responsive: true },
    bottom: { type: 'enum', values: positionEdgeValues, default: undefined, responsive: true },
    left: { type: 'enum', values: positionEdgeValues, default: undefined, responsive: true },
    width: { type: 'enum', values: widthHeightValues, default: undefined, responsive: true },
    height: { type: 'enum', values: widthHeightValues, default: undefined, responsive: true },
    shrink: { type: 'enum', values: flexShrinkValues, default: undefined, responsive: true },
    grow: { type: 'enum', values: flexGrowValues, default: undefined, responsive: true }
} satisfies {
    p: PropDef<(typeof paddingValues)[number]>;
    px: PropDef<(typeof paddingValues)[number]>;
    py: PropDef<(typeof paddingValues)[number]>;
    pt: PropDef<(typeof paddingValues)[number]>;
    pr: PropDef<(typeof paddingValues)[number]>;
    pb: PropDef<(typeof paddingValues)[number]>;
    pl: PropDef<(typeof paddingValues)[number]>;
    position: PropDef<(typeof positionValues)[number]>;
    inset: PropDef<(typeof positionEdgeValues)[number]>;
    top: PropDef<(typeof positionEdgeValues)[number]>;
    right: PropDef<(typeof positionEdgeValues)[number]>;
    bottom: PropDef<(typeof positionEdgeValues)[number]>;
    left: PropDef<(typeof positionEdgeValues)[number]>;
    width: PropDef<(typeof widthHeightValues)[number]>;
    height: PropDef<(typeof widthHeightValues)[number]>;
    shrink: PropDef<(typeof flexShrinkValues)[number]>;
    grow: PropDef<(typeof flexGrowValues)[number]>;
};

export type LayoutProps = GetPropDefTypes<typeof layoutPropDefs>;
