/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { PropDef } from 'shop/client/features/layout/interfaces/layout';

const boxDisplayValues = ['none', 'inline', 'inline-block', 'block'] as const;
export const boxPropDefs = {
    display: { type: 'enum', values: boxDisplayValues, default: undefined, responsive: true }
} satisfies {
    display: PropDef<(typeof boxDisplayValues)[number]>;
};

export const containerSizes = ['1', '2', '3', '4'] as const;
export const containerDisplayValues = ['none', 'block'] as const;
export const containerPropDefs = {
    size: { type: 'enum', values: containerSizes, default: '4', responsive: true },
    display: { type: 'enum', values: containerDisplayValues, default: undefined, responsive: true }
} satisfies {
    size: PropDef<(typeof containerSizes)[number]>;
    display: PropDef<(typeof containerDisplayValues)[number]>;
};

const flexDisplayValues = ['none', 'inline-flex', 'flex'] as const;
const flexDirectionValues = ['row', 'column', 'row-reverse', 'column-reverse'] as const;
const flexAlignValues = ['start', 'center', 'end', 'baseline', 'stretch'] as const;
const flexJustifyValues = ['start', 'center', 'end', 'between'] as const;
const flexWrapValues = ['nowrap', 'wrap', 'wrap-reverse'] as const;
const flexGapValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;

export const flexPropDefs = {
    display: { type: 'enum', values: flexDisplayValues, default: 'flex', responsive: true },
    direction: { type: 'enum', values: flexDirectionValues, default: undefined, responsive: true },
    align: { type: 'enum', values: flexAlignValues, default: undefined, responsive: true },
    justify: { type: 'enum', values: flexJustifyValues, default: 'start', responsive: true },
    wrap: { type: 'enum', values: flexWrapValues, default: undefined, responsive: true },
    gap: { type: 'enum', values: flexGapValues, default: undefined, responsive: true }
} satisfies {
    display: PropDef<(typeof flexDisplayValues)[number]>;
    direction: PropDef<(typeof flexDirectionValues)[number]>;
    align: PropDef<(typeof flexAlignValues)[number]>;
    justify: PropDef<(typeof flexJustifyValues)[number]>;
    wrap: PropDef<(typeof flexWrapValues)[number]>;
    gap: PropDef<(typeof flexGapValues)[number]>;
};

const gridDisplayValues = ['none', 'inline-grid', 'grid'] as const;
const gridFlowValues = ['row', 'column', 'dense', 'row-dense', 'column-dense'] as const;
const gridAlignValues = ['start', 'center', 'end', 'baseline', 'stretch'] as const;
const gridJustifyValues = ['start', 'center', 'end', 'between'] as const;
const gridGapValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] as const;

export const gridPropDefs = {
    display: { type: 'enum', values: gridDisplayValues, default: undefined, responsive: true },
    columns: { type: 'string', default: undefined, responsive: true },
    rows: { type: 'string', default: undefined, responsive: true },
    flow: { type: 'enum', values: gridFlowValues, default: undefined, responsive: true },
    align: { type: 'enum', values: gridAlignValues, default: undefined, responsive: true },
    justify: { type: 'enum', values: gridJustifyValues, default: undefined, responsive: true },
    gap: { type: 'enum', values: gridGapValues, default: undefined, responsive: true },
    gapX: { type: 'enum', values: gridGapValues, default: undefined, responsive: true },
    gapY: { type: 'enum', values: gridGapValues, default: undefined, responsive: true }
} satisfies {
    display: PropDef<(typeof gridDisplayValues)[number]>;
    columns: PropDef<string>;
    rows: PropDef<string>;
    flow: PropDef<(typeof gridFlowValues)[number]>;
    align: PropDef<(typeof gridAlignValues)[number]>;
    justify: PropDef<(typeof gridJustifyValues)[number]>;
    gap: PropDef<(typeof gridGapValues)[number]>;
    gapX: PropDef<(typeof gridGapValues)[number]>;
    gapY: PropDef<(typeof gridGapValues)[number]>;
};

const sectionSizes = ['1', '2', '3'] as const;
const sectionDisplayValues = ['none', 'block'] as const;

export const sectionPropDefs = {
    size: { type: 'enum', values: sectionSizes, default: '3', responsive: true },
    display: { type: 'enum', values: sectionDisplayValues, default: undefined, responsive: true }
} satisfies {
    size: PropDef<(typeof sectionSizes)[number]>;
    display: PropDef<(typeof sectionDisplayValues)[number]>;
};
