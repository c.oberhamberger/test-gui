/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Product } from 'shop/client/interfaces/base';

export function getProductDetailBreadcrumbs(product: Product, homepageLabel: string) {
    const breadcrumbs = [
        {
            label: homepageLabel,
            link: '/'
        }
    ];

    if (product?.categories) {
        breadcrumbs.push({
            label: product.categories[0].name,
            link: '/c/' + product.categories[0].urlHash
        });
    }

    if (product?.brand) {
        breadcrumbs.push({
            label: product.brand.name,
            link: product.brand.url
        });
    }

    breadcrumbs.push({
        label: product.name ?? '',
        link: product.url ?? ''
    });

    return breadcrumbs;
}

export function getProductListingBreadcrumbs(categoryName?: string, categoryCode?: string, homepageLabel?: string) {
    return [
        {
            label: homepageLabel ?? '',
            link: '/'
        },
        {
            label: categoryName ?? '',
            link: '/c/' + (categoryCode ?? '')
        }
    ];
}
