/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode, type DefaultCoreDataState, type DefaultServerState, type DeepPartial } from '@archibald/core';
import type { DefaultCMSSearchEngineOptimizations } from '@archibald/storefront';

import { CMSComponentTypeCode, CMSPosition, CMSTemplate, CommonConstants, Language } from 'shop/client/constants';

import type { CMSComponent, CMSPages, CMSState, NavigationInterface, Product } from 'shop/client/interfaces/base';

export const mockComponent: CMSComponent = {
    modifiedtime: '2018-03-26 09:26:43.410 UTC',
    uid: 'Header_Component',
    uuid: 'Header_Component_UUID',
    name: 'Header Component',
    typeCode: CMSComponentTypeCode.HEADER,
    columns: '12'
};

export const mockCoreData: DeepPartial<DefaultCoreDataState> = {
    messages: {},
    language: Language.DE,
    errors: {},
    lastHttpStatus: HttpCode.CODE_200
};

export const mockServer: DeepPartial<DefaultServerState> = {
    userAgent: '',
    cookie: '',
    requestUrl: ''
};

export const mockSrchOptPage: DefaultCMSSearchEngineOptimizations = {
    title: 'I am a title',
    description: 'I am a description',
    index: CommonConstants.NO_INDEX,
    follow: CommonConstants.NO_FOLLOW,
    canonical: '/cp/cannonicalUrl'
};

export const mockPages: CMSPages = {
    test123: {
        uid: 'uid',
        name: 'test123 Page',
        catalogVersionUuid: '123',
        template: CMSTemplate.LANDING_PAGE,
        searchEngineOptimizations: mockSrchOptPage,
        contentSlots: [
            {
                slotId: 'header',
                slotUuid: 'header123',
                name: 'headerSlot',
                position: CMSPosition.HEADER,
                components: [
                    {
                        typeCode: CMSComponentTypeCode.HEADER,
                        uid: '123'
                    }
                ]
            },
            {
                slotId: 'content',
                slotUuid: 'content123',
                name: 'contentSlot',
                position: CMSPosition.CONTENT,
                components: [
                    {
                        typeCode: CMSComponentTypeCode.LINK,
                        uid: '123'
                    }
                ]
            }
        ]
    }
};

export const mockCMS: CMSState = {
    cmsTicketId: ''
};

export const mockProduct: Product = {
    categories: [],
    code: '793447800000',
    images: [{ url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' }, { url: 'https://netconomy.net/public/dji.jpg' }],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    }
};

export const mockProduct2: Product = {
    categories: [],
    code: '793447800001',
    images: [{ url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' }, { url: 'https://netconomy.net/public/dji.jpg' }],
    name: 'Dji Osmo 5',
    price: {
        currencyIso: 'EUR',
        value: 899.99,
        formattedValue: 'EUR 899.99'
    }
};
export const productWithImages: Product = {
    categories: [],
    code: '793447800000',
    images: [{ url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' }, { url: 'https://netconomy.net/public/dji.jpg' }],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    }
};

export const productWithoutImages: Product = {
    categories: [],
    code: '793447800000',
    images: [],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    }
};

export const productWithSingleImage: Product = {
    categories: [],
    code: '793447800000',
    images: [{ url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' }],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    }
};

export const productWithFeatures: Product = {
    categories: [],
    code: '793447800000',
    images: [],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    },
    features: [
        {
            label: 'Filmaufnahme',
            labelCode: 'melFilmaufnahme',
            topFact: false,
            values: [
                {
                    value: '4K UHD bei 60 fps, 100 MBit/s'
                }
            ]
        },
        {
            label: 'Bildstabilisator',
            labelCode: 'mel_bildstabilisator',
            topFact: true,
            values: [
                {
                    value: 'Ja'
                }
            ]
        },
        {
            label: 'Auflösung',
            labelCode: 'mel_auflösung',
            topFact: false,
            values: [
                {
                    value: '12 Mpx'
                }
            ]
        },
        {
            label: 'Mitgeliefertes Zubehör',
            labelCode: 'MEL_CLA_FotoWelt_Alle',
            topFact: false,
            values: [
                {
                    value: 'Charging Case'
                }
            ]
        },
        {
            label: 'Zusätzliche Funktionen',
            labelCode: 'Zusätzliche Funktionen',
            topFact: false,
            values: [
                {
                    value: 'Bis zu 140 min Akkulaufzeit , 3-Achsen-Stabilisiserung'
                }
            ]
        },
        {
            label: 'Breite',
            labelCode: 'mel_breite',
            topFact: false,
            values: [
                {
                    value: '36.9 mm'
                }
            ]
        },
        {
            label: 'Höhe',
            labelCode: 'mel_hoehe',
            topFact: false,
            values: [
                {
                    value: '121.9 mm'
                }
            ]
        },
        {
            label: 'Tiefe',
            labelCode: 'mel_tiefe',
            topFact: false,
            values: [
                {
                    value: '28.6 mm'
                }
            ]
        },
        {
            label: 'Gewicht',
            labelCode: 'mel_gewicht',
            topFact: false,
            values: [
                {
                    value: '116 g'
                }
            ]
        }
    ]
};

export const productWithFeaturesAndImages: Product = {
    code: '793447800000',
    categories: [
        {
            code: '3699718344',
            name: 'Camcorder',
            slug: 'melectronics/foto-video/video-actioncams/camcorder',
            urlHash: '3699718344'
        }
    ],
    images: [
        { url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' },
        { url: 'https://netconomy.net/public/dji.jpg' },
        { url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' },
        { url: 'https://netconomy.net/public/dji.jpg' },
        { url: 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg' }
    ],
    name: 'Dji Osmo 4',
    price: {
        currencyIso: 'EUR',
        value: 999.99,
        formattedValue: 'EUR 999.99'
    },
    features: [
        {
            label: 'Filmaufnahme',
            labelCode: 'melFilmaufnahme',
            topFact: false,
            values: [
                {
                    value: '4K UHD bei 60 fps, 100 MBit/s'
                }
            ]
        },
        {
            label: 'Bildstabilisator',
            labelCode: 'mel_bildstabilisator',
            topFact: true,
            values: [
                {
                    value: 'Ja'
                }
            ]
        },
        {
            label: 'Auflösung',
            labelCode: 'mel_auflösung',
            topFact: false,
            values: [
                {
                    value: '12 Mpx'
                }
            ]
        },
        {
            label: 'Mitgeliefertes Zubehör',
            labelCode: 'MEL_CLA_FotoWelt_Alle',
            topFact: false,
            values: [
                {
                    value: 'Charging Case'
                }
            ]
        },
        {
            label: 'Zusätzliche Funktionen',
            labelCode: 'Zusätzliche Funktionen',
            topFact: false,
            values: [
                {
                    value: 'Bis zu 140 min Akkulaufzeit , 3-Achsen-Stabilisiserung'
                }
            ]
        },
        {
            label: 'Breite',
            labelCode: 'mel_breite',
            topFact: false,
            values: [
                {
                    value: '36.9 mm'
                }
            ]
        },
        {
            label: 'Höhe',
            labelCode: 'mel_hoehe',
            topFact: false,
            values: [
                {
                    value: '121.9 mm'
                }
            ]
        },
        {
            label: 'Tiefe',
            labelCode: 'mel_tiefe',
            topFact: false,
            values: [
                {
                    value: '28.6 mm'
                }
            ]
        },
        {
            label: 'Gewicht',
            labelCode: 'mel_gewicht',
            topFact: false,
            values: [
                {
                    value: '116 g'
                }
            ]
        }
    ]
};

export const mockNavigation: NavigationInterface[] = [
    {
        id: 0,
        title: 'Produkte',
        to: '/c/3699718344',
        className: '',
        content: '',
        children: [
            {
                id: 1,
                title: '',
                to: '/',
                className: '',
                style: { gridRow: 1, gridColumn: 1 },
                content: '<img loading="lazy" src="/public/Elec-EN-01-350W.png" width="100%" />'
            },
            {
                id: 2,
                title: '',
                to: '/',
                className: '',
                style: { gridRow: 2, gridColumn: 1 },
                content: '<img loading="lazy" src="/public/Elec-EN-02-350W.png" width="100%" />'
            },
            {
                id: 3,
                title: '',
                to: '/',
                className: '',
                style: { gridRow: 1, gridColumn: 2 },
                content: '<img loading="lazy" src="/public/Elec-EN-03-350W.png" width="100%" />'
            },
            {
                id: 4,
                title: '',
                to: '/',
                className: '',
                style: { gridRow: 2, gridColumn: 2 },
                content: '<img loading="lazy" src="/public/Elec-EN-04-350W.png" width="100%" />'
            },
            {
                id: 5,
                title: '',
                to: '/',
                className: '',
                style: { gridRow: '1 / span 2', gridColumn: '3 / span 4' },
                content: '<img loading="lazy" src="/public/Electronics_EN_01_1400W.png" width="100%" />'
            }
        ]
    },
    {
        id: 1,
        title: 'Angebote',
        to: '/',
        className: '',
        content: '',
        children: []
    },
    {
        id: 2,
        title: 'TV & Audio',
        to: '/',
        className: '',
        content: '',
        children: []
    },
    {
        id: 3,
        title: 'Computing & Tablets',
        to: '/',
        className: '',
        content: '',
        children: []
    },
    {
        id: 4,
        title: 'Photo & Video',
        to: '/',
        className: '',
        content: '',
        children: []
    },
    {
        id: 5,
        title: 'Telephony & Navigation',
        to: '/',
        className: '',
        content: '',
        children: []
    },
    {
        id: 6,
        title: 'Brands',
        to: '/',
        className: '',
        content: '',
        children: []
    }
];
