/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { MockAdapter, SessionClient, SessionProvider } from '@archibald/auth';
import { mockIntersectionObserver, mockLocalStorage, mockMatchMedia, mockResizeObserver, mockSetImmediate } from '@archibald/testing';
import { registerConfigMock, registerDefaultCustomWrapper } from '@archibald/testing/register';

import type { ReactNode } from 'react';

import ConfigMock from 'shop/client/config/__mocks__/config';

function TestingProviders(component: ReactNode) {
    const sessionClient = new SessionClient({ adapter: MockAdapter });
    return <SessionProvider client={sessionClient}>{component}</SessionProvider>;
}

registerConfigMock(ConfigMock);
registerDefaultCustomWrapper(TestingProviders);

mockSetImmediate();
mockIntersectionObserver();
mockLocalStorage();
mockResizeObserver();
mockMatchMedia();
