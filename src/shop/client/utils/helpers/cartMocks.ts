import { ProductType } from '@archibald/storefront';
import { PriceType } from '@archibald/storefront/lib/node/constants/price';

import { mockProduct, mockProduct2 } from 'shop/client/utils/helpers/mocks';

import { Cart } from 'shop/client/interfaces/base';

export const emptyCart: Cart = {
    entries: [],
    guid: 'cart-guid',
    code: 'cart-code',
    totalItems: 0,
    totalPrice: {
        currencyIso: 'EUR',
        priceType: PriceType.BUY,
        formattedValue: '0 €',
        value: 0
    },
    user: {
        id: '123',
        uid: '123',
        name: 'Archibald'
    }
};

export const cartWithOneEntry: Cart = {
    entries: [
        {
            entryNumber: 1,
            productType: ProductType.PRODUCT,
            quantity: 1,
            product: mockProduct,
            basePrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 999.99',
                value: 999.99
            },
            totalPrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 999.99',
                value: 999.99
            }
        }
    ],
    guid: 'cart-guid',
    code: 'cart-code',
    totalItems: 1,
    totalPrice: {
        currencyIso: 'EUR',
        priceType: PriceType.BUY,
        formattedValue: '999.99 €',
        value: 999.99
    },
    user: {
        id: '123',
        uid: '123',
        name: 'Archibald'
    }
};

export const cartWithTwoEntries: Cart = {
    entries: [
        {
            entryNumber: 1,
            productType: ProductType.PRODUCT,
            quantity: 1,
            product: mockProduct,
            basePrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 999.99',
                value: 999.99
            },
            totalPrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 999.99',
                value: 999.99
            }
        },
        {
            entryNumber: 2,
            productType: ProductType.PRODUCT,
            quantity: 1,
            product: mockProduct2,
            basePrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 899.99',
                value: 899.99
            },
            totalPrice: {
                currencyIso: 'EUR',
                priceType: PriceType.BUY,
                formattedValue: 'EUR 899.99',
                value: 899.99
            }
        }
    ],
    guid: 'cart-guid',
    code: 'cart-code',
    totalItems: 2,
    totalPrice: {
        currencyIso: 'EUR',
        priceType: PriceType.BUY,
        formattedValue: '1899.98 €',
        value: 1899.98
    },
    user: {
        id: '123',
        uid: '123',
        name: 'Archibald'
    }
};
