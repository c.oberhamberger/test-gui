/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CMSComponent, CMSLinkInterface } from 'shop/client/interfaces/base';

import Link from 'shop/client/components/atoms/link/Link';
import Text from 'shop/client/components/atoms/text/Text';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/organisms/cms/promo-bar/CMSPromoBarComponent.scss';

interface Props extends CMSComponent {
    link: CMSLinkInterface;
    text: string;
}

function CMSPromoBarComponent({ link, text }: Props) {
    if (link?.url) {
        return (
            <Flex align="center" justify="center" className={Classes.promoBar}>
                <Link to={link.url}>
                    <Text>{text}</Text>
                </Link>
            </Flex>
        );
    }

    return (
        <Flex align="center" justify="center" className={Classes.promoBar}>
            <Text>{text}</Text>
        </Flex>
    );
}

export default CMSPromoBarComponent;
