/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';
import { useTranslate } from '@archibald/storefront';

import { useMemo } from 'react';
import { getProductListingBreadcrumbs } from 'shop/client/utils/helpers/product';

import { CategoryRouteParams } from 'shop/client/interfaces/base';

import { useProductsByCategory } from 'shop/client/hooks/product';

import Breadcrumbs from 'shop/client/components/molecules/breadcrumbs/Breadcrumbs';
import ProductListing from 'shop/client/components/molecules/product/listing/ProductListing';
import EmptyProductListing from 'shop/client/components/molecules/product/listing/empty/EmptyProductListing';
import ProductSortingComponent from 'shop/client/components/molecules/product/sorting/ProductSortingComponent';

import Classes from 'shop/client/components/organisms/cms/product/listing/CMSProductListingComponent.scss';

function CMSProductListingComponent() {
    const translate = useTranslate();
    const { categoryId } = useParams<CategoryRouteParams>();
    const { data: searchResult } = useProductsByCategory(categoryId, true);
    const { categoryCode, categoryName, products } = searchResult ?? {};
    const breadcrumbs = useMemo(() => getProductListingBreadcrumbs(categoryName, categoryCode, translate('link.homepage')), [categoryCode, categoryName]);

    if (!products?.length) {
        return <EmptyProductListing category={categoryId} />;
    }

    return (
        <>
            <div className={Classes.facets}>
                <Breadcrumbs breadcrumbs={breadcrumbs} />
                <ProductSortingComponent />
            </div>
            <ProductListing products={products} />
        </>
    );
}

export default CMSProductListingComponent;
