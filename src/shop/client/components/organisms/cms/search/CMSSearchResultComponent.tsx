/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useLocation } from '@archibald/core';

import { useSearch } from 'shop/client/hooks';

import Message from 'shop/client/components/atoms/message/Message';
import ProductListing from 'shop/client/components/molecules/product/listing/ProductListing';
import ProductSortingComponent from 'shop/client/components/molecules/product/sorting/ProductSortingComponent';
import EmptySearchResult from 'shop/client/components/molecules/search/empty/EmptySearchResult';

import Classes from 'shop/client/components/organisms/cms/search/CMSSearchResultComponent.scss';

function CMSSearchResultComponent() {
    const { search } = useLocation();
    const { data: searchResult } = useSearch();
    const { products } = searchResult ?? {};
    const params = new URLSearchParams(search);
    const term = params.get('q') || '';

    if (!products?.length) {
        return <EmptySearchResult term={term} />;
    }

    return (
        <>
            <div className={Classes.header}>
                <div className={Classes.results}>
                    <Message code="search.result" params={[`${products.length}`, term]} />
                </div>
                <ProductSortingComponent />
            </div>
            <ProductListing products={products} />
        </>
    );
}

export default CMSSearchResultComponent;
