/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSComponent, CMSLinkInterface, ImageInterface } from 'shop/client/interfaces/base';

import { useCarousel } from 'shop/client/hooks';

import CarouselArrow from 'shop/client/components/atoms/carousel/arrow/CarouselArrow';
import CarouselDots from 'shop/client/components/atoms/carousel/dots/CarouselDots';
import TextImageComponent from 'shop/client/components/molecules/text-image/TextImageComponent';

import Classes from 'shop/client/components/organisms/cms/banners/CMSBannersComponent.scss';

interface BannerInterface extends ImageInterface {
    link: CMSLinkInterface;
}

interface Props extends CMSComponent {
    banners: BannerInterface[];
}

function CMSBannersComponent({ banners }: Props) {
    const { ref, selectedSlideIndex, scrollNext, scrollPrevious, scrollTo, isPreviousButtonDisabled, isNextButtonDisabled } = useCarousel({
        skipSnaps: false,
        useArrows: true,
        useDots: true
    });

    return (
        <div data-testid="CMSBannerComponentCarousel" className={Classes.carousel}>
            <CarouselArrow direction="left" onClick={scrollPrevious} disabled={isPreviousButtonDisabled} />
            <div className={Classes.viewport} ref={ref}>
                <div className={Classes.container}>
                    {banners?.map((banner: BannerInterface, index: number) => {
                        return (
                            <div key={index} className={Classes.slide}>
                                <div className={Classes.slideInner}>
                                    <TextImageComponent key={banner.src} image={banner} link={banner.link} />
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
            <CarouselArrow direction="right" onClick={scrollNext} disabled={isNextButtonDisabled} />
            <CarouselDots dots={banners.length} selectedDot={selectedSlideIndex} onClick={scrollTo} />
        </div>
    );
}

export default CMSBannersComponent;
