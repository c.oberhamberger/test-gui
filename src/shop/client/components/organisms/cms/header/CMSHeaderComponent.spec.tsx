/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import CMSHeaderComponent from 'shop/client/components/organisms/cms/header/CMSHeaderComponent';

describe('<CMSHeaderComponent /> component:', () => {
    it('should have logo', async () => {
        await renderAsync(<CMSHeaderComponent />);
        const logo = screen.getByRole('img', { name: /logo/i });
        expect(logo).toBeDefined();
    });

    it('should have at more than one link in the full header', async () => {
        await renderAsync(<CMSHeaderComponent />);

        const links = screen.queryAllByRole('link');

        expect(links.length).toBeGreaterThan(1);
    });

    it('should have at most one link in the reduced header', async () => {
        await renderAsync(<CMSHeaderComponent fullHeader={false} />);

        const links = screen.queryAllByRole('link');

        expect(links.length).toBeLessThanOrEqual(1);
    });
});
