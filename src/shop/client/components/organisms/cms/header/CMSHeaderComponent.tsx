/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import Header from 'shop/client/components/molecules/header/Header';
import ReducedHeader from 'shop/client/components/molecules/header/ReducedHeader';

interface Props {
    fullHeader?: boolean;
}

function CMSHeaderComponent({ fullHeader = true }: Props) {
    if (fullHeader) {
        return <Header />;
    }

    return <ReducedHeader />;
}

export default CMSHeaderComponent;
