/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Suspense } from 'react';

import type { CMSComponent } from 'shop/client/interfaces/base';

import Loader from 'shop/client/components/atoms/loader/Loader';
import CartComponent from 'shop/client/components/molecules/cart/cart/CartComponent';

interface Props extends CMSComponent {
    max: number;
}

function CMSCartComponent({ max }: Props) {
    return (
        <Suspense fallback={<Loader type="page" />}>
            <CartComponent max={max} />
        </Suspense>
    );
}

export default CMSCartComponent;
