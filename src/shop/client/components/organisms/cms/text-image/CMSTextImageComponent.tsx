/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CMSComponent, CMSImageInterface, CMSLinkInterface } from 'shop/client/interfaces/base';

import TextImageComponent from 'shop/client/components/molecules/text-image/TextImageComponent';

interface Props extends CMSComponent {
    image: CMSImageInterface;
    link: CMSLinkInterface;
    title?: string;
    shortText?: string;
}

function CMSTextImageComponent({ image, link, title, shortText, index }: Props) {
    return <TextImageComponent image={image} shortText={shortText} link={link} title={title} index={index} />;
}

export default CMSTextImageComponent;
