/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import { CMSComponentTypeCode } from 'shop/client/constants';

import { CMSImageInterface, CMSLinkInterface, LinkTarget } from 'shop/client/interfaces/base';

import CMSTextImageComponent from 'shop/client/components/organisms/cms/text-image/CMSTextImageComponent';

const TestLink: CMSLinkInterface = {
    creationtime: 'a',
    modifiedtime: 'a',
    uid: 'link',
    typeCode: CMSComponentTypeCode.LINK,
    url: 'http://localhost/de',
    external: false,
    name: 'TestLink',
    targetName: LinkTarget.SAME
};

const TestImage: CMSImageInterface = {
    creationtime: 'a',
    modifiedtime: 'a',
    typeCode: CMSComponentTypeCode.IMAGE,
    uuid: '123',
    uid: '123ab',
    src: 'http://localhost/image.jpg'
};

const defaultProps = {
    creationtime: 'a',
    modifiedtime: 'a',
    uuid: '123',
    uid: '123ab',
    typeCode: CMSComponentTypeCode.TEXT_IMAGE,
    className: 'test--class',
    title: 'Text Title',
    image: TestImage,
    link: TestLink,
    shortText: 'Kurzer Text',
    columns: '2'
} as const;

describe('<CMSTextImageComponent /> component:', () => {
    describe('Rendering CMSTextImageComponent', () => {
        it('should exist ', async () => {
            const component = await renderAsync(<CMSTextImageComponent {...defaultProps} />);
            expect(component).toBeDefined();
        });
    });
});
