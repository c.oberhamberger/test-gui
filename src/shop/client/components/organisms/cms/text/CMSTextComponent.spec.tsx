/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import { CMSComponentTypeCode } from 'shop/client/constants';

import { CMSLinkInterface, LinkTarget } from 'shop/client/interfaces/base';

import CMSTextComponent from 'shop/client/components/organisms/cms/text/CMSTextComponent';

const TestLink: CMSLinkInterface = {
    creationtime: 'a',
    modifiedtime: 'a',
    uid: 'link',
    typeCode: CMSComponentTypeCode.LINK,
    url: 'http://localhost/de',
    external: false,
    name: 'TestLink',
    targetName: LinkTarget.SAME
};

const defaultProps = {
    creationtime: 'a',
    modifiedtime: 'a',
    uuid: '123',
    uid: '123ab',
    typeCode: CMSComponentTypeCode.TEXT_IMAGE,
    className: 'test--class',
    link: TestLink
};

describe('<CMSTextComponent /> component:', () => {
    const text = 'This is a text to be displayed';

    it('should exist', async () => {
        const { getByText } = await renderAsync(<CMSTextComponent text={text} {...defaultProps} />);
        const component = getByText(text);
        expect(component).toBeDefined();
    });
});
