/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CMSComponent, CMSLinkInterface } from 'shop/client/interfaces/base';

import Link from 'shop/client/components/atoms/link/Link';
import Text from 'shop/client/components/atoms/text/Text';

interface Props extends CMSComponent {
    className?: string;
    link: CMSLinkInterface;
    text: string;
}

function CMSTextComponent(props: Props) {
    const { className, link, text } = props;

    return (
        <Link className={className} to={link.url}>
            <Text>{text}</Text>
        </Link>
    );
}

export default CMSTextComponent;
