/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import Button from 'shop/client/components/atoms/button/Button';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Flex from 'shop/client/features/layout/components/flex/Flex';

interface Props {
    term?: string;
}

function EmptySearchResult({ term }: Props) {
    return (
        <Flex align="center" justify="center" direction="column" gap="7">
            <Text size="large" align="center">
                <Message code="search.result" params={['0', term ?? '']} simpleText />
            </Text>
            <Button appearance="primary" url="/">
                <Message code="link.homepage" />
            </Button>
        </Flex>
    );
}

export default EmptySearchResult;
