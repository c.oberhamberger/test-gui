/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import Availability from 'shop/client/components/molecules/availability/Availability';

describe('<Availability /> component:', () => {
    it('should be red if item is not in stock', async () => {
        await renderAsync(<Availability isInStock={false} />);
        const indicator = screen.getByTestId('circle');
        expect(indicator).toHaveClass('red');
    });

    it('should be green if item is in stock', async () => {
        await renderAsync(<Availability isInStock={true} />);
        const indicator = screen.getByTestId('circle');
        expect(indicator).toHaveClass('green');
    });
});
