/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Box from 'shop/client/features/layout/components/box/Box';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/molecules/availability/Availability.scss';

interface Props {
    isInStock?: boolean;
}

function Availability({ isInStock }: Props) {
    return (
        <Flex align="baseline" justify="center">
            <Box className={classNamesHelper([Classes.circle, isInStock ? Classes.green : Classes.red])} data-testid="circle" />
            <Text size="small" weight="medium">
                <Message code={`product.availability.${isInStock ? '' : 'not'}instock`} simpleText />
            </Text>
        </Flex>
    );
}

export default Availability;
