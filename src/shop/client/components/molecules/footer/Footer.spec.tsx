/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import Footer from 'shop/client/components/molecules/footer/Footer';

describe('<Footer /> component:', () => {
    const footerText = 'Help & Contact';

    describe('Rendering Footer', () => {
        it('should exist ', async () => {
            const { getByText, container } = await renderAsync(<Footer />);
            const footer = getByText(footerText);
            expect(container).toBeDefined();
            expect(footer).toBeDefined();
        });
    });
});
