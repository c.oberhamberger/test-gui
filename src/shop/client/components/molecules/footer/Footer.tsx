/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import Text from 'shop/client/components/atoms/text/Text';
import Box from 'shop/client/features/layout/components/box/Box';
import Column from 'shop/client/features/layout/components/column/Column';
import Container from 'shop/client/features/layout/components/container/Container';
import Row from 'shop/client/features/layout/components/row/Row';
import Section from 'shop/client/features/layout/components/section/Section';

import Classes from 'shop/client/components/molecules/footer/Footer.scss';

function Footer() {
    return (
        <Section className={Classes.footer}>
            <Container>
                <Row className={Classes.row}>
                    <Column columns="3">
                        <Text size="medium" weight="bold">
                            Help & Contact
                        </Text>
                        <Box className={Classes.items}>
                            <Text tag="div">Customer Service</Text>
                            <Text tag="div">Customer Account</Text>
                            <Text tag="div">FAQ</Text>
                            <Text tag="div">Gift Cards</Text>
                            <Text tag="div">Shipping</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Text size="medium" weight="bold">
                            Privacy
                        </Text>
                        <Box className={Classes.items}>
                            <Text tag="div">Privacy</Text>
                            <Text tag="div">Cancellation Policy</Text>
                            <Text tag="div">Cancellation Form</Text>
                            <Text tag="div">Stores</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Text size="medium" weight="bold">
                            Payment Methods
                        </Text>
                        <Box className={Classes.items}>
                            <Text tag="div">Master Card</Text>
                            <Text tag="div">Visa</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Text size="medium" weight="bold">
                            Shipping Partner
                        </Text>
                        <Box className={Classes.items}>
                            <Text tag="div">DHL</Text>
                            <Text tag="div">Post AG</Text>
                            <Text tag="div">DPD</Text>
                        </Box>
                    </Column>
                </Row>
            </Container>
        </Section>
    );
}

export default Footer;
