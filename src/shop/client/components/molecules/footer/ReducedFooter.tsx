/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import Text from 'shop/client/components/atoms/text/Text';
import Box from 'shop/client/features/layout/components/box/Box';
import Column from 'shop/client/features/layout/components/column/Column';
import Container from 'shop/client/features/layout/components/container/Container';
import Row from 'shop/client/features/layout/components/row/Row';
import Section from 'shop/client/features/layout/components/section/Section';

import Classes from 'shop/client/components/molecules/footer/Footer.scss';

function ReducedFooter() {
    return (
        <Section className={Classes.footer}>
            <Container>
                <Row className={Classes.row}>
                    <Column columns="3">
                        <Box className={Classes.items}>
                            <Text tag="div">Customer Service</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Box className={Classes.items}>
                            <Text tag="div">Privacy</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Box className={Classes.items}>
                            <Text tag="div">Cancellation Policy</Text>
                        </Box>
                    </Column>

                    <Column columns="3">
                        <Box className={Classes.items}>
                            <Text tag="div">FAQ</Text>
                        </Box>
                    </Column>
                </Row>
            </Container>
        </Section>
    );
}

export default ReducedFooter;
