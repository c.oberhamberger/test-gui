/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Fragment, memo } from 'react';

import Icon from 'shop/client/components/atoms/icon/Icon';
import Link from 'shop/client/components/atoms/link/Link';
import Text from 'shop/client/components/atoms/text/Text';
import Box from 'shop/client/features/layout/components/box/Box';
import Flex from 'shop/client/features/layout/components/flex/Flex';

export interface BreadcrumbItem {
    label: string;
    link?: string;
}

interface Props {
    breadcrumbs?: BreadcrumbItem[];
}

function Breadcrumbs({ breadcrumbs }: Props) {
    if (!breadcrumbs?.length) {
        return null;
    }

    const breadcrumbsLength = breadcrumbs.length - 1;

    function renderBreadcrumbs({ label, link }: BreadcrumbItem, index: number) {
        if (breadcrumbsLength !== index && link && label) {
            return (
                <Fragment key={`${label}-${index}`}>
                    <Box display="inline-block">
                        <Link to={link}>
                            <Text size="small">{label}</Text>
                        </Link>
                    </Box>
                    <Box display="inline-block" ml="1" mr="1" pt="1">
                        <Icon type="caret-right" />
                    </Box>
                </Fragment>
            );
        }

        return (
            <Box key={`${label}-${index}`} display="inline-block">
                <Text size="small">{label}</Text>
            </Box>
        );
    }

    return (
        <Flex align="center" mt="3" mb="3" wrap="wrap">
            {breadcrumbs.map(renderBreadcrumbs)}
        </Flex>
    );
}

export default memo(Breadcrumbs);
