/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import Breadcrumbs from 'shop/client/components/molecules/breadcrumbs/Breadcrumbs';

describe('<Breadcrumbs /> component:', () => {
    it('should return null if breadcrumbs are empty', async () => {
        await renderAsync(<Breadcrumbs breadcrumbs={[]} />);

        const breadcrumb = screen.queryByText(/test breadcrumb/i);
        expect(breadcrumb).toBeNull();
    });

    it('should render provided breadcrumbs', async () => {
        const breadcrumbs = [
            {
                label: 'Test breadcrumb',
                link: '/a/link'
            },
            {
                label: 'Test breadcrumb 2',
                link: '/a/link-2'
            }
        ];

        await renderAsync(<Breadcrumbs breadcrumbs={breadcrumbs} />);

        const firstBreadcrumbLink = screen.getByRole('link', { name: 'Test breadcrumb' });
        expect(firstBreadcrumbLink).toHaveTextContent(breadcrumbs[0].label);

        const secondBreadcrumbLink = screen.queryByRole('link', { name: 'Test breadcrumb 2' });
        expect(secondBreadcrumbLink).toBeNull();

        const secondBreadcrumbText = screen.getByText('Test breadcrumb 2');
        expect(secondBreadcrumbText).toHaveTextContent(breadcrumbs[0].label);
    });
});
