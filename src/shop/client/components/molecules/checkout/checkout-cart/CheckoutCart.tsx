/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CartEntry } from 'shop/client/interfaces/base';

import { useCart } from 'shop/client/hooks/cart';

import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import CheckoutCartEntry from 'shop/client/components/molecules/checkout/checkout-cart/CheckoutCartEntry';
import Section from 'shop/client/features/layout/components/section/Section';

function CheckoutCart() {
    const { data: cart } = useCart();

    return (
        <Section size="1">
            <Text headline="h2">
                <Message code="checkout.cartheader" />
            </Text>
            {cart?.entries.map((entry: CartEntry) => (
                <CheckoutCartEntry key={entry.entryNumber} entry={entry} />
            ))}
        </Section>
    );
}
export default CheckoutCart;
