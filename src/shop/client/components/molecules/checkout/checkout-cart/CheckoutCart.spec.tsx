/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import { cartWithTwoEntries } from 'shop/client/utils/helpers/cartMocks';

import { useCart } from 'shop/client/hooks/cart';

import CheckoutCart from 'shop/client/components/molecules/checkout/checkout-cart/CheckoutCart';

jest.mock('shop/client/hooks/cart');

describe('<CheckoutCart/> component:', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders two cart entries', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: cartWithTwoEntries });

        await renderAsync(<CheckoutCart />);

        const images = screen.getAllByRole('img');

        expect(images).toHaveLength(2);
    });
});
