/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import { emptyCart, cartWithOneEntry } from 'shop/client/utils/helpers/cartMocks';

import { UrlConstants } from 'shop/client/constants';

import { useCart } from 'shop/client/hooks/cart';

import CheckoutComponent from 'shop/client/components/molecules/checkout/checkout/CheckoutComponent';
import CheckoutForm from 'shop/client/components/molecules/checkout/form/CheckoutForm';

jest.mock('shop/client/hooks/cart');
jest.mock('shop/client/components/molecules/checkout/form/CheckoutForm');

describe('<CheckoutComponent/> component:', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('shows a link to the default category when the cart is empty', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: emptyCart, isLoading: false });

        await renderAsync(<CheckoutComponent />);

        const link = screen.getByRole('link', { name: '[cart.popup.recommendations]' });

        expect(link).toHaveAttribute('href', UrlConstants.defaultCategory);
    });

    it('shows loading spinner while loading', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: null, isLoading: true });

        await renderAsync(<CheckoutComponent />);

        expect(screen.getByRole('alert')).toBeInTheDocument();
    });

    it('shows the checkout form when there is a non-empty cart', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: cartWithOneEntry, isLoading: false });

        await renderAsync(<CheckoutComponent />);

        expect(CheckoutForm as jest.Mock).toHaveBeenCalled();
    });
});
