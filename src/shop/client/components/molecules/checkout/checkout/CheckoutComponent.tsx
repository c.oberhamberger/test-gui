/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { UrlConstants } from 'shop/client/constants';

import { useCart } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Loader from 'shop/client/components/atoms/loader/Loader';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import CheckoutForm from 'shop/client/components/molecules/checkout/form/CheckoutForm';
import Column from 'shop/client/features/layout/components/column/Column';
import Flex from 'shop/client/features/layout/components/flex/Flex';
import Row from 'shop/client/features/layout/components/row/Row';

function CheckoutComponent() {
    const { data: cart, isLoading } = useCart(false);

    if (isLoading && !cart?.entries.length) {
        return <Loader type="page" />;
    }

    if (!cart?.entries?.length) {
        return (
            <Flex justify="center" align="center" direction="column" gap="8" mt="9" mb="9">
                <Text size="large" align="center">
                    <Message code="cart.popup.empty" simpleText />
                </Text>
                <Button appearance="primary" url={UrlConstants.defaultCategory}>
                    <Message code="cart.popup.recommendations" />
                </Button>
            </Flex>
        );
    }

    return (
        <Row>
            <Column responsive={{ sm: '10', md: '8', lg: '6', offsets: { sm: '1', md: '2', lg: '3' } }}>
                <CheckoutForm />
            </Column>
        </Row>
    );
}

export default CheckoutComponent;
