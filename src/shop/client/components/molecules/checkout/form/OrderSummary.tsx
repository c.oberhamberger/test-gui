/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useCart } from 'shop/client/hooks/cart';

import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';
import Box from 'shop/client/features/layout/components/box/Box';
import Flex from 'shop/client/features/layout/components/flex/Flex';

function OrderSummary() {
    const { data: cart } = useCart(false);

    if (!cart?.totalItems) {
        return null;
    }

    return (
        <Flex direction="column">
            <Flex direction="column" align={{ initial: 'center', md: 'end' }} mb="8">
                {!!cart?.totalPrice && (
                    <Flex direction="row" align="end" mb="5">
                        <Text weight="medium">
                            <Message code="cart.summary.total" simpleText />
                            <Box ml="5" display="inline-block">
                                <PriceComponent currencyIso={cart.totalPrice.currencyIso} value={cart.totalPrice.value} />
                            </Box>
                        </Text>
                    </Flex>
                )}
            </Flex>
        </Flex>
    );
}

export default OrderSummary;
