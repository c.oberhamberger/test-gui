/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useTranslate } from '@archibald/storefront';

import { FieldErrors, UseFormRegister } from 'react-hook-form';

import LabelledInput from 'shop/client/components/atoms/form/input/LabelledInput';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import { CheckoutFormData } from 'shop/client/components/molecules/checkout/form/CheckoutForm';
import Section from 'shop/client/features/layout/components/section/Section';

interface Props {
    register: UseFormRegister<CheckoutFormData>;
    errors: FieldErrors<CheckoutFormData>;
}

function PersonalData({ register, errors }: Props) {
    const translate = useTranslate();

    return (
        <Section size="1">
            <Text headline="h2">
                <Message code="checkout.form.personaldata" />
            </Text>
            <LabelledInput
                id="firstName"
                labelText={translate('checkout.form.firstname')}
                register={register}
                registerOptions={{ required: translate('checkout.form.firstname.error') }}
                errors={errors}
            />
            <LabelledInput
                id="lastName"
                labelText={translate('checkout.form.lastname')}
                register={register}
                registerOptions={{ required: translate('checkout.form.lastname.error') }}
                errors={errors}
            />
            <LabelledInput
                id="email"
                labelText={translate('checkout.form.email')}
                type="email"
                register={register}
                registerOptions={{ required: translate('checkout.form.email.error') }}
                errors={errors}
            />
        </Section>
    );
}

export default PersonalData;
