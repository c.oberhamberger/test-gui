/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

/* eslint-disable react/jsx-no-bind */
import { DefaultCountry, useTranslate } from '@archibald/storefront';
import { Group } from '@radix-ui/react-select';

import { Control, Controller, FieldErrors, UseFormRegister } from 'react-hook-form';

import { useCountries } from 'shop/client/hooks/useCountries';

import LabelledInput from 'shop/client/components/atoms/form/input/LabelledInput';
import FormSelect from 'shop/client/components/atoms/form/select/FormSelect';
import Message from 'shop/client/components/atoms/message/Message';
import SelectItem from 'shop/client/components/atoms/select/item/SelectItem';
import Text from 'shop/client/components/atoms/text/Text';
import { CheckoutFormData } from 'shop/client/components/molecules/checkout/form/CheckoutForm';
import Section from 'shop/client/features/layout/components/section/Section';

interface Props {
    register: UseFormRegister<CheckoutFormData>;
    control: Control<CheckoutFormData>;
    errors: FieldErrors<CheckoutFormData>;
}

export function renderOptions({ isocode, name }: DefaultCountry) {
    return (
        <SelectItem key={isocode} value={isocode}>
            {name}
        </SelectItem>
    );
}

function Address({ register, control, errors }: Props) {
    const translate = useTranslate();
    const { data: countriesData } = useCountries();

    return (
        <Section size="1">
            <Text headline="h2">
                <Message code="checkout.form.deliveryandbillingaddress" />
            </Text>
            <LabelledInput
                id="line1"
                labelText={translate('checkout.form.street')}
                register={register}
                registerOptions={{ required: translate('checkout.form.street.error') }}
                errors={errors}
            />
            <LabelledInput id="line2" labelText={translate('checkout.form.streetadditional')} register={register} />
            <LabelledInput
                id="postalCode"
                labelText={translate('checkout.form.postalcode')}
                register={register}
                registerOptions={{ required: translate('checkout.form.postalcode.error') }}
                errors={errors}
            />
            <LabelledInput
                id="town"
                labelText={translate('checkout.form.city')}
                register={register}
                registerOptions={{ required: translate('checkout.form.city.error') }}
                errors={errors}
            />
            <LabelledInput id="country" labelText={translate('checkout.form.country')} errors={errors}>
                <Controller
                    control={control}
                    name="country"
                    rules={{ required: translate('checkout.form.country.error') }}
                    render={({ field }) => {
                        return (
                            <FormSelect label={translate('checkout.form.selectcountry')} field={field} name="country">
                                <Group>{countriesData?.countries?.map(renderOptions)}</Group>
                            </FormSelect>
                        );
                    }}
                />
            </LabelledInput>
        </Section>
    );
}

export default Address;
