/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useRedirect } from '@archibald/core';
import { DefaultAddress } from '@archibald/storefront';

import { SubmitHandler, useForm } from 'react-hook-form';

import { UrlConstants } from 'shop/client/constants';

import { useOrderMutation } from 'shop/client/hooks';
import { useCartMutation } from 'shop/client/hooks/cart';

import CheckoutCart from 'shop/client/components/molecules/checkout/checkout-cart/CheckoutCart';
import Address from 'shop/client/components/molecules/checkout/form/Address';
import BuyButton from 'shop/client/components/molecules/checkout/form/BuyButton';
import OrderSummary from 'shop/client/components/molecules/checkout/form/OrderSummary';
import Payment from 'shop/client/components/molecules/checkout/form/Payment';
import PersonalData from 'shop/client/components/molecules/checkout/form/PersonalData';
import Terms from 'shop/client/components/molecules/checkout/form/Terms';

export type CheckoutFormData = {
    firstName: string;
    lastName: string;
    email: string;
    line1: string;
    line2: string;
    postalCode: string;
    town: string;
    country: string;
    payment: string;
};

function CheckoutForm() {
    const redirect = useRedirect();
    const {
        handleSubmit,
        formState: { errors },
        register,
        control
    } = useForm<CheckoutFormData>();

    const { createDeliveryAddress } = useCartMutation();
    const { placeOrder } = useOrderMutation();

    const onSubmit: SubmitHandler<CheckoutFormData> = async (data) => {
        const address: DefaultAddress = {
            ...data,
            country: {
                isocode: data.country
            }
        };
        await createDeliveryAddress(address);
        await placeOrder();
        console.log('place order done');
        redirect({ to: UrlConstants.thankYouPage });
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <PersonalData register={register} errors={errors} />
            <Address register={register} control={control} errors={errors} />
            <Payment control={control} />
            <CheckoutCart />
            <OrderSummary />
            <BuyButton />
            <Terms />
        </form>
    );
}

export default CheckoutForm;
