/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

/* eslint-disable react/jsx-no-bind */
import { useTranslate } from '@archibald/storefront';

import { Control, Controller } from 'react-hook-form';

import { usePaymentModes } from 'shop/client/hooks';

import RadioGroup from 'shop/client/components/atoms/form/radio/RadioGroup';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import { CheckoutFormData } from 'shop/client/components/molecules/checkout/form/CheckoutForm';
import Box from 'shop/client/features/layout/components/box/Box';
import Section from 'shop/client/features/layout/components/section/Section';

interface Props {
    control: Control<CheckoutFormData>;
}

function Payment({ control }: Props) {
    const translate = useTranslate();
    const { data: paymentModesData } = usePaymentModes();

    const options = paymentModesData?.paymentModes?.map((paymentMode) => ({
        value: paymentMode.code,
        label: paymentMode.name
    }));

    return (
        <Section size="1">
            <Text headline="h2">
                <Message code="checkout.form.payment" />
            </Text>

            <Box pt="3">
                {options && (
                    <Controller
                        control={control}
                        name="payment"
                        render={({ field }) => (
                            <RadioGroup label={translate('checkout.form.payment')} defaultValue="creditcard" options={options} field={field} />
                        )}
                    />
                )}
            </Box>
        </Section>
    );
}

export default Payment;
