/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useCart } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Message from 'shop/client/components/atoms/message/Message';
import MiniCartEntries from 'shop/client/components/molecules/header/mini-cart/entries/MiniCartEntries';
import MiniCartSummary from 'shop/client/components/molecules/header/mini-cart/summary/MiniCartSummary';

import Classes from 'shop/client/components/molecules/header/mini-cart/content/MiniCartContent.scss';

function MiniCartContent() {
    const { data: cart } = useCart();
    const isCartEmpty = !cart?.entries?.length;

    return (
        <div data-testid="cartPopup">
            <div className={Classes.headline}>
                <Message code="cart.popup.headline" />
            </div>
            {isCartEmpty ? (
                <div className={Classes.popupContent}>
                    <div className={Classes.emptyPopupContent}>
                        <Message code="cart.popup.empty" />
                    </div>
                    <Button appearance="primary" fullWidth url="/c/3699718344">
                        <Message code="cart.popup.recommendations" />
                    </Button>
                </div>
            ) : (
                <div>
                    <MiniCartEntries />
                    <MiniCartSummary />
                </div>
            )}
        </div>
    );
}

export default MiniCartContent;
