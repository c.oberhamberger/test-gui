/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useCart } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Message from 'shop/client/components/atoms/message/Message';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';

import Classes from 'shop/client/components/molecules/header/mini-cart/summary/MiniCartSummary.scss';

function MiniCartSummary() {
    const { data: cart } = useCart();

    return (
        <>
            <div className={Classes.totalPrice}>
                <Message code="cart.popup.total" />
                <PriceComponent currencyIso={cart?.totalPrice.currencyIso} value={cart?.totalPrice.value || 0} />
            </div>
            <Button appearance="primary" fullWidth url="/cart">
                <Message code="cart.popup.gotocart" />
            </Button>
        </>
    );
}

export default MiniCartSummary;
