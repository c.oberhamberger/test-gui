/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { usePathname } from '@archibald/core';
import * as HoverCard from '@radix-ui/react-hover-card';

import { useLanguage } from 'shop/client/hooks';

import CartIconComponent from 'shop/client/components/molecules/cart/icon/CartIconComponent';
import MiniCartContent from 'shop/client/components/molecules/header/mini-cart/content/MiniCartContent';

import Classes from 'shop/client/components/molecules/header/mini-cart/MiniCart.scss';

function MiniCart() {
    const path = usePathname();
    const language = useLanguage();

    if (path === `/${language}/cart`) {
        return (
            <div className={Classes.miniCart}>
                <CartIconComponent />
            </div>
        );
    }

    return (
        <div className={Classes.miniCart}>
            <div data-testid="cartIcon">
                <HoverCard.Root openDelay={500}>
                    <HoverCard.Trigger asChild>
                        <CartIconComponent />
                    </HoverCard.Trigger>
                    <HoverCard.Portal>
                        <HoverCard.Content className={Classes.hoverCardContent} align="end" arrowPadding={20}>
                            <HoverCard.Arrow asChild>
                                <span className={Classes.hoverCardArrow} />
                            </HoverCard.Arrow>
                            <MiniCartContent />
                        </HoverCard.Content>
                    </HoverCard.Portal>
                </HoverCard.Root>
            </div>
        </div>
    );
}

export default MiniCart;
