/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CartEntry } from 'shop/client/interfaces/base';

import { useCart } from 'shop/client/hooks/cart';

import Image from 'shop/client/components/atoms/image/Image';
import Link from 'shop/client/components/atoms/link/Link';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';

import Classes from 'shop/client/components/molecules/header/mini-cart/entries/MiniCartEntries.scss';

function MiniCartEntries() {
    const { data: cart } = useCart();

    function renderEntries(cartEntry: CartEntry) {
        return (
            <Link to={cartEntry.product.url ?? ''} key={cartEntry.product.code}>
                <div className={Classes.product}>
                    <div className={Classes.description}>
                        <div className={Classes.image}>
                            <Image src={cartEntry.product.images?.[0]?.url ?? ''} />
                        </div>
                        <div>
                            <div className={Classes.name}>{cartEntry.product.name}</div>
                            <div className={Classes.info}>{cartEntry.product.brand?.name}</div>
                            <div className={Classes.info}>x{cartEntry.quantity}</div>
                        </div>
                    </div>
                    <div className={Classes.price}>
                        <PriceComponent currencyIso={cartEntry.product.price.currencyIso} value={cartEntry.product.price.value} />
                    </div>
                </div>
            </Link>
        );
    }

    return <div className={Classes.entries}>{cart?.entries?.map(renderEntries)}</div>;
}

export default MiniCartEntries;
