/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Suspense } from 'react';
import logo from 'shop/resources/img/base/archibald-logo.png';

import Link from 'shop/client/components/atoms/link/Link';
import Loader from 'shop/client/components/atoms/loader/Loader';
import MiniCart from 'shop/client/components/molecules/header/mini-cart/MiniCart';
import SearchComponent from 'shop/client/components/molecules/header/search/SearchComponent';
import NavigationComponent from 'shop/client/components/molecules/navigation/NavigationComponent';
import UserIconComponent from 'shop/client/components/molecules/user/icon/UserIconComponent';
import Box from 'shop/client/features/layout/components/box/Box';
import Container from 'shop/client/features/layout/components/container/Container';
import Flex from 'shop/client/features/layout/components/flex/Flex';
import Grid from 'shop/client/features/layout/components/grid/Grid';

import Classes from 'shop/client/components/molecules/header/Header.scss';

function Header() {
    return (
        <Container>
            <Grid columns="1fr 2fr 1fr" rows="auto auto" gap="5" align="center" justify="between" mt="5" mb="7">
                <Box className={Classes.search}>
                    <SearchComponent />
                </Box>
                <Flex align="center" justify="center" className={Classes.logo}>
                    <Link className={Classes.logoLink} to="/">
                        <img className={Classes.logoIcon} src={logo} alt="Logo" />
                        RCHIBALD
                    </Link>
                </Flex>
                <Flex align="center" justify="end" gap={{ initial: '3', xs: '6', sm: '9' }} className={Classes.icons}>
                    <Flex align="center" justify="end" width="6" height="6">
                        <Suspense fallback={<Loader />}>
                            <UserIconComponent />
                        </Suspense>
                    </Flex>
                    <MiniCart />
                </Flex>
                <Box className={Classes.navigation}>
                    <NavigationComponent />
                </Box>
            </Grid>
        </Container>
    );
}

export default Header;
