/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useLanguage, useNavigate } from '@archibald/core';
import { useTranslate } from '@archibald/storefront';

import type { KeyboardEvent } from 'react';

import Input from 'shop/client/components/atoms/input/input/Input';

import Classes from 'shop/client/components/molecules/header/search/SearchComponent.scss';

function SearchComponent() {
    const translate = useTranslate();
    const navigate = useNavigate();
    const language = useLanguage();

    function handleOnEnter(event: KeyboardEvent<HTMLInputElement>) {
        if (event.key === 'Enter') {
            navigate(`/${language}/s?q=${event.currentTarget.value}`);
        }
    }

    function handleOnIconClick(value: string) {
        navigate(`/${language}/s?q=${value}`);
    }

    return (
        <div className={Classes.search}>
            <Input
                testId="searchInput"
                icon="search"
                placeholder={translate('header.search.placeholder')}
                onKeyDown={handleOnEnter}
                onIconClick={handleOnIconClick}
            />
        </div>
    );
}

export default SearchComponent;
