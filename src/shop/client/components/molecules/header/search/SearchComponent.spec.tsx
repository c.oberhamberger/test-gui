/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { fireEvent, renderAsync, screen } from '@archibald/testing';

import SearchComponent from 'shop/client/components/molecules/header/search/SearchComponent';

describe('<SearchComponent /> component:', () => {
    it('should have a functional input', async () => {
        await renderAsync(<SearchComponent />);
        const input = screen.getByRole('textbox');
        expect(input).toBeDefined();
        expect(input).toHaveValue('');
        const searchTerm = 'search term or product id';
        fireEvent.change(input, { target: { value: searchTerm } });
        expect(input).toHaveValue(searchTerm);
    });
});
