/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import logo from 'shop/resources/img/base/archibald-logo.png';

import Link from 'shop/client/components/atoms/link/Link';
import Container from 'shop/client/features/layout/components/container/Container';
import Flex from 'shop/client/features/layout/components/flex/Flex';
import Grid from 'shop/client/features/layout/components/grid/Grid';

import Classes from 'shop/client/components/molecules/header/Header.scss';

function ReducedHeader() {
    return (
        <Container>
            <Grid columns="1fr 2fr 1fr" rows="auto auto" gap="5" align="center" justify="between" mt="5" mb="7">
                <Flex align="center" justify="center" className={Classes.logo}>
                    <Link className={Classes.logoLink} to="/">
                        <img className={Classes.logoIcon} src={logo} alt="Logo" />
                        RCHIBALD
                    </Link>
                </Flex>
            </Grid>
        </Container>
    );
}

export default ReducedHeader;
