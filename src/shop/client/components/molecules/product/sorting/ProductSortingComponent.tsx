/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Group } from '@radix-ui/react-select';

import { memo } from 'react';

import { DropdownOption } from 'shop/client/interfaces/base';

import { useProductSorting } from 'shop/client/hooks/product';

import Select from 'shop/client/components/atoms/select/Select';
import SelectItem from 'shop/client/components/atoms/select/item/SelectItem';

import Classes from 'shop/client/components/molecules/product/sorting/ProductSortingComponent.scss';

function ProductSortingComponent() {
    const { currentSorting, changeSorting, sortingOptions } = useProductSorting();
    const currentSortingLabel = sortingOptions.find((option: DropdownOption) => option.value === currentSorting)?.label ?? '';

    function renderOptions({ value, label }) {
        return (
            <SelectItem key={value} value={value}>
                {label}
            </SelectItem>
        );
    }

    return (
        <div className={Classes.sortingWrapper}>
            <Select value={currentSorting} label={currentSortingLabel} onValueChange={changeSorting}>
                <Group>{sortingOptions.map(renderOptions)}</Group>
            </Select>
        </div>
    );
}

export default memo(ProductSortingComponent);
