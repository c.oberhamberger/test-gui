/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { memo } from 'react';

import type { Product } from 'shop/client/interfaces/base';

import { useCarousel } from 'shop/client/hooks';
import { useProductsByCategory } from 'shop/client/hooks/product';

import CarouselArrow from 'shop/client/components/atoms/carousel/arrow/CarouselArrow';
import ProductTileComponent from 'shop/client/components/molecules/product/tile/ProductTileComponent';

import Classes from 'shop/client/components/molecules/product/carousel/ProductCarousel.scss';

interface Props {
    categoryId: string;
}

function ProductCarousel({ categoryId }: Props) {
    const { data: searchResult } = useProductsByCategory(categoryId);
    const { ref, isPreviousButtonDisabled, isNextButtonDisabled, scrollNext, scrollPrevious } = useCarousel({
        slidesToScroll: 'auto',
        containScroll: 'trimSnaps',
        useArrows: true
    });

    if (!searchResult?.products.length) {
        return null;
    }

    return (
        <div data-testid="ProductCarousel" className={Classes.carousel}>
            <CarouselArrow direction="left" size="big" disabled={isPreviousButtonDisabled} onClick={scrollPrevious} />
            <div className={Classes.viewport} ref={ref}>
                <div className={Classes.container}>
                    {searchResult.products.map((product: Product, index: number) => (
                        <div className={Classes.slide} key={index}>
                            <ProductTileComponent product={product} />
                        </div>
                    ))}
                </div>
            </div>
            <CarouselArrow direction="right" size="big" disabled={isNextButtonDisabled} onClick={scrollNext} />
        </div>
    );
}

export default memo(ProductCarousel);
