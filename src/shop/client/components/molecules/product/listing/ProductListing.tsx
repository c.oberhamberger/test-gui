/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { Product } from 'shop/client/interfaces/base';

import ProductTileComponent from 'shop/client/components/molecules/product/tile/ProductTileComponent';
import Column from 'shop/client/features/layout/components/column/Column';
import Row from 'shop/client/features/layout/components/row/Row';

import Classes from 'shop/client/components/molecules/product/listing/ProductListing.scss';

interface Props {
    products: Product[];
}

function ProductListing({ products }: Props) {
    if (!products.length) {
        return null;
    }

    function renderProducts(product: Product, index: number) {
        return (
            <Column key={`${product.code}-${index}`} columns="3">
                <div className={Classes.product}>
                    <ProductTileComponent product={product} />
                </div>
            </Column>
        );
    }

    return <Row mt="6">{products.map(renderProducts)}</Row>;
}

export default ProductListing;
