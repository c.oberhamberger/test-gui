/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';
import { useTranslate } from '@archibald/storefront';

import { getProductDetailBreadcrumbs } from 'shop/client/utils/helpers/product';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { useProduct } from 'shop/client/hooks/product';

import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Breadcrumbs from 'shop/client/components/molecules/breadcrumbs/Breadcrumbs';
import ProductCarousel from 'shop/client/components/molecules/product/carousel/ProductCarousel';
import ProductDetailCartComponent from 'shop/client/components/molecules/product/detail/cart/ProductDetailCartComponent';
import ProductDetailDescriptionComponent from 'shop/client/components/molecules/product/detail/description/ProductDetailDescriptionComponent';
import ProductGalleryComponent from 'shop/client/components/molecules/product/detail/gallery/ProductGalleryComponent';
import Column from 'shop/client/features/layout/components/column/Column';
import Row from 'shop/client/features/layout/components/row/Row';

function ProductDetailComponent() {
    const translate = useTranslate();
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);

    if (!product) {
        throw new Error('No product');
    }

    return (
        <>
            <Row>
                <Column>
                    <Breadcrumbs breadcrumbs={getProductDetailBreadcrumbs(product, translate('link.homepage'))} />
                </Column>
            </Row>
            <Row mt="6" mb="9">
                <Column columns="4">
                    <ProductGalleryComponent />
                </Column>

                <Column columns="4">
                    <ProductDetailDescriptionComponent />
                </Column>

                <Column columns="4">
                    <ProductDetailCartComponent />
                </Column>
            </Row>

            <Row mt="9" mb="5">
                <Column>
                    <Text headline="h2">
                        <Message code="category.recommended" />
                    </Text>
                    <ProductCarousel categoryId={product.categories[0].code} />
                </Column>
            </Row>
        </>
    );
}

export default ProductDetailComponent;
