/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';

import { useCallback, useEffect } from 'react';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { useCarousel } from 'shop/client/hooks';
import { useProduct } from 'shop/client/hooks/product';

import ProductGalleryImages from 'shop/client/components/molecules/product/detail/gallery/images/ProductGalleryImages';
import ProductGalleryThumbs from 'shop/client/components/molecules/product/detail/gallery/thumbs/ProductGalleryThumbs';

import Classes from 'shop/client/components/molecules/product/detail/gallery/ProductGalleryComponent.scss';

function ProductGalleryComponent() {
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);
    const {
        ref: mainRef,
        api: mainApi,
        selectedSlideIndex: selectedMainSlideIndex,
        setSelectedSlideIndex: setSelectedMainSlideIndex
    } = useCarousel({ skipSnaps: false });
    const {
        ref: thumbsRef,
        api: thumbsApi,
        selectedSlideIndex: selectedThumbSlideIndex,
        setSelectedSlideIndex: setSelectedThumbSlideIndex
    } = useCarousel({
        containScroll: 'keepSnaps',
        dragFree: true,
        axis: 'y'
    });

    const scrollTo = useCallback(
        (index: number) => {
            mainApi && mainApi.scrollTo(index);
            setSelectedMainSlideIndex(index);
        },
        [mainApi, selectedMainSlideIndex]
    );
    const onSelect = useCallback(() => {
        if (mainApi && thumbsApi) {
            const scrollSnap = mainApi.selectedScrollSnap();
            setSelectedThumbSlideIndex(scrollSnap);
            thumbsApi.scrollTo(scrollSnap);
        }
    }, [mainApi, thumbsApi, setSelectedThumbSlideIndex]);

    useEffect(() => {
        if (mainApi) {
            onSelect();
            mainApi.on('select', onSelect);
            mainApi.on('reInit', onSelect);
        }
    }, [mainApi, onSelect]);

    if (!product?.images?.length) {
        return null;
    }

    return (
        <div className={Classes.carousel}>
            <ProductGalleryThumbs ref={thumbsRef} images={product.images} onClick={scrollTo} selectedIndex={selectedThumbSlideIndex} />
            <ProductGalleryImages ref={mainRef} images={product.images} selectedIndex={selectedMainSlideIndex} />
        </div>
    );
}

export default ProductGalleryComponent;
