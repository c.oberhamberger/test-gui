/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import type { EmblaOptionsType } from 'embla-carousel';
import { ForwardedRef, forwardRef } from 'react';

import type { ProductImage } from 'shop/client/interfaces/base';

import Button from 'shop/client/components/atoms/button/Button';

import Classes from 'shop/client/components/molecules/product/detail/gallery/thumbs/ProductGalleryThumbs.scss';

interface Props {
    selectedIndex: number;
    images: ProductImage[];
    onClick: (index: number) => void;
    options?: EmblaOptionsType;
}

const ProductGalleryThumbs = forwardRef(({ images, selectedIndex, onClick }: Props, ref: ForwardedRef<HTMLDivElement>) => {
    const handleClick = (index: number) => () => {
        onClick(index);
    };

    if (images.length < 2) {
        return null;
    }

    return (
        <div className={Classes.carousel}>
            <div ref={ref} className={Classes.viewport} aria-roledescription="carousel">
                <div className={Classes.container}>
                    {images.map(({ url, alt = '' }, index: number) => (
                        <Button
                            key={`${url}${index}`}
                            aria-roledescription="slide"
                            aria-label={`${index + 1} of ${images.length}`}
                            className={Classes.slide}
                            onClick={handleClick(index)}
                        >
                            <img
                                className={classNamesHelper([Classes.slideImage, selectedIndex === index && Classes.slideImageSelected])}
                                src={url}
                                alt={alt}
                            />
                        </Button>
                    ))}
                </div>
            </div>
        </div>
    );
});

ProductGalleryThumbs.displayName = 'ProductGalleryThumbs';

export default ProductGalleryThumbs;
