/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ForwardedRef, forwardRef } from 'react';

import { ProductImage } from 'shop/client/interfaces/base';

import Classes from 'shop/client/components/molecules/product/detail/gallery/images/ProductGalleryImages.scss';

interface Props {
    images: ProductImage[];
    selectedIndex: number;
}

const ProductGalleryImages = forwardRef(({ images, selectedIndex }: Props, ref: ForwardedRef<HTMLDivElement>) => {
    return (
        <div className={Classes.carousel}>
            <div className={Classes.viewport} ref={ref} aria-roledescription="carousel">
                <div className={Classes.container}>
                    {images.map(({ url, alt = '' }, index: number) => (
                        <div className={Classes.slide} key={index}>
                            <img className={Classes.slideImage} src={url} alt={`${alt}${index === selectedIndex ? ' / Visible' : ''}`} />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
});

ProductGalleryImages.displayName = 'ProductGalleryImages';

export default ProductGalleryImages;
