/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DataRequest } from '@archibald/core';
import { fireEvent, renderAsync, screen } from '@archibald/testing';

import { productWithImages, productWithoutImages, productWithSingleImage } from 'shop/client/utils/helpers/mocks';

import type { Product } from 'shop/client/interfaces/base';

import * as productHooks from 'shop/client/hooks/product';

import ProductGalleryComponent from 'shop/client/components/molecules/product/detail/gallery/ProductGalleryComponent';

jest.mock('@archibald/core', () => ({
    ...jest.requireActual<typeof import('@archibald/core')>('@archibald/core'),
    useParams: jest.fn().mockReturnValue(52656256)
}));

const useProductSpy = (product: Product) =>
    jest.spyOn(productHooks, 'useProduct').mockReturnValue({
        data: product,
        error: null,
        isLoading: false,
        isDone: true,
        isError: false,
        isPrefetched: false,
        isStale: false,
        request: {} as DataRequest<any>,
        refetch: jest.fn(),
        prefetch: jest.fn()
    });

describe('<ProductGalleryComponent /> component:', () => {
    it('should display nothing if product does not have images', async () => {
        useProductSpy(productWithoutImages);
        await renderAsync(<ProductGalleryComponent />);
        const gallery = screen.queryByTestId('product-gallery');
        expect(gallery).toBeNull();
    });

    it('should display a single image', async () => {
        useProductSpy(productWithSingleImage);

        await renderAsync(<ProductGalleryComponent />);

        const images = screen.getAllByRole('img');
        expect(images).toHaveLength(1);
    });

    it('should display image gallery with all images', async () => {
        useProductSpy(productWithImages);

        await renderAsync(<ProductGalleryComponent />);

        const images = screen.getAllByRole('img');
        expect(images).toHaveLength(4);
    });

    it('should display correct image after click', async () => {
        useProductSpy(productWithImages);

        await renderAsync(<ProductGalleryComponent />);
        expect(screen.getByAltText(/\/ Visible/)).toHaveProperty('src', 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg');

        fireEvent.click(await screen.findByRole('button', { name: '2 of 2' }));
        expect(screen.getByAltText(/\/ Visible/)).toHaveProperty('src', 'https://netconomy.net/public/dji.jpg');

        fireEvent.click(await screen.findByRole('button', { name: '1 of 2' }));
        expect(screen.getByAltText(/\/ Visible/)).toHaveProperty('src', 'https://netconomy.net/public/gopro-hero-9-black-actioncam.jpg');
    });
});
