/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { useProduct } from 'shop/client/hooks/product';

import Text from 'shop/client/components/atoms/text/Text';
import ProductDetailDescriptionFeatures from 'shop/client/components/molecules/product/detail/description/features/ProductDetailDescriptionFeatures';
import ProductDetailDescriptionTabsComponent from 'shop/client/components/molecules/product/detail/description/tabs/ProductDetailDescriptionTabsComponent';

import Classes from 'shop/client/components/molecules/product/detail/description/ProductDetailDescriptionComponent.scss';

function ProductDetailDescriptionComponent() {
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);

    return (
        <>
            <div className={Classes.headline}>
                <Text headline="h1">{product?.name}</Text>
                <Text headline="h3">
                    {product?.brand?.name} - {product?.categories?.[0]?.name}
                </Text>
            </div>

            <div className={Classes.features}>
                <ProductDetailDescriptionFeatures count={4} />
            </div>

            <div className={Classes.tabs}>
                <ProductDetailDescriptionTabsComponent />
            </div>
        </>
    );
}

export default ProductDetailDescriptionComponent;
