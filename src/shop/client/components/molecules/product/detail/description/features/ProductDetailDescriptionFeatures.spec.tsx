/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import { productWithFeatures } from 'shop/client/utils/helpers/mocks';

import ProductDetailDescriptionFeatures from 'shop/client/components/molecules/product/detail/description/features/ProductDetailDescriptionFeatures';

jest.mock('shop/client/hooks/product/useProduct', () => ({
    useProduct: jest.fn(() => ({
        data: productWithFeatures
    }))
}));

jest.mock('@archibald/core', () => ({
    ...jest.requireActual<typeof import('@archibald/core')>('@archibald/core'),
    useParams: jest.fn().mockReturnValue(52656256)
}));

describe('<ProductDetailDescriptionFeatures/> component:', () => {
    it('should exist', async () => {
        const component = await renderAsync(<ProductDetailDescriptionFeatures count={4} />);
        expect(component).toBeDefined();
    });

    it('should render requested number of features', async () => {
        const { getAllByTestId } = await renderAsync(<ProductDetailDescriptionFeatures count={3} />);
        const features = getAllByTestId('feature');
        expect(features).toHaveLength(3);
    });
});
