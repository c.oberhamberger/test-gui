/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { SafeHTML } from '@archibald/client';
import { useParams } from '@archibald/core';
import { Content, List, Root, Trigger } from '@radix-ui/react-tabs';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { useProduct } from 'shop/client/hooks/product';

import Message from 'shop/client/components/atoms/message/Message';
import ProductDetailDescriptionFeatures from 'shop/client/components/molecules/product/detail/description/features/ProductDetailDescriptionFeatures';

import Classes from 'shop/client/components/molecules/product/detail/description/tabs/ProductDetailDescriptionTabsComponent.scss';

function ProductDetailDescriptionTabsComponent() {
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);

    return (
        <Root className={Classes.root} defaultValue="tab1">
            <List className={Classes.list} aria-label="Manage your account">
                <Trigger className={Classes.trigger} value="tab1">
                    <Message code="product.detail.details" />
                </Trigger>
                <Trigger className={Classes.trigger} value="tab2">
                    <Message code="product.detail.technicalfacts" />
                </Trigger>
                <Trigger className={Classes.trigger} value="tab3">
                    <Message code="product.detail.usage" />
                </Trigger>
            </List>
            <Content className={Classes.content} value="tab1">
                <SafeHTML content={product?.description} />
            </Content>
            <Content className={Classes.content} value="tab2">
                <ProductDetailDescriptionFeatures count={product?.features?.length || 0} />
            </Content>
            <Content className={Classes.content} value="tab3">
                <SafeHTML content={product?.description} />
            </Content>
        </Root>
    );
}

export default ProductDetailDescriptionTabsComponent;
