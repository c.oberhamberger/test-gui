/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';

import type { ProductFeature, ProductRouteParams } from 'shop/client/interfaces/base';

import { useProduct } from 'shop/client/hooks/product';

import Text from 'shop/client/components/atoms/text/Text';
import Box from 'shop/client/features/layout/components/box/Box';

interface Props {
    count: number;
}

function ProductDetailDescriptionFeatures({ count }: Props) {
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);

    function renderFeatures(feature: ProductFeature, index: number) {
        return (
            <Box key={feature.labelCode || index.toString()} data-testid="feature">
                <Text weight="bold">{`${feature.label}:  `}</Text>
                {feature.values[0].value}
            </Box>
        );
    }

    return <>{product?.features?.slice(0, count)?.map(renderFeatures)}</>;
}

export default ProductDetailDescriptionFeatures;
