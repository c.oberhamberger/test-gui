/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import { mockProduct } from 'shop/client/utils/helpers/mocks';

import ProductDetailCartComponent from 'shop/client/components/molecules/product/detail/cart/ProductDetailCartComponent';

jest.mock('shop/client/hooks/product/useProduct', () => ({
    useProduct: jest.fn(() => ({
        data: mockProduct
    }))
}));

jest.mock('@archibald/core', () => ({
    ...jest.requireActual<typeof import('@archibald/core')>('@archibald/core'),
    useParams: jest.fn().mockReturnValue(52656256)
}));

describe('<ProductDetailCartComponent> component:', () => {
    it('should exist', async () => {
        const component = await renderAsync(<ProductDetailCartComponent />);
        expect(component).toBeDefined();
    });

    it('should contain price, cart button/counter and stock status', async () => {
        const { getByText } = await renderAsync(<ProductDetailCartComponent />);

        let component = getByText('999.99€');
        expect(component).toBeDefined();

        component = getByText('[product.button.addtocart]');
        expect(component).toBeDefined();

        component = getByText('[product.availability.instock]');
        expect(component).toBeDefined();
    });
});
