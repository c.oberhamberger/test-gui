/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';

import { useState } from 'react';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { useCartMutation } from 'shop/client/hooks/cart';
import { useProduct } from 'shop/client/hooks/product';

import Button from 'shop/client/components/atoms/button/Button';
import NumberInput, { ChangeEvent } from 'shop/client/components/atoms/input/number/NumberInput';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Availability from 'shop/client/components/molecules/availability/Availability';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';

import Classes from 'shop/client/components/molecules/product/detail/cart/ProductDetailCartComponent.scss';

function ProductDetailCartComponent() {
    const { productId } = useParams<ProductRouteParams>();
    const { data: product } = useProduct(productId);
    const [quantity, setQuantity] = useState<number>(1);
    const { addToCart } = useCartMutation();

    function onQuantityChange(event: ChangeEvent) {
        setQuantity(event.value);
    }

    async function onAddToCartClick() {
        if (product) {
            await addToCart(product, quantity);
        }
    }

    return (
        <div className={Classes.cart}>
            <Text size="large">
                <PriceComponent value={product?.price.value ?? 0} currencyIso={product?.price.currencyIso} />
            </Text>

            <div className={Classes.quantity}>
                <NumberInput onChange={onQuantityChange} min={1} value={quantity} fullWidth />
            </div>

            <div className={Classes.cartButton}>
                <Button testId="pdpAddToCart" appearance="primary" onClick={onAddToCartClick} fullWidth iconAfter="cart">
                    <Message code="product.button.addtocart" />
                </Button>
            </div>

            <div className={Classes.availability}>
                <Availability isInStock={true} />
            </div>
        </div>
    );
}

export default ProductDetailCartComponent;
