/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { Product } from 'shop/client/interfaces/base';

import { useCartMutation } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Image from 'shop/client/components/atoms/image/Image';
import Link from 'shop/client/components/atoms/link/Link';
import Message from 'shop/client/components/atoms/message/Message';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';

import Classes from 'shop/client/components/molecules/product/tile/ProductTileComponent.scss';

interface Props {
    product: Product;
}

function ProductTileComponent({ product }: Props) {
    const { addToCart } = useCartMutation();

    async function onAddToCartClick() {
        await addToCart(product, 1);
    }

    return (
        <div data-testid="productTile" className={Classes.product}>
            <Link to={`/p/${product.code}`} className={Classes.link}>
                <div className={Classes.details}>
                    <div className={Classes.image}>
                        <Image src={product.images?.[0].url ?? ''} />
                    </div>
                    <div className={Classes.name}>{product.name}</div>
                    <div>{product.categories?.[0].name ?? ''}</div>
                    <div>{product.brand?.name ?? ''}</div>
                </div>
                <div className={Classes.price}>
                    <PriceComponent value={product.price.value} currencyIso={product.price.currencyIso} />
                </div>
            </Link>
            <div className={Classes.button}>
                <Button data-testid="productTileAddToCart" appearance="primary" onClick={onAddToCartClick} fullWidth iconAfter="cart">
                    <Message code="product.button.addtocart" />
                </Button>
            </div>
        </div>
    );
}

export default ProductTileComponent;
