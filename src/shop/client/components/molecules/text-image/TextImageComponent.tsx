/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { SafeHTML } from '@archibald/client';

import type { ImageInterface, LinkInterface } from 'shop/client/interfaces/base';

import Image from 'shop/client/components/atoms/image/Image';
import Link from 'shop/client/components/atoms/link/Link';
import Text from 'shop/client/components/atoms/text/Text';

import Classes from 'shop/client/components/molecules/text-image/TextImageComponent.scss';

interface Props {
    image: ImageInterface;
    link: LinkInterface;
    title?: string;
    index?: number;
    shortText?: string;
}

function TextImageComponent({ image, link, title, shortText, index = 0 }: Props) {
    return (
        <Link to={link.url} className={Classes.wrapper} data-testid="TextImageComponent">
            <Image src={image.src} height={image.ratio?.height ?? 530} width={image.ratio?.width ?? 785} lazy={index > 0} />
            {title && (
                <div className={Classes.textContainer}>
                    <Text headline="h3">{title}</Text>
                    {shortText && (
                        <div className={Classes.text}>
                            <SafeHTML content={shortText} />
                        </div>
                    )}
                </div>
            )}
        </Link>
    );
}

export default TextImageComponent;
