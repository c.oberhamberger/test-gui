/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import { LinkTarget } from 'shop/client/interfaces/base';

import TextImageComponent from 'shop/client/components/molecules/text-image/TextImageComponent';

const TestLink = {
    visible: true,
    url: 'http://localhost/de',
    external: false,
    name: 'TestLink',
    targetName: LinkTarget.SAME
};

const TestImage = {
    src: 'http://localhost/image.jpg'
};

const defaultProps = {
    className: 'test--class',
    title: 'Text Title',
    image: TestImage,
    link: TestLink,
    shortText: 'Kurzer Text'
};

describe('<TextImageComponent /> component:', () => {
    describe('Rendering TextImageComponent', () => {
        it('should exist ', async () => {
            const component = await renderAsync(<TextImageComponent {...defaultProps} />);
            expect(component).toBeDefined();
        });

        it('should have a ShortText', async () => {
            const component = await renderAsync(<TextImageComponent {...defaultProps} />);
            const shortText = await component.findByText(defaultProps.shortText);
            expect(shortText).toHaveTextContent(defaultProps.shortText);
        });

        it('should not have a ShortText', async () => {
            const component = await renderAsync(<TextImageComponent {...defaultProps} shortText="" />);
            const shortText = component.queryByText(defaultProps.shortText);
            expect(shortText).toBeNull();
        });
    });
});
