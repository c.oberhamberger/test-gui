/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useTranslate } from '@archibald/storefront';

import type { CartEntry } from 'shop/client/interfaces/base';

import { useCartMutation } from 'shop/client/hooks/cart';

import Icon from 'shop/client/components/atoms/icon/Icon';
import Image from 'shop/client/components/atoms/image/Image';
import Checkbox from 'shop/client/components/atoms/input/checkbox/Checkbox';
import NumberInput, { type ChangeEvent } from 'shop/client/components/atoms/input/number/NumberInput';
import Link from 'shop/client/components/atoms/link/Link';
import Availability from 'shop/client/components/molecules/availability/Availability';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';
import Box from 'shop/client/features/layout/components/box/Box';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/molecules/cart/entry/CartEntryComponent.scss';

interface CartEntryComponentProps {
    entry: CartEntry;
}

function CartEntryComponent({ entry }: CartEntryComponentProps) {
    const { addToCart, removeFromCart } = useCartMutation();
    const translate = useTranslate();

    async function updateEntryCount(event: ChangeEvent) {
        if (event.value === 0) {
            await removeFromCart(entry);
            return;
        }

        if (entry.quantity !== event.value) {
            await addToCart(entry.product, event.direction === 'increment' ? 1 : -1);
        }
    }

    async function removeEntry() {
        await removeFromCart(entry);
    }

    return (
        <Flex
            data-entrynumber={entry.entryNumber}
            align="center"
            justify="between"
            gap={{ initial: '3', sm: '6' }}
            direction={{ initial: 'column', md: 'row' }}
        >
            <Flex align="center" justify="between" gap={{ initial: '3', sm: '6' }} direction={{ initial: 'column', md: 'row' }}>
                <Flex align="center" gap={{ initial: '3', sm: '6' }} direction={{ initial: 'column', sm: 'row' }}>
                    <Flex align="center" gap="6" direction="row">
                        <Box className={Classes.remove} data-testid="remove" onClick={removeEntry}>
                            <Icon type="close" size="extra-large" />
                        </Box>
                        <Box className={Classes.image}>
                            <Link to={entry.product.url ?? ''}>
                                <Image src={entry.product.images?.[0]?.url ?? ''} />
                            </Link>
                        </Box>
                    </Flex>
                    <Box>
                        <Box className={Classes.name}>
                            <Link to={entry.product.url ?? ''}>{entry.product.name}</Link>
                        </Box>
                        <Box mt="2">{entry.product.brand?.name ?? ''}</Box>
                    </Box>
                </Flex>
            </Flex>

            <Flex align="center" direction={{ initial: 'column', xs: 'row' }} gap={{ initial: '3', xs: '8' }}>
                <Box>
                    <Box mb="3">
                        <Availability isInStock={true} />
                    </Box>
                    <Checkbox id="giftWrapCheckbox" label={translate('cart.summary.giftwrap')} />
                </Box>
                <NumberInput value={entry.quantity} min={0} testId="quantity" onChange={updateEntryCount} />
                <Box className={Classes.price}>
                    <PriceComponent currencyIso={entry.product.price.currencyIso} value={entry.totalPrice.value} />
                </Box>
            </Flex>
        </Flex>
    );
}

export default CartEntryComponent;
