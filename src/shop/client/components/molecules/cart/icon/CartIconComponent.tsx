/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ForwardedRef, forwardRef, Suspense } from 'react';

import Badge from 'shop/client/components/atoms/badge/Badge';
import Icon from 'shop/client/components/atoms/icon/Icon';
import Link from 'shop/client/components/atoms/link/Link';
import CartBadge from 'shop/client/components/molecules/cart/badge/CartBadge';

const CartIconComponent = forwardRef((props, ref: ForwardedRef<HTMLAnchorElement>) => {
    return (
        <Link to="/cart" ref={ref} {...props}>
            <Icon data-testid="carticon" type="cart" size="extra-large" />
            <Suspense fallback={<Badge text="..." />}>
                <CartBadge />
            </Suspense>
        </Link>
    );
});

CartIconComponent.displayName = 'CartIconComponent';

export default CartIconComponent;
