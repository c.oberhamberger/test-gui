/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { PriceType } from '@archibald/storefront/lib/node/constants/price';
import { renderAsync, screen } from '@archibald/testing';

import { UrlConstants } from 'shop/client/constants';

import { Cart } from 'shop/client/interfaces/base';

import { useCart } from 'shop/client/hooks/cart';

import CartComponent from 'shop/client/components/molecules/cart/cart/CartComponent';

jest.mock('shop/client/hooks/cart');

const emptyCart: Cart = {
    entries: [],
    guid: 'cart-guid',
    code: 'cart-code',
    totalItems: 0,
    totalPrice: {
        currencyIso: 'EUR',
        priceType: PriceType.BUY,
        formattedValue: '0 €',
        value: 0
    },
    user: {
        id: '123',
        uid: '123',
        name: 'Archibald'
    }
};

describe('<CartComponent/> component:', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('shows a link to the default category when the cart is empty', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: emptyCart, isLoading: false });

        await renderAsync(<CartComponent max={10} />);

        const link = screen.getByRole('link', { name: '[cart.popup.recommendations]' });

        expect(link).toHaveAttribute('href', UrlConstants.defaultCategory);
    });

    it('shows loading spinner while loading', async () => {
        (useCart as jest.Mock).mockReturnValueOnce({ data: null, isLoading: true });

        await renderAsync(<CartComponent max={10} />);

        expect(screen.getByRole('alert')).toBeInTheDocument();
    });
});
