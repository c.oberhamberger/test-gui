/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { UrlConstants } from 'shop/client/constants';

import type { CartEntry } from 'shop/client/interfaces/base';

import { useCart } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Loader from 'shop/client/components/atoms/loader/Loader';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import CartEntryComponent from 'shop/client/components/molecules/cart/entry/CartEntryComponent';
import CartSummaryComponent from 'shop/client/components/molecules/cart/summary/CartSummaryComponent';
import Flex from 'shop/client/features/layout/components/flex/Flex';

function CartComponent({ max }) {
    const { data: cart, isLoading } = useCart(false);

    if (isLoading && !cart?.entries?.length) {
        return <Loader type="page" />;
    }

    if (!cart?.entries?.length) {
        return (
            <Flex justify="center" align="center" direction="column" gap="8" mt="9" mb="9">
                <Text size="large" align="center">
                    <Message code="cart.popup.empty" simpleText />
                </Text>
                <Button appearance="primary" url={UrlConstants.defaultCategory}>
                    <Message code="cart.popup.recommendations" />
                </Button>
            </Flex>
        );
    }

    return (
        <Flex direction="column" gap="8" mt="8" mb="4">
            {cart.entries.slice(0, max).map((entry: CartEntry) => (
                <CartEntryComponent key={entry.entryNumber} entry={entry} />
            ))}
            <CartSummaryComponent />
        </Flex>
    );
}

export default CartComponent;
