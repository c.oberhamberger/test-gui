/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useTranslate } from '@archibald/storefront';

import { useCart } from 'shop/client/hooks/cart';

import Button from 'shop/client/components/atoms/button/Button';
import Input from 'shop/client/components/atoms/input/input/Input';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';
import Box from 'shop/client/features/layout/components/box/Box';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/molecules/cart/summary/CartSummaryComponent.scss';

function CartSummaryComponent() {
    const { data: cart } = useCart(false);
    const translate = useTranslate();

    if (!cart?.totalItems) {
        return null;
    }

    return (
        <Flex direction="column">
            <Flex direction="column" align={{ initial: 'center', md: 'end' }} mb="8">
                {!!cart?.totalPrice && (
                    <Flex direction="row" align="end" mb="5">
                        <Text weight="medium">
                            <Message code="cart.summary.total" simpleText />
                            <Box ml="5" display="inline-block">
                                <PriceComponent currencyIso={cart.totalPrice.currencyIso} value={cart.totalPrice.value} />
                            </Box>
                        </Text>
                    </Flex>
                )}
                <Box className={Classes.coupon}>
                    <Input placeholder={translate('cart.summary.coupon')} icon="caret-right" />
                </Box>
            </Flex>
            <Box className={Classes.button}>
                <Button url="/checkout" fullWidth appearance="primary" iconBefore="cart">
                    <Message code="cart.summary.continue" />
                </Button>
            </Box>
        </Flex>
    );
}

export default CartSummaryComponent;
