/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

import Icon from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/molecules/user/icon/UserIconComponent.scss';

function UserIconComponent() {

    return (
        <div className={Classes.user}>
            <Icon type={'user'} size="extra-large" />
        </div>
    );
}

export default UserIconComponent;
