/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { UrlHelper } from '@archibald/core';

import Button from 'shop/client/components/atoms/button/Button';
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Container from 'shop/client/features/layout/components/container/Container';
import Flex from 'shop/client/features/layout/components/flex/Flex';

function pageReload() {
    UrlHelper.fullPageReload(null);
}

interface Props {
    retryAction?: () => void;
    staticText?: {
        headline: string;
        button: string;
    };
}

function FatalError(props: Props) {
    return (
        <Container>
            <Flex direction="column" gap="5" align="center" justify="center">
                <Text size="large" weight="light">
                    {props?.staticText?.headline ?? <Message code="api.error.general" />}
                </Text>
                <Button appearance="primary" onClick={pageReload}>
                    {props?.staticText?.button ?? <Message code="api.error.general.retry" />}
                </Button>
            </Flex>
        </Container>
    );
}

export default FatalError;
