/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import FatalError from 'shop/client/components/molecules/error/fatal/FatalError';

const generalTextKey = '[api.error.general]';
const generalRetryKey = '[api.error.general.retry]';

describe('<FatalError />', () => {
    it('should render', async () => {
        const { getByText, container } = await renderAsync(<FatalError />);
        const general = getByText(generalTextKey);
        const retry = getByText(generalRetryKey);
        expect(container).toBeDefined();
        expect(general).toHaveTextContent(generalTextKey);
        expect(retry).toHaveTextContent(generalRetryKey);
    });
});
