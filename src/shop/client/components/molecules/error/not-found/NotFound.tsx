/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import Message from 'shop/client/components/atoms/message/Message';
import Text from 'shop/client/components/atoms/text/Text';
import Container from 'shop/client/features/layout/components/container/Container';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/molecules/error/not-found/NotFound.scss';

function NotFound() {
    return (
        <Container>
            <Flex align="center" justify="center" direction="column" gap="6">
                <Flex align="center" justify="center" className={Classes.sign}>
                    <Text size="extra-large" weight="light">
                        404
                    </Text>
                </Flex>
                <Text size="large" weight="light">
                    <Message code="error.404" />
                </Text>
            </Flex>
        </Container>
    );
}

export default NotFound;
