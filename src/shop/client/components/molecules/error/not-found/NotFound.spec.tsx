/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import NotFound from 'shop/client/components/molecules/error/not-found/NotFound';

describe('<Error404 />', () => {
    it('should render 404', async () => {
        await renderAsync(<NotFound />);
        const notFoundSign = '404';
        const text = screen.getByText(notFoundSign);
        expect(text).toHaveTextContent(notFoundSign);
    });
});
