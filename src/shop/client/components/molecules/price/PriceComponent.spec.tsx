/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import PriceComponent from 'shop/client/components/molecules/price/PriceComponent';

describe('<PriceComponent /> component:', () => {
    it('should display correct value and currency symbol', async () => {
        await renderAsync(<PriceComponent currencyIso={'EUR'} value={99.99} />);
        const text = screen.getByText('99.99€');
        expect(text).toBeDefined();
    });

    it('should display currency symbol before value', async () => {
        await renderAsync(<PriceComponent currencyIso={'EUR'} value={99.99} currencyPosition="before" />);
        const container = screen.getByText('€99.99');
        expect(container).toBeDefined();
    });

    it('should display value with 3 fractionDigits', async () => {
        await renderAsync(<PriceComponent currencyIso={'EUR'} value={99} fractionDigits={3} />);
        const container = screen.getByText('99.000€');
        expect(container).toBeDefined();
    });
});
