/**********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { getCurrencySymbol } from 'shop/client/utils/helpers/currency';

interface Props {
    currencyIso?: string;
    currencyPosition?: 'before' | 'after';
    fractionDigits?: number;
    value: number;
}

function PriceComponent({ currencyIso, currencyPosition = 'after', fractionDigits, value }: Props) {
    return (
        <>
            {currencyIso && currencyPosition === 'before' && getCurrencySymbol(currencyIso)}
            {value.toFixed(fractionDigits || 2)}
            {currencyIso && currencyPosition === 'after' && getCurrencySymbol(currencyIso)}
        </>
    );
}

export default PriceComponent;
