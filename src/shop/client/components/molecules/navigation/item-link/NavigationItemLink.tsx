/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Link as NavigationMenuLink } from '@radix-ui/react-navigation-menu';

import { AnchorHTMLAttributes, ForwardedRef, forwardRef, type PropsWithChildren } from 'react';

import Link from 'shop/client/components/atoms/link/Link';
import Text from 'shop/client/components/atoms/text/Text';

import Classes from 'shop/client/components/molecules/navigation/item-link/NavigationItemLink.scss';

interface Props extends PropsWithChildren, AnchorHTMLAttributes<unknown> {
    title: string;
    to: string;
}

const NavigationItemLink = forwardRef(({ children, title, to, ...props }: Props, forwardedRef: ForwardedRef<HTMLAnchorElement>) => (
    <NavigationMenuLink asChild>
        <Link to={to} className={Classes.link} {...props} ref={forwardedRef}>
            <Text>{title}</Text>
            {children && <div>{children}</div>}
        </Link>
    </NavigationMenuLink>
));

NavigationItemLink.displayName = 'NavigationItemLink';

export default NavigationItemLink;
