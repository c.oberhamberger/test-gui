/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { SafeHTML } from '@archibald/client';
import { Content, Item, Trigger } from '@radix-ui/react-navigation-menu';

import type { NavigationInterface } from 'shop/client/interfaces/base';

import NavigationItemLink from 'shop/client/components/molecules/navigation/item-link/NavigationItemLink';

import Classes from 'shop/client/components/molecules/navigation/item/NavigationItem.scss';

interface Props {
    navigation: NavigationInterface;
    mobile?: boolean;
}

function NavigationItem({ navigation, mobile = false }: Props) {
    const { to = '', title = '', children = [] } = navigation ?? {};

    function renderNavTrigger() {
        if (!children.length || mobile) {
            return <NavigationItemLink className={Classes.link} to={to} title={title} />;
        }

        return (
            <Trigger className={Classes.trigger}>
                <NavigationItemLink className={Classes.link} to={to} title={title} />
            </Trigger>
        );
    }

    function renderNavLinks({ id, title = '', to = '', className = '', style, content }: NavigationInterface) {
        return (
            <NavigationItemLink key={id} className={Classes[className]} to={to} title={title} style={style}>
                <SafeHTML content={content} />
            </NavigationItemLink>
        );
    }
    function renderNavContent() {
        if (!children.length || mobile) {
            return null;
        }

        return (
            <Content className={Classes.content}>
                <ul className={Classes.contentList}>{children.map(renderNavLinks)}</ul>
            </Content>
        );
    }

    return (
        <Item className={Classes.item}>
            {renderNavTrigger()}
            {renderNavContent()}
        </Item>
    );
}
export default NavigationItem;
