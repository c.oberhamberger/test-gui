/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Indicator, List, Root, Viewport } from '@radix-ui/react-navigation-menu';

import type { NavigationInterface } from 'shop/client/interfaces/base';

import NavigationItem from 'shop/client/components/molecules/navigation/item/NavigationItem';

import Classes from 'shop/client/components/molecules/navigation/main/MainNavigation.scss';

interface Props {
    navigation: NavigationInterface[];
    mobile?: boolean;
}

function MainNavigation({ navigation, mobile = false }: Props) {
    return (
        <Root data-testid="mainNavigation" className={Classes.root}>
            <List className={Classes.list}>
                {navigation.map((item: NavigationInterface) => (
                    <NavigationItem key={item.id} navigation={item} mobile={mobile} />
                ))}
                <Indicator className={Classes.indicator}>
                    <div className={Classes.arrow} />
                </Indicator>
            </List>
            <div className={Classes.viewportPosition}>
                <Viewport className={Classes.viewport} />
            </div>
        </Root>
    );
}

export default MainNavigation;
