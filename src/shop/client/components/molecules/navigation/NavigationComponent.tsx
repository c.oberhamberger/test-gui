/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { mockNavigation as navigation } from 'shop/client/utils/helpers/mocks';

import Drawer from 'shop/client/components/atoms/drawer/Drawer';
import Icon from 'shop/client/components/atoms/icon/Icon';
import MainNavigation from 'shop/client/components/molecules/navigation/main/MainNavigation';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/molecules/navigation/NavigationComponent.scss';

function NavigationComponent() {
    return (
        <>
            <Flex align="center" justify="start" display={{ md: 'none' }}>
                <Drawer.Root>
                    <Drawer.Trigger>
                        <Icon type="burger" testId="burgerMenuButton" size="large" />
                    </Drawer.Trigger>
                    <Drawer.Content title="Archibald" className={Classes.drawer}>
                        <MainNavigation navigation={navigation} mobile />
                    </Drawer.Content>
                </Drawer.Root>
            </Flex>
            <Flex align="center" justify="center" display={{ initial: 'none', md: 'flex' }}>
                <MainNavigation navigation={navigation} />
            </Flex>
        </>
    );
}

export default NavigationComponent;
