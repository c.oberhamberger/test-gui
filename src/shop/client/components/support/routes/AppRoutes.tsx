/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Outlet, Route, Routes } from '@archibald/core';

import { memo } from 'react';

import Link from 'shop/client/components/atoms/link/Link';
import CartPage from 'shop/client/components/pages/cart';
import CheckoutPage from 'shop/client/components/pages/checkout';
import NotFoundPage from 'shop/client/components/pages/error/not-found';
import CMSPage from 'shop/client/components/pages/page/cms';
import HomePage from 'shop/client/components/pages/page/home';
import ProductDetailPage from 'shop/client/components/pages/product/product-detail';
import ProductListingPage from 'shop/client/components/pages/product/product-listing';
import SearchPage from 'shop/client/components/pages/search';
import ThankYouPage from 'shop/client/components/pages/thankyou';
import PreviewPage from 'shop/client/components/support/smart-edit/page';
import { DefaultFallback, Layout } from 'shop/client/components/support/templates/layout/Layout';

function TestHomePlaceholder() {
    return (
        <DefaultFallback>
            <div style={{ display: 'grid', gridGap: 30, gridTemplateColumns: 'repeat(2, 1fr)', padding: '0 50px' }}>
                <div style={{ height: 362, backgroundColor: 'lightgray', gridColumn: '1 / 3' }} />
                <div style={{ height: 500, backgroundColor: 'lightgray' }} />
                <div style={{ height: 500, backgroundColor: 'lightgray' }} />
            </div>
        </DefaultFallback>
    );
}

function AppRoutes() {
    return (
        // default fallback that will be used on every Route if none provided
        <Routes fallback={<DefaultFallback />}>
            {/*
            Layout - shared layout across all the child Routes (they will be rendered inside it)
            fallback - if provided will overwrite the one from <Routes /> and behave the same way.
            */}
            <Route path="/:language?" element={<Layout />}>
                {/* fallback here is unique for HomePage */}
                <Route index element={<HomePage />} fallback={<TestHomePlaceholder />} />
                <Route path="c/:categoryId(\d+)" element={<ProductListingPage />} />
                <Route path="p/:productId(\d+)" element={<ProductDetailPage />} />
                <Route path="sa/:productId(\d+)" element={<ProductDetailPage />} />
                <Route path="s">
                    <Route path=":first-:second" element={<SearchPage />} />
                    <Route index element={<SearchPage />} />
                </Route>
                <Route path="cp/:pageLabel" element={<CMSPage />} />
                <Route path="cart" element={<CartPage />} />
                <Route path="checkout" element={<CheckoutPage />} />
                <Route path="thankyou" element={<ThankYouPage />} />
                <Route path="nested-products/*" element={<NestedProductRoutes />} />
                <Route path="cx-preview" element={<PreviewPage />} />
                <Route path="*" element={<NotFoundPage />} />
            </Route>
        </Routes>
    );
}

// Components below are purely for presentational purpose of Router functionality
function NestedProductRoutes() {
    return (
        <Routes fallback={<DefaultFallback />}>
            <Route path="p" element={<PDPLayout />}>
                <Route path=":productId(\d+)" element={<ProductDetailPage />} />
                <Route path="static-unordered" element={<DummyStaticPDP />} />
                <Route index element={<DummyPDPIndexPage />} />
            </Route>
            <Route path="*" element={<NotFoundPage />} />
        </Routes>
    );
}

function PDPLayout() {
    return (
        <>
            <Link to="static-unordered">To static PDP route</Link>
            <Link to="p">To PDP Index Route ??</Link>
            <Link to="793447800000">To PDP</Link>

            <Outlet />
        </>
    );
}

function DummyStaticPDP() {
    return <h1>Welcome to Dummy static PDP Route</h1>;
}

function DummyPDPIndexPage() {
    return <h1>Welcome to Dummy PDP Index Route</h1>;
}

export default memo(AppRoutes);
