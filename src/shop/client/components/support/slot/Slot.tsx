/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { CMSPosition } from 'shop/client/constants';
import type { CMSComponentTypeCode } from 'shop/client/constants';

import type { CMSSlot, StaticSlotComponentInterface } from 'shop/client/interfaces/base';

import { usePage } from 'shop/client/hooks/cms';
import { useSmartEdit } from 'shop/client/hooks/smart-edit';

import DynamicComponent from 'shop/client/components/support/dynamic-component/DynamicComponent';

interface Props {
    position: CMSPosition;
    contentSlots?: CMSSlot[];
    exclude?: CMSComponentTypeCode[];
    staticSlotComponentProp?: StaticSlotComponentInterface;
}

function Slot(props: Props) {
    const { position, contentSlots, staticSlotComponentProp, exclude = [] } = props;
    const page = usePage();

    const { getDomContractAttributes } = useSmartEdit();

    let slots: CMSSlot[];
    if (contentSlots) {
        slots = contentSlots;
    } else {
        slots = page?.contentSlots;
    }

    const slot = slots?.find((currentSlot) => currentSlot.position === position);
    const components = slot?.components ?? [];

    if (!slot || !Array.isArray(components)) {
        return null;
    }

    const componentsToRender = components.filter((component) => !exclude.includes(component.typeCode));
    const domContractAttributes = getDomContractAttributes({ typeCode: 'ContentSlot', ...slot }, page);

    const renderedComponents = componentsToRender.map((component, index) => {
        return <DynamicComponent key={`${component.typeCode}-${index}`} component={{ ...component, index, staticSlotComponentProp }} />;
    });

    if (domContractAttributes) {
        return (
            <div data-testid="smartedit-wrapper" className="smartEditComponent" {...domContractAttributes}>
                {renderedComponents}
            </div>
        );
    }

    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{renderedComponents}</>;
}

export default Slot;
