/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSParam, CMSProvider } from '@archibald/storefront';
import { renderAsync, TestingOptionProps } from '@archibald/testing';

import { mockCMS, mockPages } from 'shop/client/utils/helpers/mocks';

import { CMSPosition } from 'shop/client/constants';

import Slot from 'shop/client/components/support/slot/Slot';

import { CustomState } from 'shop/client/reducers';

interface Props {
    position: CMSPosition;
    inline?: boolean;
}

const defaultProps = {
    position: CMSPosition.CONTENT
};

const defaultOptions: TestingOptionProps<CustomState> = {
    context: {
        state: {
            cms: mockCMS
        }
    }
};

const ticketIdOptions: TestingOptionProps<CustomState> = {
    context: {
        state: {
            cms: {
                ...mockCMS,
                [CMSParam.CMS_TICKET_ID]: '123'
            }
        }
    }
};

const page = mockPages.test123;

describe('<Slot /> component:', () => {
    const WrappedComponent = (props: Props) => {
        return (
            <CMSProvider page={page}>
                <Slot {...props} />
            </CMSProvider>
        );
    };

    it('should exist ', async () => {
        const { container } = await renderAsync(<WrappedComponent {...defaultProps} />, defaultOptions);
        expect(container).toBeDefined();
    });

    it('should render empty ', async () => {
        const { container } = await renderAsync(<WrappedComponent {...defaultProps} position={CMSPosition.BOTTOM_FOOTER} />, defaultOptions);
        expect(container).toBeDefined();
        expect(container).toBeEmptyDOMElement();
    });

    it('should wrapp with cmsWrapper ', async () => {
        const { container, getByTestId } = await renderAsync(<WrappedComponent {...defaultProps} />, ticketIdOptions);
        expect(container).toBeDefined();
        getByTestId('smartedit-wrapper');
    });
});
