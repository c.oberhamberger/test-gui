/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSProvider } from '@archibald/storefront';
import { TestingContext, renderAsync } from '@archibald/testing';

import { createMemoryHistory } from 'history';
import { mockSrchOptPage, mockCMS, mockPages } from 'shop/client/utils/helpers/mocks';

import HeadManager from 'shop/client/components/support/head-manager/HeadManager';

import type { CustomState } from 'shop/client/reducers';

const mockHistory = createMemoryHistory();

const context: TestingContext<CustomState> = {
    state: {
        cms: {
            ...mockCMS
        }
    },
    history: mockHistory
};

const page = {
    ...mockPages.test123,
    searchEngineOptimizations: mockSrchOptPage
};

describe('<HeadManager /> component:', () => {
    beforeEach(() => {
        process.env.APP_CODE = 'client';
    });

    it('should exist ', async () => {
        await renderAsync(
            <CMSProvider page={page} loading={false}>
                <HeadManager />
            </CMSProvider>,
            context
        );
        const description = document.querySelector('meta[name="description"]');
        const robots = document.querySelector('meta[name="robots"]');
        expect(document.title).toEqual(mockSrchOptPage.title);
        expect(description).toHaveAttribute('content', mockSrchOptPage.description);
        expect(robots).toHaveAttribute('content', 'NOINDEX, NOFOLLOW');
    });
});
