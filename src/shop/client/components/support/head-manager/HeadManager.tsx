/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Head } from '@archibald/client';
import { HttpCode, Navigate, ParamsHelper, UrlHelper, useLocation } from '@archibald/core';
import { selectCmsTicketId } from '@archibald/storefront';

import { useSelector } from 'react-redux';

import { CMSParam, CommonConstants } from 'shop/client/constants';

import Config from 'shop/client/config/config';

import { usePage } from 'shop/client/hooks/cms';

const baseUrl = Config.get('app.canonicalBaseUrl');

function HeadManager() {
    const page = usePage();
    const location = useLocation();
    const cmsTicketId = useSelector(selectCmsTicketId);
    const index = page?.searchEngineOptimizations?.index ?? CommonConstants.NO_INDEX;
    const follow = page?.searchEngineOptimizations?.follow ?? CommonConstants.FOLLOW;
    const robotsContent = `${index}, ${follow}`.toUpperCase();

    const searchEngineOptimizations = page?.searchEngineOptimizations;

    if (process.env.APP_CODE === 'server') {
        // set the correct url for "autocorrect" of urls
        let redirectUrl = '';
        const pathname = location.pathname;
        const search = location.search;

        if (redirectUrl && `${pathname}${search}` !== redirectUrl) {
            redirectUrl = ParamsHelper.create(redirectUrl, { [CMSParam.CMS_TICKET_ID]: cmsTicketId });

            return <Navigate statusCode={HttpCode.CODE_301} to={redirectUrl} />;
        }
    }

    return (
        <Head>
            <title>{searchEngineOptimizations?.title ?? ''}</title>
            <meta name="description" content={searchEngineOptimizations?.description ?? ''} />
            <meta name="robots" content={robotsContent} />
            <link
                rel="canonical"
                href={
                    UrlHelper.isAbsoluteHttpUrl(searchEngineOptimizations?.canonical ?? '')
                        ? searchEngineOptimizations?.canonical ?? ''
                        : `${baseUrl}${searchEngineOptimizations?.canonical ?? ''}`
                }
            />
        </Head>
    );
}

export default HeadManager;
