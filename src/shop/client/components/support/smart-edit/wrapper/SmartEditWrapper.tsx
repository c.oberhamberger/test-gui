/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { selectCmsTicketId } from '@archibald/storefront';

import type { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import type { ColumnsSizeType } from 'shop/client/features/layout/interfaces';

import type { CMSComponentTypeCode } from 'shop/client/constants';

import type { CMSPage, ISmartEditAttributes } from 'shop/client/interfaces/base';

// no barrel import because of "wrong" circular dependency warning from metro
import { usePage } from 'shop/client/hooks/cms';

import Column from 'shop/client/features/layout/components/column/Column';

import Classes from 'shop/client/components/support/smart-edit/wrapper/SmartEditWrapper.scss';

interface SmartEditBaseComponent {
    typeCode: CMSComponentTypeCode | 'ContentSlot';
    catalogVersionUuid?: string;
}

export interface SmartEditComponent extends SmartEditBaseComponent {
    uid?: string;
    uuid?: string;
}

export interface SmartEditSlot extends SmartEditBaseComponent {
    slotId?: string;
    slotUuid?: string;
}

export type SmartEditComponentOrSlot = SmartEditComponent | SmartEditSlot;

function isSlot(componentOrSlot: SmartEditComponentOrSlot): componentOrSlot is SmartEditSlot {
    return !!((componentOrSlot as SmartEditSlot).slotUuid && (componentOrSlot as SmartEditSlot).slotId);
}

export function getAttributes(componentOrSlot: SmartEditComponentOrSlot, page: CMSPage, cmsTicketId: string | null) {
    let uid: string;
    let uuid: string;
    const typeCode = componentOrSlot.typeCode;
    const catalogVersionUuid = componentOrSlot.catalogVersionUuid;
    if (isSlot(componentOrSlot)) {
        uid = componentOrSlot.slotId ?? '';
        uuid = componentOrSlot.slotUuid ?? '';
    } else {
        uid = componentOrSlot.uid ?? '';
        uuid = componentOrSlot.uuid ?? '';
    }

    return cmsTicketId && typeCode && uid && uuid && page?.catalogVersionUuid
        ? {
              'data-smartedit-component-type': typeCode,
              'data-smartedit-component-id': uid,
              'data-smartedit-component-uuid': uuid,
              'data-smartedit-catalog-version-uuid': catalogVersionUuid ?? page?.catalogVersionUuid
          }
        : null;
}

interface SmartEditProps extends SmartEditComponent {
    columns?: ColumnsSizeType;
    children: ReactNode;
}

function CmsSmartEditWrapper(props: SmartEditProps) {
    const { children, columns } = props;

    const page = usePage();
    const cmsTicketId = useSelector(selectCmsTicketId);

    const columnAttributes = getAttributes(props, page, cmsTicketId) as ISmartEditAttributes;

    if (columnAttributes) {
        return (
            <Column className={Classes.component} columns={columns} attributes={columnAttributes}>
                {children}
            </Column>
        );
    }
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{children}</>;
}

export default CmsSmartEditWrapper;
