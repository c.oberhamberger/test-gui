/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSParam, CMSProvider } from '@archibald/storefront';
import { renderAsync, TestingContext } from '@archibald/testing';

import type { FunctionComponent } from 'react';
import { mockComponent, mockPages } from 'shop/client/utils/helpers/mocks';

import type { CMSComponent } from 'shop/client/interfaces/base';

import CmsSmartEditWrapper from 'shop/client/components/support/smart-edit/wrapper/SmartEditWrapper';

import type { CustomState } from 'shop/client/reducers';

const testComponent = 'Component';
const Component: FunctionComponent<CMSComponent> = () => <div>{testComponent}</div>;

const props = { ...mockComponent, uid: 'uid', uuid: 'uuid' };
const defaultContext: TestingContext<CustomState> = {
    state: {
        cms: {
            [CMSParam.CMS_TICKET_ID]: ''
        }
    }
};
const ticketContext: TestingContext<CustomState> = {
    state: {
        cms: {
            [CMSParam.CMS_TICKET_ID]: '123'
        }
    }
};

const page = {
    ...mockPages.test123,
    catalogVersionUuid: 'version'
};

describe('<SmartEditWrapper /> component:', () => {
    const WrappedComponent = (props) => {
        return (
            <CMSProvider page={page}>
                <CmsSmartEditWrapper {...props}>
                    <Component {...props} />
                </CmsSmartEditWrapper>
            </CMSProvider>
        );
    };

    describe('Rendering SmartEditWrapper', () => {
        it('should render with Column and cmsTicketId', async () => {
            const { getByText } = await renderAsync(<WrappedComponent {...props} />, ticketContext);
            const container = getByText(testComponent);
            expect(container.parentElement).toHaveClass('component');
            expect(container.parentElement).toHaveAttribute('data-smartedit-component-type', props.typeCode);
            expect(container.parentElement).toHaveAttribute('data-smartedit-component-id', props.uid);
            expect(container.parentElement).toHaveAttribute('data-smartedit-component-uuid', props.uuid);
            expect(container.parentElement).toHaveAttribute('data-smartedit-catalog-version-uuid', page.catalogVersionUuid);
            expect(container.parentElement).toBeDefined();
        });

        it('should render without cmsTicketId', async () => {
            const { getByText } = await renderAsync(<WrappedComponent {...props} />, defaultContext);
            const container = getByText(testComponent);
            expect(container).toBeDefined();
        });
    });
});
