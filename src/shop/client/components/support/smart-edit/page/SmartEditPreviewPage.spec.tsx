/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ConfigManager } from '@archibald/core';
import { CMSParam } from '@archibald/storefront';
import { renderAsync, TestingContext } from '@archibald/testing';

import { createBrowserHistory } from 'history';

import { usePreviewTicket } from 'shop/client/hooks/smart-edit';

import SmarteditPreviewPage from 'shop/client/components/support/smart-edit/page/SmartEditPreviewPage';

import type { CustomState } from 'shop/client/reducers';

jest.mock('shop/client/hooks/smart-edit/usePreviewTicket');

const defaultContext: TestingContext<CustomState> = {
    state: {
        cms: {
            [CMSParam.CMS_TICKET_ID]: ''
        }
    }
};
const ticketContext: TestingContext<CustomState> = {
    state: {
        cms: {
            [CMSParam.CMS_TICKET_ID]: '1234'
        }
    }
};

const mockReplace = jest.fn();

function getMockHistory(pathname: string, search?: string) {
    const history = createBrowserHistory();
    return {
        ...history,
        replace: mockReplace,
        location: {
            pathname: pathname,
            search: search ? `?${search}` : '',
            state: {},
            hash: ''
        }
    };
}

describe('<SmartEditPreviewPage /> component:', () => {
    describe('Rendering SmartEditWrapper', () => {
        ConfigManager.set({ supportedLanguages: ['de', 'en'], defaultLanguage: 'de' });

        beforeEach(() => {
            mockReplace.mockClear();
        });

        afterAll(() => {
            mockReplace.mockRestore();
        });

        it('should redirect to homepage without cmsTicketId', async () => {
            const history = getMockHistory('/en');
            await renderAsync(<SmarteditPreviewPage />, { ...defaultContext, history });
            expect(history.location.pathname).toEqual('/en');
        });

        it('should redirect to homepage with cmsTicketId', async () => {
            const history = getMockHistory('/en');
            (usePreviewTicket as jest.Mock).mockReturnValue('/');
            await renderAsync(<SmarteditPreviewPage />, { ...ticketContext, history });
            expect(history.replace).toBeCalledWith(expect.objectContaining({ pathname: '/de', search: '?cmsTicketId=1234' }), undefined);
        });

        it('should keep current params and add cmsTicketId', async () => {
            const history = getMockHistory('/en', 'test=1&test2=2');
            (usePreviewTicket as jest.Mock).mockReturnValue('/de?cmsTicketId=1234');
            await renderAsync(<SmarteditPreviewPage />, { ...ticketContext, history });
            expect(history.replace).toBeCalledWith(expect.objectContaining({ pathname: '/de', search: '?cmsTicketId=1234&test=1&test2=2' }), undefined);
        });

        it('should not add cmsTicketId multiple times from current url', async () => {
            const history = getMockHistory('/en', 'cmsTicketId=1234');
            (usePreviewTicket as jest.Mock).mockReturnValue('/de?cmsTicketId=1234');
            await renderAsync(<SmarteditPreviewPage />, { ...ticketContext, history });
            expect(history.replace).toBeCalledWith(expect.objectContaining({ pathname: '/de', search: '?cmsTicketId=1234' }), undefined);
        });

        it('should not add cmsTicketId multiple times from return url', async () => {
            const history = getMockHistory('/en');
            (usePreviewTicket as jest.Mock).mockReturnValue('/de?cmsTicketId=1234');
            await renderAsync(<SmarteditPreviewPage />, { ...ticketContext, history });
            expect(history.replace).toBeCalledWith(expect.objectContaining({ pathname: '/de', search: '?cmsTicketId=1234' }), undefined);
        });

        it('should do nothing if no redirect url is sent from BE', async () => {
            const history = getMockHistory('/en');
            await renderAsync(<SmarteditPreviewPage />, { ...ticketContext, history });
            expect(history.replace).not.toBeCalledWith(expect.objectContaining({ pathname: '/en' }), undefined);
        });
    });
});
