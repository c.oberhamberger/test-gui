/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, Navigate, useLocation, UrlHelper } from '@archibald/core';

import { CMSParam } from 'shop/client/constants';

import { useSmartEdit, usePreviewTicket } from 'shop/client/hooks/smart-edit';

function SmarteditPreviewPage() {
    const location = useLocation();
    const redirectUrl = usePreviewTicket();
    const { cmsTicketId } = useSmartEdit();

    if (!cmsTicketId) {
        return <Navigate to="/" />;
    }

    if (!redirectUrl) {
        return null;
    }
    const searchParams = new URLSearchParams(location.search);
    if (!redirectUrl.includes(CMSParam.CMS_TICKET_ID)) {
        searchParams.set(CMSParam.CMS_TICKET_ID, cmsTicketId);
    } else {
        searchParams.delete(CMSParam.CMS_TICKET_ID);
    }
    // in case the url we get from BE already contains a query param we append the search parameters to the url
    const params = searchParams.toString() ? `${redirectUrl.includes('?') ? '&' : '?'}${searchParams.toString()}` : '';
    const redirectUrlWithParameters = redirectUrl + params;
    if (redirectUrlWithParameters !== UrlHelper.getUrl(location)) {
        Logger.info(`SmartEdit Redirect url=${redirectUrlWithParameters}, cmsTicketId=${cmsTicketId}`);
        return <Navigate to={redirectUrlWithParameters} />;
    }
    return null;
}

export default SmarteditPreviewPage;
