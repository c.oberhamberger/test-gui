/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSRegistry } from '@archibald/storefront';

import { CMSComponentTypeCode } from 'shop/client/constants';

import type { CMSComponentClass } from 'shop/client/interfaces/base';

import BannersComponent from 'shop/client/components/organisms/cms/banners';
import CartComponent from 'shop/client/components/organisms/cms/cart';
import CheckoutComponent from 'shop/client/components/organisms/cms/checkout';
import FooterComponent from 'shop/client/components/organisms/cms/footer';
import HeaderComponent from 'shop/client/components/organisms/cms/header';
import ProductCarouselComponent from 'shop/client/components/organisms/cms/product/carousel';
import ProductDetailComponent from 'shop/client/components/organisms/cms/product/detail';
import ProductListingComponent from 'shop/client/components/organisms/cms/product/listing';
import PromoBarComponent from 'shop/client/components/organisms/cms/promo-bar';
import SearchResultComponent from 'shop/client/components/organisms/cms/search';
import TextComponent from 'shop/client/components/organisms/cms/text';
import TextImageComponent from 'shop/client/components/organisms/cms/text-image';

const CMSComponentRegistry = new CMSRegistry<CMSComponentClass>({ hydrate: { type: 'default', options: { hydrationTrigger: 'visible' } } });

CMSComponentRegistry.register(CMSComponentTypeCode.BANNERS, BannersComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.CART, CartComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.CHECKOUT, CheckoutComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.HEADER, HeaderComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.FOOTER, FooterComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.PRODUCT_CAROUSEL, ProductCarouselComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.PRODUCT_DETAIL, ProductDetailComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.PRODUCT_LISTING, ProductListingComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.PROMO_BAR, PromoBarComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.TEXT, TextComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.TEXT_IMAGE, TextImageComponent);
CMSComponentRegistry.register(CMSComponentTypeCode.SEARCH_RESULT, SearchResultComponent);

export default CMSComponentRegistry;
