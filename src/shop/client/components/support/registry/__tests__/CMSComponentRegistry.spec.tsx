/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSComponentTypeCode } from 'shop/client/constants';

import type { CMSComponentClass } from 'shop/client/interfaces/base';

import CMSComponentRegistry from 'shop/client/components/support/registry/CMSComponentRegistry';

const TestComponent: CMSComponentClass = () => null;
TestComponent.preload = () => new Promise<void>((resolve) => resolve());

describe('CMS Registry', () => {
    describe('getRegisteredCMSComponent', () => {
        it('should give back Header Component ', () => {
            const header = CMSComponentRegistry.get(CMSComponentTypeCode.HEADER);
            expect(header).toBeDefined();
        });

        it('should give back Footer Component ', () => {
            const brands = CMSComponentRegistry.get(CMSComponentTypeCode.FOOTER);
            expect(brands).toBeDefined();
        });
    });

    describe('registeredCMSComponent', () => {
        it('should register Test Component ', () => {
            const TestComponentID = 'test';
            CMSComponentRegistry.register(TestComponentID, TestComponent);
            const test = CMSComponentRegistry.get(TestComponentID);
            expect(test).toBeDefined();
        });

        it('should still give back Header Component ', () => {
            const header = CMSComponentRegistry.get(CMSComponentTypeCode.HEADER);
            expect(header).toBeDefined();
        });
    });
});
