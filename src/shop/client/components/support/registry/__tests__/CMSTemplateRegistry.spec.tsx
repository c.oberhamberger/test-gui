/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { LoadableComponent } from '@archibald/storefront';

import { CMSTemplate } from 'shop/client/constants';

import CMSTemplateRegistry from 'shop/client/components/support/registry/CMSTemplateRegistry';

const TestComponent: LoadableComponent<any> = () => null;
TestComponent.preload = () => new Promise<void>((resolve) => resolve());

describe('CMS Template Registry', () => {
    describe('get', () => {
        it('should give back Landing Template ', () => {
            const header = CMSTemplateRegistry.get(CMSTemplate.LANDING_PAGE);
            expect(header).toBeDefined();
        });

        it('should give back Cart Template ', () => {
            const brands = CMSTemplateRegistry.get(CMSTemplate.CART_PAGE);
            expect(brands).toBeDefined();
        });
    });

    describe('set', () => {
        it('should register Test Template ', () => {
            const TestComponentID = 'test';
            CMSTemplateRegistry.register(TestComponentID, TestComponent);
            const test = CMSTemplateRegistry.get(TestComponentID);
            expect(test).toBeDefined();
        });

        it('should still give back Landing Template ', () => {
            const header = CMSTemplateRegistry.get(CMSTemplate.LANDING_PAGE);
            expect(header).toBeDefined();
        });
    });
});
