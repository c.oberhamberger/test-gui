/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSRegistry } from '@archibald/storefront';

import { CMSTemplate } from 'shop/client/constants';

import CartTemplate from 'shop/client/components/pages/cart/template';
import CheckoutTemplate from 'shop/client/components/pages/checkout/template';
import FatalErrorTemplate from 'shop/client/components/pages/error/fatal/template';
import NotFoundTemplate from 'shop/client/components/pages/error/not-found/template';
import ContentTemplate from 'shop/client/components/pages/page/templates/content';
import LandingTemplate from 'shop/client/components/pages/page/templates/landing';
import TwoColumnTemplate from 'shop/client/components/pages/page/templates/two-column';
import ProductDetailTemplate from 'shop/client/components/pages/product/product-detail/template';
import ProductListingTemplate from 'shop/client/components/pages/product/product-listing/template';
import SearchPageTemplate from 'shop/client/components/pages/search/template';
import ThankYouTemplate from 'shop/client/components/pages/thankyou/template';
import FooterTemplate from 'shop/client/components/support/templates/layout/footer';
import HeaderTemplate from 'shop/client/components/support/templates/layout/header';

const CMSTemplateRegistry = new CMSRegistry({ static: true });

CMSTemplateRegistry.register(CMSTemplate.LANDING_PAGE, LandingTemplate);
CMSTemplateRegistry.register(CMSTemplate.CONTENT_PAGE, ContentTemplate);
CMSTemplateRegistry.register(CMSTemplate.NOT_FOUND_PAGE, NotFoundTemplate);
CMSTemplateRegistry.register(CMSTemplate.TWO_COLUMNS_PAGE, TwoColumnTemplate);
CMSTemplateRegistry.register(CMSTemplate.FATAL_ERROR_PAGE, FatalErrorTemplate);
CMSTemplateRegistry.register(CMSTemplate.PRODUCT_DETAIL_PAGE, ProductDetailTemplate);
CMSTemplateRegistry.register(CMSTemplate.PRODUCT_LISTING_PAGE, ProductListingTemplate);
CMSTemplateRegistry.register(CMSTemplate.SEARCH_PAGE, SearchPageTemplate);
CMSTemplateRegistry.register(CMSTemplate.CART_PAGE, CartTemplate);
CMSTemplateRegistry.register(CMSTemplate.CHECKOUT_PAGE, CheckoutTemplate);
CMSTemplateRegistry.register(CMSTemplate.THANKYOU_PAGE, ThankYouTemplate);
CMSTemplateRegistry.register(CMSTemplate.HEADER, HeaderTemplate);
CMSTemplateRegistry.register(CMSTemplate.FOOTER, FooterTemplate);

export default CMSTemplateRegistry;
