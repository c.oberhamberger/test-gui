/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import * as Storefront from '@archibald/storefront';
import { renderAsync } from '@archibald/testing';

import Loadable from 'shop/client/components/support/loadable/Loadable';

jest.mock('@archibald/storefront');

const TestComponent = () => {
    return <div>TestComponent</div>;
};

const asyncImport = (comp = TestComponent) => comp as unknown as Promise<any>;

// @ts-ignore
const InternalLoadableSpy = jest.spyOn(Storefront, 'InternalLoadable').mockImplementation((args) => {
    return args.factory();
});

describe('<Loadable /> component:', () => {
    it('should render and add default config ', async () => {
        const factory = () => asyncImport();
        const Component = Loadable({ factory });
        const { container } = await renderAsync(<Component />);
        expect(container).toBeDefined();
        expect(InternalLoadableSpy).toHaveBeenCalledWith({ excluded: true, factory });
    });

    it('should render and set config ', async () => {
        const factory = () => asyncImport();
        const Component = Loadable({ factory, excluded: false });
        const { container } = await renderAsync(<Component />);
        expect(container).toBeDefined();
        expect(InternalLoadableSpy).toHaveBeenCalledWith({ excluded: false, factory });
    });
});
