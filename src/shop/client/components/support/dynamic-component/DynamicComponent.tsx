/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger } from '@archibald/core';

import type { CMSComponent } from 'shop/client/interfaces/base';

import CMSRegistry from 'shop/client/components/support/registry/CMSComponentRegistry';
import CmsSmartEditWrapper from 'shop/client/components/support/smart-edit/wrapper/SmartEditWrapper';
import Column from 'shop/client/features/layout/components/column/Column';

interface Props {
    component: CMSComponent | null;
}

function DynamicComponent({ component }: Props) {
    if (component) {
        const typeCode = component.typeCode;
        const { Element } = CMSRegistry.get(typeCode);

        if (Element) {
            const renderElement = () => {
                const componentIndex = component.index ? `-${component.index}` : '';
                const id = `${component.typeCode}${componentIndex}`;
                return (
                    <CmsSmartEditWrapper
                        uid={component.uid}
                        catalogVersionUuid={component.catalogVersion}
                        uuid={component.uuid}
                        typeCode={component.typeCode}
                        columns={component.columns}
                    >
                        <Element id={id} {...component} />
                    </CmsSmartEditWrapper>
                );
            };

            if (component.columns) {
                return <Column columns={component.columns}>{renderElement()}</Column>;
            }
            return renderElement();
        }

        Logger.debug(`Skipped unknown CMS component typeCode=${typeCode}`);
    }

    return null;
}

export default DynamicComponent;
