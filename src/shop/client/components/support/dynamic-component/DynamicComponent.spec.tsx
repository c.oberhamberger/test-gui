/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import type { ColumnsSizeType } from 'shop/client/features/layout/interfaces';

import { CMSComponentTypeCode } from 'shop/client/constants';

import type { CMSComponentClass } from 'shop/client/interfaces/base';

import DynamicComponent from 'shop/client/components/support/dynamic-component/DynamicComponent';
import CMSComponentRegistry from 'shop/client/components/support/registry/CMSComponentRegistry';

const props = {
    component: {
        uid: 'TestParagraphComponent1',
        uuid: 'ContentSlot46ddd',
        typeCode: CMSComponentTypeCode.LINK,
        modifiedtime: '2018-11-06T15:11:03.505Z',
        name: 'Test Paragraph Component 1',
        to: 'Lorem ipsum dolor sit amet'
    }
};
const content = 'Content';
const TestComponent: CMSComponentClass = () => <div>{content}</div>;
TestComponent.preload = () => new Promise<void>((resolve) => resolve());

describe('<DynamicComponent /> component:', () => {
    beforeEach(() => {
        CMSComponentRegistry.clear();
    });

    it('should exist ', async () => {
        const component = await renderAsync(<DynamicComponent {...props} />);
        expect(component).toBeDefined();
    });

    it('should render without columns', async () => {
        CMSComponentRegistry.register(CMSComponentTypeCode.LINK, TestComponent);
        const { container } = await renderAsync(<DynamicComponent {...props} />);
        expect(container).toBeDefined();
    });

    it('should render with metadata', async () => {
        CMSComponentRegistry.register(CMSComponentTypeCode.LINK, TestComponent, { height: '62px' });
        const { container } = await renderAsync(<DynamicComponent {...props} />);
        expect(container).toBeDefined();
    });

    it('should render with columns', async () => {
        CMSComponentRegistry.register(CMSComponentTypeCode.LINK, TestComponent);
        const componentProps = { ...props.component, columns: '2' as ColumnsSizeType };
        await renderAsync(<DynamicComponent component={componentProps} />);
        const column = screen.getByTestId('column');
        expect(column).toHaveClass('columns2');
    });
});
