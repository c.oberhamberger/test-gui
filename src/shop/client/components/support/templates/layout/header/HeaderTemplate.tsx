/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { staticMemo } from '@archibald/storefront';

import { Suspense } from 'react';

import { CMSPosition } from 'shop/client/constants';

import Slot from 'shop/client/components/support/slot/Slot';

function HeaderTemplate() {
    return (
        <Suspense>
            <header>
                <Slot position={CMSPosition.TOP_HEADER} />
                <Slot position={CMSPosition.HEADER} />
                <Slot position={CMSPosition.BOTTOM_HEADER} />
                <Slot position={CMSPosition.NAVIGATION} />
            </header>
        </Suspense>
    );
}

export default staticMemo(HeaderTemplate);
