/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import * as ArchibaldCore from '@archibald/core';
import { render, renderAsync, screen, waitForRequests } from '@archibald/testing';

import { CMSTemplate } from 'shop/client/constants';

import CMSTemplateRegistry from 'shop/client/components/support/registry/CMSTemplateRegistry';
import Layout from 'shop/client/components/support/templates/layout/Layout';

jest.mock('@archibald/core', () => {
    const rest = jest.requireActual('@archibald/core');
    return {
        ...rest,
        useIsNavigationPending: jest.fn()
    };
});

const useIsNavigationPendingSpy = jest.spyOn(ArchibaldCore, 'useIsNavigationPending');

describe('<Layout /> component:', () => {
    const loaderText = 'Loader';
    const Loader = () => <div>{loaderText}</div>;

    beforeEach(() => {
        useIsNavigationPendingSpy.mockReturnValue(false);
    });

    beforeAll(async () => {
        const header = CMSTemplateRegistry.get(CMSTemplate.HEADER);
        const footer = CMSTemplateRegistry.get(CMSTemplate.FOOTER);
        await header.Element?.preload();
        await footer.Element?.preload();
    });

    it('should render loader if fallback', async () => {
        const { Element } = CMSTemplateRegistry.get(CMSTemplate.CART_PAGE);
        render(
            <Layout fallback={<Loader />}>
                <Element />
            </Layout>
        );
        const loader = screen.getByText(loaderText);
        expect(loader).toBeInTheDocument();
        await waitForRequests();
    });

    it('should not render loader if Element not suspending', async () => {
        const { Element } = CMSTemplateRegistry.get(CMSTemplate.CART_PAGE);
        await Element.preload();
        await renderAsync(
            <Layout fallback={<Loader />}>
                <Element />
            </Layout>
        );
        const cartTemplate = screen.getByTestId('cart-template');
        expect(cartTemplate).toBeInTheDocument();
    });
});
