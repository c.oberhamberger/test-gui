/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Outlet, useRequest, useSSR } from '@archibald/core';
import { ErrorBoundary, ErrorRenderer, type FallbackProps, staticMemo } from '@archibald/storefront';

import { type PropsWithChildren, type ReactNode, Suspense } from 'react';

import { CMSTemplate } from 'shop/client/constants';

import { useMessages } from 'shop/client/hooks';
import { useSmartEdit } from 'shop/client/hooks/smart-edit';

import Loader from 'shop/client/components/atoms/loader/Loader';
import HeadManager from 'shop/client/components/support/head-manager/HeadManager';
import CMSTemplateRegistry from 'shop/client/components/support/registry/CMSTemplateRegistry';
import Container from 'shop/client/features/layout/components/container/Container';

const HeaderTemplate = CMSTemplateRegistry.get(CMSTemplate.HEADER).Element;
const FooterTemplate = CMSTemplateRegistry.get(CMSTemplate.FOOTER).Element;

interface Props extends PropsWithChildren {
    fallback?: ReactNode;
}

function DefaultErrorFallback(props: FallbackProps) {
    const { isBrowser } = useSSR();

    if (isBrowser && props?.error?.message?.toLowerCase().includes('chunk')) {
        window.location.reload();
    }

    function renderErrorComponent(error: Error | null) {
        return (
            <div key={error?.name}>
                <h1>{error?.name ?? 'Something went Wrong'}</h1>
                <h3>{error?.message}</h3>
                <p>{error?.stack}</p>
            </div>
        );
    }

    return (
        <Container>
            <ErrorRenderer errorOrErrors={props.error} fallback={renderErrorComponent} />
        </Container>
    );
}

export function DefaultFallback({ children }: { children?: ReactNode }) {
    return (
        <>
            <HeaderTemplate />
            <HeadManager />
            {children || <Loader type="page" />}
            <FooterTemplate />
        </>
    );
}

function AppLayout({ children, fallback = <DefaultFallback /> }: Props) {
    return (
        <ErrorBoundary FallbackComponent={DefaultErrorFallback}>
            <Suspense fallback={fallback}>{children}</Suspense>
        </ErrorBoundary>
    );
}

export function Layout() {
    useRequest();
    useSmartEdit();
    useMessages();

    return <Outlet />;
}

export default staticMemo(AppLayout);
