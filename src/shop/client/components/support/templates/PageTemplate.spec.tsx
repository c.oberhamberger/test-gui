/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ErrorType, HttpCode } from '@archibald/core';
import { CMSProvider } from '@archibald/storefront';
import { act, render, renderAsync, TestingContext } from '@archibald/testing';

import { Suspense } from 'react';

import { CMSTemplate } from 'shop/client/constants';

import Loadable from 'shop/client/components/support/loadable/Loadable';
import CMSTemplateRegistry from 'shop/client/components/support/registry/CMSTemplateRegistry';
import PageTemplate from 'shop/client/components/support/templates/PageTemplate';

import type { CustomState } from 'shop/client/reducers';

import { mockCMS, mockCoreData, mockPages } from '../../../utils/helpers/mocks';

const fatalText = 'fatal';

const defaultContext: TestingContext<CustomState> = {
    state: {
        cms: mockCMS
    }
};

const notFoundContext: TestingContext<CustomState> = {
    state: {
        cms: mockCMS,
        coredata: {
            ...mockCoreData,
            lastHttpStatus: HttpCode.CODE_404
        }
    }
};

const fatalContext: TestingContext<CustomState> = {
    state: {
        coredata: {
            messages: {
                'api.error.general': fatalText
            },
            errors: {
                cms: [
                    {
                        type: ErrorType.ERROR,
                        message: 'Test',
                        fatal: true
                    }
                ]
            }
        },
        cms: {
            ...mockCMS
        }
    }
};

const twoColumsPage = {
    ...mockPages.test123,
    template: CMSTemplate.TWO_COLUMNS_PAGE
};

const contentPage = {
    ...mockPages.test123,
    template: CMSTemplate.CONTENT_PAGE
};

const notFoundPage = {
    ...mockPages.test123,
    template: CMSTemplate.NOT_FOUND_PAGE
};

const fatalPage = {
    ...mockPages.test123,
    template: CMSTemplate.FATAL_ERROR_PAGE
};

describe('<PageTemplate /> component:', () => {
    const loaderText = 'Loader';
    const Loader = () => <div>{loaderText}</div>;

    beforeAll(async () => {
        const header = CMSTemplateRegistry.get(CMSTemplate.HEADER);
        const footer = CMSTemplateRegistry.get(CMSTemplate.FOOTER);
        await header.Element?.preload();
        await footer.Element?.preload();
    });

    it('should exist ', async () => {
        const { container } = await renderAsync(<PageTemplate />, defaultContext);
        expect(container).toBeDefined();
    });

    it('should render loader if fallback', async () => {
        // don't use renderAsync so that it render the Loader and doesn't wait until it is "finished"
        const { getByText } = render(
            <CMSProvider page={contentPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            defaultContext
        );
        await act(async () => {
            getByText(loaderText);
        });
    });

    it('should render landing page ', async () => {
        const template = CMSTemplateRegistry.get(CMSTemplate.CONTENT_PAGE);
        await template.Element?.preload();
        const { queryByText } = await renderAsync(
            <CMSProvider page={contentPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            defaultContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
    });

    it('should render NotFound page ', async () => {
        const template = CMSTemplateRegistry.get(CMSTemplate.NOT_FOUND_PAGE);
        await template.Element?.preload();
        const { queryByText, getByText } = await renderAsync(
            <CMSProvider page={notFoundPage} error={true}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            notFoundContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
        getByText('404');
    });

    it('should render fatal page if no content', async () => {
        CMSTemplateRegistry.delete(CMSTemplate.CONTENT_PAGE);
        const template = CMSTemplateRegistry.get(CMSTemplate.FATAL_ERROR_PAGE);
        await template.Element?.preload();
        const { queryByText, getByText } = await renderAsync(
            <CMSProvider page={contentPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            fatalContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
        getByText(fatalText);
        const ContentTemplate = Loadable({ factory: () => import('shop/client/components/pages/page/templates/content/ContentTemplate') });
        CMSTemplateRegistry.register(CMSTemplate.CONTENT_PAGE, ContentTemplate);
    });

    it('should render fatal page ', async () => {
        const template = CMSTemplateRegistry.get(CMSTemplate.FATAL_ERROR_PAGE);
        await template.Element?.preload();
        const { queryByText, getByText } = await renderAsync(
            <CMSProvider page={fatalPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            fatalContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
        getByText(fatalText);
    });

    it('should render fatal error page on no registry ', async () => {
        const { queryByText, getByText } = await renderAsync(
            <CMSProvider page={fatalPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            fatalContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
        getByText(fatalText);
    });

    it('should render content page ', async () => {
        const template = CMSTemplateRegistry.get(CMSTemplate.CONTENT_PAGE);
        await template.Element?.preload();
        const { queryByText } = await renderAsync(
            <CMSProvider page={contentPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            defaultContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
    });

    it('should render two columns page ', async () => {
        const template = CMSTemplateRegistry.get(CMSTemplate.TWO_COLUMNS_PAGE);
        await template.Element?.preload();
        const { queryByText } = await renderAsync(
            <CMSProvider page={twoColumsPage}>
                <Suspense fallback={<Loader />}>
                    <PageTemplate />
                </Suspense>
            </CMSProvider>,
            defaultContext
        );
        const loader = queryByText(loaderText);
        expect(loader).toBeNull();
    });
});
