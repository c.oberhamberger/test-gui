/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { staticMemo } from '@archibald/storefront';

import { CMSTemplate } from 'shop/client/constants';

import { usePage, usePageError } from 'shop/client/hooks/cms';

import CMSTemplateRegistry from 'shop/client/components/support/registry/CMSTemplateRegistry';

const NotFoundTemplate = CMSTemplateRegistry.get(CMSTemplate.NOT_FOUND_PAGE).Element;
const FatalErrorTemplate = CMSTemplateRegistry.get(CMSTemplate.FATAL_ERROR_PAGE).Element;

const HeaderTemplate = CMSTemplateRegistry.get(CMSTemplate.HEADER).Element;
const FooterTemplate = CMSTemplateRegistry.get(CMSTemplate.FOOTER).Element;

function PageTemplate() {
    const page = usePage();
    const error = usePageError();

    const { Element: Template, empty } = CMSTemplateRegistry.get(page?.template);

    let content = <Template />;

    if (error) {
        content = <NotFoundTemplate />;
    } else if (empty) {
        content = <FatalErrorTemplate />;
    }

    return (
        <>
            <HeaderTemplate />
            <main>{content}</main>
            <FooterTemplate />
        </>
    );
}

export default staticMemo(PageTemplate);
