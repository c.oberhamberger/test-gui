import { Content, Icon, Portal, Root, ScrollDownButton, ScrollUpButton, SelectProps, Trigger, Value, Viewport } from '@radix-ui/react-select';

import { ForwardedRef, forwardRef } from 'react';

import IconComponent from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/atoms/select/Select.scss';

interface SelectInterface extends SelectProps {
    label: string;
}

const Select = forwardRef(({ children, value, label, ...props }: SelectInterface, forwardedRef: ForwardedRef<HTMLButtonElement>) => {
    // Workaround until issue https://github.com/radix-ui/primitives/issues/1658 in the library gets fixed
    function setRef(ref) {
        if (!ref) {
            return;
        }
        ref.ontouchstart = (event) => {
            event.preventDefault();
        };
    }

    return (
        <Root value={value} {...props}>
            <Trigger className={Classes.selectTrigger} ref={forwardedRef}>
                <Value aria-label={value}>{label}</Value>
                <Icon>
                    <IconComponent type="caret-down" size="small" />
                </Icon>
            </Trigger>
            <Portal>
                <Content className={Classes.selectContent} position="popper" ref={setRef}>
                    <ScrollUpButton className={Classes.selectScrollButton}>
                        <IconComponent type="caret-up" />
                    </ScrollUpButton>
                    <Viewport>{children}</Viewport>
                    <ScrollDownButton className={Classes.selectScrollButton}>
                        <IconComponent type="caret-down" />
                    </ScrollDownButton>
                </Content>
            </Portal>
        </Root>
    );
});

Select.displayName = 'Select';

export default Select;
