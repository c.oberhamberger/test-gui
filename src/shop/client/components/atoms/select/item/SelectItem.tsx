/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Item, ItemText, SelectItemProps } from '@radix-ui/react-select';

import { ForwardedRef, forwardRef } from 'react';

import Classes from 'shop/client/components/atoms/select/item/SelectItem.scss';

const SelectItem = forwardRef(({ children, ...props }: SelectItemProps, forwardedRef: ForwardedRef<HTMLDivElement>) => {
    return (
        <Item className={Classes.selectItem} {...props} ref={forwardedRef}>
            <ItemText>{children}</ItemText>
        </Item>
    );
});

SelectItem.displayName = 'SelectItem';

export default SelectItem;
