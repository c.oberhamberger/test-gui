/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { Logger } from '@archibald/core';

import type { MouseEvent, PropsWithChildren } from 'react';

import type { IconType } from 'shop/client/interfaces/base';
import type { ButtonAppearance } from 'shop/client/interfaces/base';

import Icon from 'shop/client/components/atoms/icon/Icon';
import Link from 'shop/client/components/atoms/link/Link';

import Classes from 'shop/client/components/atoms/button/Button.scss';

export interface Props extends PropsWithChildren {
    appearance?: ButtonAppearance;
    className?: string;
    disabled?: boolean;
    fullWidth?: boolean;
    iconAfter?: IconType;
    iconBefore?: IconType;
    onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
    spinner?: boolean;
    type?: 'submit' | 'reset' | 'button';
    url?: string;
    urlTarget?: string;
    testId?: string;
}

function Button({
    appearance,
    children,
    disabled,
    fullWidth,
    iconAfter,
    iconBefore,
    onClick,
    type = 'button',
    url,
    urlTarget,
    className,
    testId,
    ...rest
}: Props) {
    const classNames = classNamesHelper([appearance && Classes[appearance], fullWidth && Classes.fullWidth, className]);

    if (appearance && className) {
        Logger.error(
            'Do not use appearance and className together. Class name should be used to specific buttons like carousel dots or arrows. ' +
                'If you use button multiple times create new appearance.'
        );
    }

    function handleClick(event: MouseEvent<HTMLButtonElement>) {
        onClick?.(event);
    }

    function renderIconBefore() {
        if (!iconBefore) {
            return null;
        }

        return (
            <div className={classNamesHelper([Classes.icon, Classes.iconBefore])}>
                <Icon type={iconBefore} size="large" />
            </div>
        );
    }

    function renderIconAfter() {
        if (!iconAfter) {
            return null;
        }

        return (
            <div className={classNamesHelper([Classes.icon, Classes.iconAfter])}>
                <Icon type={iconAfter} size="large" />
            </div>
        );
    }

    function renderContent() {
        if (!children) {
            return null;
        }

        return (
            <>
                {renderIconBefore()}
                <div className={Classes.content}>{children}</div>
                {renderIconAfter()}
            </>
        );
    }

    if (url) {
        return (
            <Link to={url} className={classNames} target={urlTarget} {...rest}>
                {renderContent()}
            </Link>
        );
    }

    return (
        <button onClick={handleClick} type={type} role="button" className={classNames} disabled={disabled} data-testid={testId} {...rest}>
            {renderContent()}
        </button>
    );
}

export default Button;
