/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { screen, fireEvent, renderAsync } from '@archibald/testing';

import Button from 'shop/client/components/atoms/button/Button';

describe('<Button /> component:', () => {
    const content = 'Button';
    const appearance = 'primary';

    it('should render children', async () => {
        await renderAsync(<Button>{content}</Button>);
        const button = screen.getByRole('button', { name: content });
        expect(button).toHaveTextContent(content);
    });

    it('should be primary button', async () => {
        await renderAsync(<Button appearance={appearance}>{content}</Button>);
        const button = screen.getByRole('button', { name: content });
        expect(button).toHaveStyle({ backgroundColor: appearance });
    });

    it('should fire onClick on click', async () => {
        const onClick = jest.fn();
        await renderAsync(<Button onClick={onClick}>{content}</Button>);
        const button = screen.getByRole('button', { name: content });
        expect(button).toHaveTextContent(content);
        fireEvent.click(button);
        expect(onClick).toHaveBeenCalled();
    });

    it('should render link with children', async () => {
        await renderAsync(<Button url="/de">{content}</Button>);
        const button = screen.getByRole('link', { name: content });
        expect(button).toHaveTextContent(content);
    });

    it('should render button with type button by default', async () => {
        await renderAsync(<Button>{content}</Button>);
        const button = screen.getByRole('button', { name: content });
        expect(button).toHaveAttribute('type', 'button');
    });

    it('should render button with type submit', async () => {
        await renderAsync(<Button type="submit">{content}</Button>);
        const button = screen.getByRole('button', { name: content });
        expect(button).toHaveAttribute('type', 'submit');
    });
});
