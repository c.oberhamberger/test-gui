/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import Button from 'shop/client/components/atoms/button/Button';

import Classes from 'shop/client/components/atoms/carousel/dots/CarouselDots.scss';

interface Props {
    dots: number;
    selectedDot: number;
    onClick?: (index: number) => void;
}

function CarouselDots({ dots, selectedDot, onClick }: Props) {
    const dotsArray = Array.from({ length: dots }, (_, idx) => idx);

    const handleClick = (index: number) => () => {
        onClick?.(index);
    };

    return (
        <div className={Classes.dots}>
            {dotsArray.map((dot: number, index: number) => (
                <Button
                    key={`${dot}-${index}`}
                    onClick={handleClick(index)}
                    className={classNamesHelper([Classes.dot, index === selectedDot && Classes.selectedDot])}
                />
            ))}
        </div>
    );
}

export default CarouselDots;
