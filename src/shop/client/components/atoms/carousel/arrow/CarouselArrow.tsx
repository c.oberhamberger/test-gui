/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import Button from 'shop/client/components/atoms/button/Button';
import Icon from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/atoms/carousel/arrow/CarouselArrow.scss';

interface Props {
    direction: 'right' | 'left';
    size?: 'small' | 'big';
    disabled?: boolean;
    onClick?: () => void;
}

function CarouselArrow({ direction, onClick, disabled = false, size = 'small' }: Props) {
    return (
        <Button
            data-testid={'arrow' + direction}
            onClick={onClick}
            className={classNamesHelper([
                Classes.arrow,
                size === 'big' && Classes.arrowBig,
                direction === 'right' ? Classes.arrowRight : Classes.arrowLeft,
                disabled && Classes.arrowDisabled
            ])}
            disabled={disabled}
        >
            <Icon type={direction === 'right' ? 'caret-right' : 'caret-left'} size="extra-large" />
        </Button>
    );
}

export default CarouselArrow;
