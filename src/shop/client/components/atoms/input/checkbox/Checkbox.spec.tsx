/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { screen, fireEvent, renderAsync } from '@archibald/testing';

import Checkbox from 'shop/client/components/atoms/input/checkbox/Checkbox';

describe('<Checkbox /> component:', () => {
    const id = 'checkboxId';
    const label = 'this is a checkbox';

    it('should render a label', async () => {
        await renderAsync(<Checkbox id={id} label={label} />);
        const checkbox = screen.getByText(label);
        expect(checkbox).toBeDefined();
        expect(checkbox).toHaveTextContent(label);
    });

    it('should be checked', async () => {
        await renderAsync(<Checkbox id={id} label={label} checked />);
        const checkbox = screen.getByRole('checkbox', { name: label });
        expect(checkbox).toBeChecked();
    });

    it('should fire onChange event on click', async () => {
        const onChange = jest.fn();
        await renderAsync(<Checkbox id={id} label={label} onChange={onChange} />);
        const checkbox = screen.getByRole('checkbox', { name: label });
        expect(checkbox).not.toBeChecked();

        fireEvent.click(checkbox);
        expect(onChange).toHaveBeenCalledWith(true);
        expect(checkbox).toBeChecked();

        fireEvent.click(checkbox);
        expect(onChange).toHaveBeenCalledWith(false);
        expect(checkbox).not.toBeChecked();
    });

    it('should not fire onChange event when disabled', async () => {
        const onChange = jest.fn();
        await renderAsync(<Checkbox id={id} label={label} onChange={onChange} disabled />);
        const checkbox = screen.getByRole('checkbox', { name: label });
        expect(checkbox).not.toBeChecked();
        expect(checkbox).toBeDisabled();
        fireEvent.click(checkbox);
        expect(checkbox).not.toBeChecked();
    });
});
