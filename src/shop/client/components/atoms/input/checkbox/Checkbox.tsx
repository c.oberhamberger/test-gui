/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type CheckedState, Indicator, Root } from '@radix-ui/react-checkbox';

import Icon from 'shop/client/components/atoms/icon/Icon';
import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/atoms/input/checkbox/Checkbox.scss';

export interface CheckboxChangeEvent {
    checked: boolean;
}

interface Props {
    id: string;
    checked?: boolean;
    defaultChecked?: boolean;
    disabled?: boolean;
    label: string;
    onChange?(checked: CheckedState): void;
}

function Checkbox({ id, checked, defaultChecked, disabled, label, onChange }: Props) {
    return (
        <form>
            <Flex align="center">
                <Root className={Classes.root} defaultChecked={defaultChecked} checked={checked} disabled={disabled} onCheckedChange={onChange} id={id}>
                    <Indicator className={Classes.indicator}>
                        <Icon type="check" size="medium" />
                    </Indicator>
                </Root>
                <label className={Classes.checkboxLabel} htmlFor={id}>
                    {label}
                </label>
            </Flex>
        </form>
    );
}

export default Checkbox;
