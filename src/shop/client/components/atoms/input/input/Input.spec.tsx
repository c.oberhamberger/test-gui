/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { fireEvent, renderAsync, screen } from '@archibald/testing';

import Input from 'shop/client/components/atoms/input/input/Input';

describe('<Input/> component:', () => {
    it('should render an icon', async () => {
        await renderAsync(<Input icon="search" />);
        const container = screen.getByTestId('icon');
        expect(container).toBeDefined();
    });

    it('should fire onChange event', async () => {
        const value = 'test input';
        const onChange = jest.fn();
        await renderAsync(<Input onChange={onChange} value={'initial value'} />);
        const input = screen.getByRole('textbox');
        fireEvent.change(input, { target: { value } });
        expect(onChange).toHaveBeenCalled();
        expect(input).toHaveValue(value);
    });

    it('should fire onClick event when icon is clicked', async () => {
        const onClick = jest.fn();
        const value = 'test input';
        await renderAsync(<Input onIconClick={onClick} icon="search" value={value} />);
        const icon = screen.getByTestId('icon');
        fireEvent.click(icon);
        expect(onClick).toHaveBeenCalledWith(value);
    });
});
