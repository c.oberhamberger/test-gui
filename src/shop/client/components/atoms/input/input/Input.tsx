/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type ChangeEvent, type DetailedHTMLProps, type InputHTMLAttributes, useState } from 'react';

import type { IconType } from 'shop/client/interfaces/base';

import Icon from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/atoms/input/input/Input.scss';

interface Props extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    icon?: IconType;
    onIconClick?: (value: string) => void;
    value?: string;
    testId?: string;
}

function Input({ icon, onIconClick, onChange, value, testId, ...restProps }: Props) {
    const [inputValue, setInputValue] = useState<string>(value || '');

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setInputValue(event.target.value);
        onChange?.(event);
    }

    function handleIconClick() {
        onIconClick?.(inputValue);
    }

    return (
        <div className={Classes.inputWrapper}>
            <input data-testid={testId} type="text" className={Classes.input} onChange={handleChange} value={inputValue} {...restProps} />
            {icon && (
                <div className={Classes.icon} onClick={handleIconClick} data-testid="icon">
                    <Icon type={icon} size="medium" />
                </div>
            )}
        </div>
    );
}

export default Input;
