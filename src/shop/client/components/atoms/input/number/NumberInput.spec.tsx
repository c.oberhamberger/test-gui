/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { fireEvent, screen, renderAsync } from '@archibald/testing';

import NumberInput from 'shop/client/components/atoms/input/number/NumberInput';

describe('<NumberInput /> component:', () => {
    it('should have background color', async () => {
        await renderAsync(<NumberInput />);
        const number = screen.getByRole('textbox');
        expect(number).toHaveValue('1');
    });

    it('should fire onChange on increment/decrement click', async () => {
        const onChange = jest.fn();
        await renderAsync(<NumberInput onChange={onChange} />);
        const number = screen.getByRole('textbox');
        const incrementButton = screen.getByRole('button', { name: 'increment' });
        const decrementButton = screen.getByRole('button', { name: 'decrement' });
        expect(number).toHaveValue('1');
        fireEvent.click(incrementButton);
        expect(onChange).toHaveBeenCalledWith({ value: 2, direction: 'increment' });
        expect(number).toHaveValue('2');
        fireEvent.click(decrementButton);
        expect(onChange).toHaveBeenCalledWith({ value: 1, direction: 'decrement' });
        expect(number).toHaveValue('1');
    });

    it('should consider min', async () => {
        const onChange = jest.fn();
        await renderAsync(<NumberInput onChange={onChange} min={0} />);
        const number = screen.getByRole('textbox');
        const decrementButton = screen.getByRole('button', { name: 'decrement' });
        expect(number).toHaveValue('1');
        fireEvent.click(decrementButton);
        expect(onChange).toHaveBeenCalledWith({ value: 0, direction: 'decrement' });
        expect(number).toHaveValue('0');
        onChange.mockClear();
        fireEvent.click(decrementButton);
        expect(onChange).not.toHaveBeenCalledWith({ value: -1, direction: 'decrement' });
        expect(number).not.toHaveValue('-1');
    });

    it('should consider max', async () => {
        const onChange = jest.fn();
        await renderAsync(<NumberInput onChange={onChange} max={2} />);
        const number = screen.getByRole('textbox');
        const incrementButton = screen.getByRole('button', { name: 'increment' });
        expect(number).toHaveValue('1');
        fireEvent.click(incrementButton);
        expect(onChange).toHaveBeenCalledWith({ value: 2, direction: 'increment' });
        expect(number).toHaveValue('2');
        onChange.mockClear();
        fireEvent.click(incrementButton);
        expect(onChange).not.toHaveBeenCalledWith({ value: 3, direction: 'increment' });
        expect(number).not.toHaveValue('3');
    });
});
