/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';

import { MouseEvent, useState } from 'react';

import { ButtonAppearance } from 'shop/client/interfaces/base/components';

import Button from 'shop/client/components/atoms/button/Button';

import Classes from 'shop/client/components/atoms/input/number/NumberInput.scss';

type NumberDirection = 'increment' | 'decrement';

export interface ChangeEvent {
    value: number;
    direction: NumberDirection;
}

export interface Props {
    value?: number;
    defaultValue?: number;
    color?: ButtonAppearance;
    min?: number;
    max?: number;
    onChange?: (e: ChangeEvent) => void;
    fullWidth?: boolean;
    testId?: string;
}

function NumberInput({ onChange, value, defaultValue = 1, min = -Infinity, max = Infinity, fullWidth, testId }: Props) {
    const [internalValue, setInternalState] = useState(defaultValue);
    const classNames = classNamesHelper([Classes.wrapper, fullWidth && Classes.fullWidth]);

    function calculateNewState(direction: NumberDirection, value: number) {
        const newValue = direction === 'increment' ? value + 1 : value - 1;
        if (newValue < min || newValue > max) {
            return value;
        }
        onChange?.({ value: newValue, direction });
        return newValue;
    }

    function handleChange(direction: NumberDirection) {
        return function (event: MouseEvent<HTMLButtonElement>) {
            event.persist();
            if (value) {
                calculateNewState(direction, value);
            } else {
                setInternalState((currentValue) => {
                    return calculateNewState(direction, currentValue);
                });
            }
        };
    }

    return (
        <label htmlFor="number" data-testid={testId} className={classNames}>
            <Button onClick={handleChange('decrement')} testId="decrement" aria-label="decrement">
                &#8212;
            </Button>
            <input id="number" className={Classes.input} value={value ?? internalValue} min={min} max={max} readOnly={true} />
            <Button onClick={handleChange('increment')} testId="increment" aria-label="increment">
                &#43;
            </Button>
        </label>
    );
}

export default NumberInput;
