/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { UrlHelper } from '@archibald/core';
import { ServerHelper } from '@archibald/server';

import { createRef, type RefObject } from 'react';

import Config from 'shop/client/config/config';

import type { ImageInterface } from 'shop/client/interfaces/base';

import Classes from 'shop/client/components/atoms/image/Image.scss';

interface Props extends ImageInterface {
    className?: string;
    lazy?: boolean;
}

function getImageUrl(src: string) {
    if (UrlHelper.isAbsoluteHttpUrl(src)) {
        return src;
    }

    if (!Config.get('app.image.protocol') || !Config.get('app.image.host')) {
        return src;
    }

    return `${Config.get('app.image.protocol')}://${Config.get('app.image.host')}${ServerHelper.mapPort(Number(Config.get('app.image.port')))}${src}`;
}

function Image({ src, className, style, height, width, lazy = true }: Props) {
    const imageRef: RefObject<HTMLImageElement> = createRef();
    const computedSrc = getImageUrl(src);
    const classNames = classNamesHelper([className, Classes['container']]);
    const imageClassNames = classNamesHelper([Classes['image']]);

    return (
        <div className={classNames}>
            <img
                className={imageClassNames}
                ref={imageRef}
                loading={lazy ? 'lazy' : 'eager'}
                src={computedSrc}
                width={width}
                height={height}
                style={style}
                alt="text"
            />
        </div>
    );
}

export default Image;
