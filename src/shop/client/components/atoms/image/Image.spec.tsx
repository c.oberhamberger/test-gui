/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import Image from 'shop/client/components/atoms/image/Image';

const defaultProps = {
    className: 'test--class',
    src: 'http://localhost/img.jpg',
    ratio: {
        width: 1,
        height: 1
    }
};

describe('<ImageComponent /> component:', () => {
    const content = 'Content';
    const imageTag = 'img';

    it('should exist ', async () => {
        const { container } = await renderAsync(<Image {...defaultProps}>{content}</Image>);
        expect(container).toBeDefined();
    });
    it('should have no url', async () => {
        const props = { ...defaultProps, src: '' };
        const { container, queryByText } = await renderAsync(<Image {...props}>{content}</Image>);
        const image = container.querySelector(imageTag);
        expect(queryByText(content)).toBeNull();
        expect(image).toHaveAttribute('src', '');
    });
});
