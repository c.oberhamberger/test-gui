/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import * as ArchibaldStorefront from '@archibald/storefront';
import { screen, fireEvent, renderAsync } from '@archibald/testing';

import Message from 'shop/client/components/atoms/message/Message';

jest.mock('@archibald/storefront');
jest.mock('@archibald/core', () => ({
    ...jest.requireActual('@archibald/core'),
    useNavigate: () => navigateSpy
}));

const navigateSpy = jest.fn();
const useTranslateSpy = jest.spyOn(ArchibaldStorefront, 'useTranslate').mockReturnValue(() => '');

const defaultProps = {
    className: 'mini-cart-title',
    code: 'mini.cart.your.order'
};

describe('<Message/> component', () => {
    const testMessage = 'Test Message';
    const relativeTestLink = `<a target="_self" href="/test">${testMessage}</a>`;

    beforeEach(() => {
        useTranslateSpy.mockReturnValue(() => '');
    });

    it('should not exist if code is empty', async () => {
        await renderAsync(<Message code="" />);
        const message = screen.queryByText(testMessage);
        expect(message).toBeNull();
    });

    it('should render simple text', async () => {
        const props = { ...defaultProps, simpleText: true };
        useTranslateSpy.mockReturnValue(() => testMessage);
        await renderAsync(<Message {...props} />);
        const translation = screen.getByText(testMessage);
        expect(translation).not.toHaveClass(defaultProps.className);
        expect(translation).toHaveTextContent(testMessage);
    });

    it('should render translation', async () => {
        useTranslateSpy.mockReturnValue(() => relativeTestLink);
        await renderAsync(<Message {...defaultProps} />);
        const translation = screen.getByText(testMessage);
        expect(translation.parentElement).toHaveClass(defaultProps.className);
        expect(translation).toHaveTextContent(testMessage);
    });

    it('should trigger onClick', async () => {
        const props = { ...defaultProps };
        useTranslateSpy.mockReturnValue(() => relativeTestLink);
        await renderAsync(<Message {...props} />);
        const translation = screen.getByText(testMessage);
        fireEvent.click(translation);
        expect(navigateSpy).toHaveBeenCalledWith('/test');
    });
});
