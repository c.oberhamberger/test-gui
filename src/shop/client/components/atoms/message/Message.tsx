/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { SafeHTML } from '@archibald/client';
import { type DefaultMessageParamsType, UrlHelper, useNavigate } from '@archibald/core';
import { useTranslate, UrlTarget } from '@archibald/storefront';

import type { MouseEvent } from 'react';

interface Props {
    code: string;
    className?: string;
    params?: DefaultMessageParamsType;
    simpleText?: boolean;
    children?: (string | number)[];
}

function Message({ code, params, className, simpleText, children }: Props) {
    const translate = useTranslate();
    const navigate = useNavigate();
    const childrenText = children?.join() ?? '';
    const text = translate(code, params);

    function onClick(event: MouseEvent<HTMLSpanElement>) {
        const targetElement = event.target as HTMLElement;
        const url = targetElement?.attributes?.getNamedItem('href')?.textContent ?? '';
        const target = targetElement?.attributes?.getNamedItem('target')?.value ?? UrlTarget.SELF;
        if (url && !UrlHelper.isUrlExternal(url) && target === UrlTarget.SELF) {
            event.preventDefault();
            navigate(url);
        }
    }

    if (!code && !text) {
        return null;
    }
    if (code && !text) {
        return <>{`[${text}${childrenText}]`}</>;
    }
    if (simpleText) {
        return <>{`${text}${childrenText}`}</>;
    }

    return <SafeHTML tag="span" content={`${text}${childrenText}`} {...{ className, onClick }} />;
}

export default Message;
