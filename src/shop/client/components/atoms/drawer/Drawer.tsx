/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import * as Dialog from '@radix-ui/react-dialog';

import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import Button from 'shop/client/components/atoms/button/Button';
import Icon from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/atoms/drawer/Drawer.scss';

interface Props extends PropsWithChildren {
    title?: string;
    description?: string;
    className?: string;
}

const DrawerContent = forwardRef(({ children, title, description, className, ...props }: Props, forwardedRef: ForwardedRef<HTMLDivElement>) => {
    return (
        <Dialog.Portal>
            <Dialog.Overlay className={Classes.overlay} />
            <Dialog.Content className={classNamesHelper([Classes.content, className])} {...props} ref={forwardedRef}>
                <Dialog.Title className={Classes.title}>{title ?? ''}</Dialog.Title>
                <Dialog.Close asChild>
                    <Button className={Classes.close}>
                        <Icon type="close" size="medium" />
                    </Button>
                </Dialog.Close>
                {children}
            </Dialog.Content>
        </Dialog.Portal>
    );
});

DrawerContent.displayName = 'DrawerContent';

export default { Root: Dialog.Root, Trigger: Dialog.Trigger, Content: DrawerContent };
