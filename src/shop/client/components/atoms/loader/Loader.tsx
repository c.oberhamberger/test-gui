/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { useSynchronizedAnimation } from '@archibald/storefront';

import Classes from 'shop/client/components/atoms/loader/Loader.scss';

export type LoaderType = 'base' | 'image' | 'page';

interface Props {
    visible?: boolean;
    type?: LoaderType;
}

function Loader({ type = 'base', visible = true }: Props) {
    const refRotate = useSynchronizedAnimation(Classes.rotate);
    const refDash = useSynchronizedAnimation(Classes.dash);

    const classNames = classNamesHelper([Classes.loader, Classes.loaderAnimate, type && Classes[`loader--${type}`]]);

    if (!visible) {
        return null;
    }

    return (
        <svg className={classNames} ref={refRotate} viewBox="0 0 50 50" role="alert" aria-label="Loading…" aria-busy="true">
            <circle className={Classes.path} ref={refDash} cx="25" cy="25" r="20" fill="none" strokeWidth="5" />
        </svg>
    );
}

export default Loader;
