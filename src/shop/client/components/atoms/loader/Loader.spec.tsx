/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync } from '@archibald/testing';

import Loader from 'shop/client/components/atoms/loader/Loader';

const defaultProps = {
    type: 'base',
    visible: false
} as const;

describe('<Loader /> component:', () => {
    const svgTag = 'svg';

    it('should exist ', async () => {
        const { container } = await renderAsync(<Loader {...defaultProps} />);
        expect(container).toBeDefined();
    });

    it('should not be visible', async () => {
        const props = { ...defaultProps };
        const { container } = await renderAsync(<Loader {...props} />);
        const loader = container.querySelector(svgTag);
        expect(loader).toBeFalsy();
    });

    it('should be visible', async () => {
        const props = { ...defaultProps, visible: true };
        const { container } = await renderAsync(<Loader {...props} />);
        const loader = container.querySelector(svgTag);
        expect(loader).toBeDefined();
    });

    it('should use type image', async () => {
        const props = { ...defaultProps, type: 'image', visible: true } as const;
        const { container } = await renderAsync(<Loader {...props} />);
        const loader = container.querySelector(svgTag);
        expect(loader).toHaveClass('loader--image');
    });
    it('should use type page', async () => {
        const props = { ...defaultProps, type: 'page', visible: true } as const;
        const { container } = await renderAsync(<Loader {...props} />);
        const loader = container.querySelector(svgTag);
        expect(loader).toHaveClass('loader--page');
    });
});
