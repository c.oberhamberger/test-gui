/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { classNamesHelper } from '@archibald/client';
import { Logger } from '@archibald/core';

import type { PropsWithChildren } from 'react';

import type { HeadlineTypes, TextAlign, TextSize, TextWeight } from 'shop/client/interfaces/base';

import Classes from 'shop/client/components/atoms/text/Text.scss';

type ValidTags = 'div' | 'span';

interface Props extends PropsWithChildren {
    tag?: ValidTags;
    size?: TextSize;
    weight?: TextWeight;
    headline?: HeadlineTypes;
    align?: TextAlign;
}

interface HeadlineType {
    size: TextSize;
    weight: TextWeight;
}

function getHeadline(headline: HeadlineTypes): HeadlineType {
    switch (headline) {
        case 'h1':
            return { size: 'extra-large', weight: 'bold' };
        case 'h2':
            return { size: 'large', weight: 'medium' };
        case 'h3':
        default:
            return { size: 'medium', weight: 'medium' };
    }
}

function Text({ headline: HeadlineTag, tag: Tag = 'span', weight, size, children, align }: Props) {
    if (HeadlineTag && (size || weight)) {
        Logger.warn('Headlines are predefined. You should not override size or weight.');
        return null;
    }

    if (HeadlineTag) {
        const { size: headlineSize, weight: headlineWeight } = getHeadline(HeadlineTag);
        return (
            <HeadlineTag
                className={classNamesHelper([Classes[`size-${headlineSize}`], Classes[`weight-${headlineWeight}`], align && Classes[`align-${align}`]])}
            >
                {children}
            </HeadlineTag>
        );
    }

    return (
        <Tag
            className={classNamesHelper([
                Classes[size ? `size-${size}` : 'size-medium'],
                Classes[weight ? `weight-${weight}` : 'weight-light'],
                align && Classes[`align-${align}`]
            ])}
        >
            {children}
        </Tag>
    );
}

export default Text;
