/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import Text from 'shop/client/components/atoms/text/Text';

describe('<Text /> component', () => {
    it('should render nothing if headline contains size or weight', async () => {
        const content = 'Default Text';
        await renderAsync(
            <Text headline="h1" size="extra-large">
                {content}
            </Text>
        );
        const text = screen.queryByRole('heading', { name: content });
        expect(text).toBeNull();
    });

    it('should render h1 through h3 tags', async () => {
        for (const tagName of ['h1', 'h2', 'h3'] as const) {
            await renderAsync(<Text headline={tagName}>{tagName}</Text>);
            const tag = screen.getByText(tagName);
            expect(tag).toHaveTextContent(tagName);
        }
    });

    it('should render bold large text', async () => {
        await renderAsync(
            <Text weight="bold" size="large">
                Bold Large Text
            </Text>
        );
        const className = 'size-large weight-bold';
        expect(screen.getByText('Bold Large Text')).toHaveClass(className);
    });

    it('should render light text', async () => {
        await renderAsync(<Text weight="light">Light Text</Text>);
        const className = 'size-medium weight-light';
        expect(screen.getByText('Light Text')).toHaveClass(className);
    });

    it('should render medium bold text', async () => {
        await renderAsync(<Text weight="medium">Medium Bold Text</Text>);
        const className = 'size-medium weight-medium';
        expect(screen.getByText('Medium Bold Text')).toHaveClass(className);
    });

    it('should render bold text', async () => {
        await renderAsync(<Text weight="bold">Bold Text</Text>);
        const className = 'size-medium weight-bold';
        expect(screen.getByText('Bold Text')).toHaveClass(className);
    });

    it('should render small text', async () => {
        await renderAsync(<Text size="small">Small Text</Text>);
        const className = 'size-small weight-light';
        expect(screen.getByText('Small Text')).toHaveClass(className);
    });

    it('should render medium text', async () => {
        await renderAsync(<Text size="medium">Medium Text</Text>);
        const className = 'size-medium weight-light';
        expect(screen.getByText('Medium Text')).toHaveClass(className);
    });

    it('should render large text', async () => {
        await renderAsync(<Text size="large">Large Text</Text>);
        const className = 'size-large weight-light';
        expect(screen.getByText('Large Text')).toHaveClass(className);
    });

    it('should render extra large text', async () => {
        await renderAsync(<Text size="extra-large">Extra Large Text</Text>);
        const className = 'size-extra-large weight-light';
        expect(screen.getByText('Extra Large Text')).toHaveClass(className);
    });
});
