/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import FormError from 'shop/client/components/atoms/form/error/FormError';

const firstName = 'firstName';
const lastName = 'lastName';
const errorFirstName = 'error firstName';

const errors = {
    firstName: { message: errorFirstName, ref: { name: firstName }, type: 'required' }
};

describe('<FormError/> component:', () => {
    it('should render an error if the field has an error', async () => {
        await renderAsync(<FormError name={firstName} errors={errors} />);
        expect(screen.getByText(errorFirstName)).toBeInTheDocument();
    });

    it('should render no error if the field has no error', async () => {
        await renderAsync(<FormError name={lastName} errors={errors} />);
        expect(screen.queryByText(errorFirstName)).not.toBeInTheDocument();
    });

    it('should render no error if errors is missing', async () => {
        await renderAsync(<FormError name={firstName} />);
        expect(screen.queryByText(errorFirstName)).not.toBeInTheDocument();
    });
});
