/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ErrorMessage } from '@hookform/error-message';

import { FieldErrors } from 'react-hook-form';

import Classes from 'shop/client/components/atoms/form/error/FormError.scss';

interface Props {
    errors?: FieldErrors;
    name: string;
}

function FormError({ errors, name }: Props) {
    if (errors) {
        return <ErrorMessage as={<p className={Classes.error} />} errors={errors} name={name} />;
    }
    return null;
}

export default FormError;
