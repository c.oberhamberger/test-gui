/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Content, Icon, Portal, Root, ScrollDownButton, ScrollUpButton, SelectProps, Trigger, Value, Viewport } from '@radix-ui/react-select';

import { ForwardedRef, forwardRef } from 'react';
import { ControllerRenderProps, FieldPath, FieldValues } from 'react-hook-form';

import IconComponent from 'shop/client/components/atoms/icon/Icon';

import Classes from 'shop/client/components/atoms/select/Select.scss';

interface SelectInterface<TFormData extends FieldValues, TName extends FieldPath<TFormData>> extends SelectProps {
    label: string;
    field: ControllerRenderProps<TFormData, TName>;
}

function FormSelectInner<TFormData extends FieldValues, TName extends FieldPath<TFormData>>(
    { name, children, label, defaultValue, field, ...props }: SelectInterface<TFormData, TName>,
    forwardedRef: ForwardedRef<HTMLButtonElement>
) {
    // Workaround until issue https://github.com/radix-ui/primitives/issues/1658 in the library gets fixed
    function setRef(ref) {
        if (!ref) {
            return;
        }
        ref.ontouchstart = (event) => {
            event.preventDefault();
        };
    }

    return (
        <Root defaultValue={defaultValue} onValueChange={field.onChange} {...field} {...props}>
            <Trigger className={Classes.selectTrigger} ref={forwardedRef}>
                <Value placeholder={label} />
                <Icon>
                    <IconComponent type="caret-down" size="small" />
                </Icon>
            </Trigger>
            <Portal>
                <Content className={Classes.selectContent} position="popper" ref={setRef}>
                    <ScrollUpButton className={Classes.selectScrollButton}>
                        <IconComponent type="caret-up" />
                    </ScrollUpButton>
                    <Viewport>{children}</Viewport>
                    <ScrollDownButton className={Classes.selectScrollButton}>
                        <IconComponent type="caret-down" />
                    </ScrollDownButton>
                </Content>
            </Portal>
        </Root>
    );
}

const FormSelect = forwardRef(FormSelectInner) as <TFormData extends FieldValues, TName extends FieldPath<TFormData>>(
    props: SelectInterface<TFormData, TName> & { ref?: ForwardedRef<HTMLButtonElement> }
) => ReturnType<typeof FormSelectInner>;

export default FormSelect;
