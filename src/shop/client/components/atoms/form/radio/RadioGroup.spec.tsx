/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/

/* eslint-disable react/jsx-no-bind */
import { fireEvent, renderAsync, renderHook, screen } from '@archibald/testing';

import { Control, Controller, useForm } from 'react-hook-form';

import RadioGroup from 'shop/client/components/atoms/form/radio/RadioGroup';
import { CheckoutFormData } from 'shop/client/components/molecules/checkout/form/CheckoutForm';

describe('<RadioGroup/> component:', () => {
    const creditCard = 'creditcard';
    const invoice = 'invoice';
    const creditCardLabel = 'Credit Card';
    const invoiceLabel = 'Invoice';

    const options = [
        {
            value: creditCard,
            label: creditCardLabel
        },
        {
            value: invoice,
            label: invoiceLabel
        }
    ];

    let control: Control<CheckoutFormData>;

    beforeEach(() => {
        const { result } = renderHook(() => {
            const { control } = useForm<CheckoutFormData>();
            return control;
        });
        control = result.current;
    });

    it('should have the default value selected', async () => {
        await renderAsync(
            <Controller
                control={control}
                name="payment"
                render={({ field }) => <RadioGroup label="Payment Options" defaultValue={invoice} options={options} field={field} />}
            />
        );

        expect(screen.getByLabelText(invoiceLabel)).toBeChecked();
    });

    it('should select a different value', async () => {
        await renderAsync(
            <Controller
                control={control}
                name="payment"
                render={({ field }) => <RadioGroup label="Payment Options" defaultValue={invoice} options={options} field={field} />}
            />
        );

        expect(screen.getByLabelText(invoiceLabel)).toBeChecked();

        fireEvent.click(screen.getByLabelText(creditCardLabel));

        expect(screen.getByLabelText(invoiceLabel)).not.toBeChecked();
        expect(screen.getByLabelText(creditCardLabel)).toBeChecked();
    });
});
