/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Indicator, Item, Root } from '@radix-ui/react-radio-group';

import { ForwardedRef, forwardRef } from 'react';
import { ControllerRenderProps, FieldPath, FieldValues } from 'react-hook-form';

import { RadioOption } from 'shop/client/interfaces/base';

import Flex from 'shop/client/features/layout/components/flex/Flex';

import Classes from 'shop/client/components/atoms/form/radio/RadioGroup.scss';

interface Props<TFormData extends FieldValues, TName extends FieldPath<TFormData>> {
    options: RadioOption[];
    label?: string;
    defaultValue?: string;
    orientation?: 'horizontal' | 'vertical';
    field: ControllerRenderProps<TFormData, TName>;
}

function RadioGroupInner<TFormData extends FieldValues, TName extends FieldPath<TFormData>>(
    { options, label, defaultValue, orientation = 'vertical', field }: Props<TFormData, TName>,
    forwardedRef: ForwardedRef<HTMLDivElement>
) {
    return (
        <Root
            className={Classes.root}
            defaultValue={defaultValue}
            aria-label={label}
            orientation={orientation}
            onValueChange={field.onChange}
            {...field}
            ref={forwardedRef}
        >
            {options.map((option) => (
                <Flex align="center" key={option.value}>
                    <Item className={Classes.item} value={option.value} id={option.value}>
                        <Indicator className={Classes.indicator} />
                    </Item>
                    <label className={Classes.label} htmlFor={option.value}>
                        {option.label}
                    </label>
                </Flex>
            ))}
        </Root>
    );
}

const RadioGroup = forwardRef(RadioGroupInner) as <TFormData extends FieldValues, TName extends FieldPath<TFormData>>(
    props: Props<TFormData, TName> & { ref?: ForwardedRef<HTMLDivElement> }
) => ReturnType<typeof RadioGroupInner>;

export default RadioGroup;
