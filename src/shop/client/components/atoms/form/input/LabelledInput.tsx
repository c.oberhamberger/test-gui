/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { FieldValuesFromFieldErrors } from '@hookform/error-message';

import { DetailedHTMLProps, InputHTMLAttributes } from 'react';
import { FieldErrors, FieldName, FieldPath, FieldValues, RegisterOptions, UseFormRegister } from 'react-hook-form';

import FormError from 'shop/client/components/atoms/form/error/FormError';
import FormInput from 'shop/client/components/atoms/form/input/FormInput';
import Flex from 'shop/client/features/layout/components/flex/Flex';

interface Props<TFormData extends FieldValues> extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    labelText: string;
    id: FieldPath<TFormData>;
    register?: UseFormRegister<TFormData>;
    registerOptions?: RegisterOptions;
    errors?: FieldErrors<TFormData>;
}

function LabelledInput<TFormData extends FieldValues>({ id, labelText, children, errors, ...rest }: Props<TFormData>) {
    return (
        <Flex direction="column" py="3">
            <label htmlFor={id}>{labelText}</label>
            {children || <FormInput id={id} {...rest} />}
            <FormError errors={errors} name={id as unknown as FieldName<FieldValuesFromFieldErrors<FieldErrors<TFormData>>>} />
        </Flex>
    );
}

export default LabelledInput;
