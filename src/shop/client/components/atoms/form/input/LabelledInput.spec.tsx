/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import LabelledInput from 'shop/client/components/atoms/form/input/LabelledInput';

const firstNameLabel = 'First name';
const firstName = 'firstName';
const errorFirstName = 'error firstName';

const errors = {
    firstName: { message: errorFirstName, ref: { name: firstName }, type: 'required' }
};

describe('<LabelledInput/> component:', () => {
    it('should render a label and an input field without an error', async () => {
        await renderAsync(<LabelledInput labelText={firstNameLabel} id={firstName} errors={{}} />);
        expect(screen.getByLabelText(firstNameLabel)).toBeInTheDocument();
        expect(screen.queryByText(errorFirstName)).not.toBeInTheDocument();
    });

    it('should render an error message', async () => {
        await renderAsync(<LabelledInput labelText={firstNameLabel} id={firstName} errors={errors} />);
        expect(screen.getByText(errorFirstName)).toBeInTheDocument();
    });
});
