/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { DetailedHTMLProps, InputHTMLAttributes } from 'react';
import { FieldPath, FieldValues, RegisterOptions, UseFormRegister } from 'react-hook-form';

import type { IconType } from 'shop/client/interfaces/base';

interface Props<TFormData extends FieldValues> extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    icon?: IconType;
    onIconClick?: (value: string) => void;
    register?: UseFormRegister<TFormData>;
    registerOptions?: RegisterOptions;
    id: FieldPath<TFormData>;
}

function FormInput<TFormData extends FieldValues>({
    icon,
    onIconClick,
    onChange,
    type = 'text',
    register,
    id,
    registerOptions = {},
    ...restProps
}: Props<TFormData>) {
    if (register && id) {
        return <input id={id} type={type} {...restProps} {...register(id, registerOptions)} />;
    }
    return <input id={id} type={type} {...restProps} />;
}

export default FormInput;
