/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { memo } from 'react';

import type { IconSize, IconType } from 'shop/client/interfaces/base';

import BurgerIcon from 'shop/client/components/atoms/icon/icon-types/BurgerIcon';
import CaretDownIcon from 'shop/client/components/atoms/icon/icon-types/CaretDownIcon';
import CaretLeftIcon from 'shop/client/components/atoms/icon/icon-types/CaretLeftIcon';
import CaretRightIcon from 'shop/client/components/atoms/icon/icon-types/CaretRightIcon';
import CaretUpIcon from 'shop/client/components/atoms/icon/icon-types/CaretUpIcon';
import CartIcon from 'shop/client/components/atoms/icon/icon-types/CartIcon';
import CheckIcon from 'shop/client/components/atoms/icon/icon-types/CheckIcon';
import CloseIcon from 'shop/client/components/atoms/icon/icon-types/CloseIcon';
import HeartFilledIcon from 'shop/client/components/atoms/icon/icon-types/HeartFilledIcon';
import HeartIcon from 'shop/client/components/atoms/icon/icon-types/HeartIcon';
import SearchIcon from 'shop/client/components/atoms/icon/icon-types/SearchIcon';
import UserIcon from 'shop/client/components/atoms/icon/icon-types/UserIcon';
import UserLoggedInIcon from 'shop/client/components/atoms/icon/icon-types/UserLoggedInIcon';

interface Props {
    type: IconType;
    size?: IconSize;
    testId?: string;
}

function getIconMeasures(iconSize?: IconSize) {
    switch (iconSize) {
        case 'extra-small':
            return { width: '16', height: '16' };
        case 'small':
            return { width: '20', height: '20' };
        case 'medium':
            return { width: '24', height: '24' };
        case 'large':
            return { width: '27', height: '27' };
        case 'extra-large':
            return { width: '32', height: '32' };
        default:
            return { width: '16', height: '16' };
    }
}

function Icon({ type, size, testId }: Props) {
    const { height, width } = getIconMeasures(size);

    switch (type) {
        case 'burger':
            return <BurgerIcon testId={testId} width={width} height={height} />;
        case 'caret-down':
            return <CaretDownIcon width={width} height={height} />;
        case 'caret-left':
            return <CaretLeftIcon width={width} height={height} />;
        case 'caret-right':
            return <CaretRightIcon width={width} height={height} />;
        case 'caret-up':
            return <CaretUpIcon width={width} height={height} />;
        case 'cart':
            return <CartIcon width={width} height={height} />;
        case 'check':
            return <CheckIcon width={width} height={height} />;
        case 'close':
            return <CloseIcon width={width} height={height} />;
        case 'heart':
            return <HeartIcon testId={testId} width={width} height={height} />;
        case 'heart-filled':
            return <HeartFilledIcon width={width} height={height} />;
        case 'search':
            return <SearchIcon width={width} height={height} />;
        case 'user':
            return <UserIcon testId={testId} width={width} height={height} />;
        case 'user-logged-in':
            return <UserLoggedInIcon testId={testId} width={width} height={height} />;

        default:
            return null;
    }
}

export default memo(Icon);
