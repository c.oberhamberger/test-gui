/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { IconInterface } from 'shop/client/interfaces/base';

function BurgerIcon({ height = '16', width = '16', testId }: IconInterface) {
    return (
        <svg data-testid={testId} width={width} height={height} fill="currentColor" viewBox="0 0 16 16">
            <path d="M0 9H19V10H0z" />
            <path d="M0 14H19V15H0z" />
            <path d="M0 4H19V5H0z" />
        </svg>
    );
}

export default BurgerIcon;
