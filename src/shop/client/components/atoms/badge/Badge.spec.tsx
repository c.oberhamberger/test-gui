/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { renderAsync, screen } from '@archibald/testing';

import Badge from 'shop/client/components/atoms/badge/Badge';

describe('<Badge /> component:', () => {
    it('should exist ', async () => {
        const container = await renderAsync(<Badge text="" />);
        expect(container).toBeDefined();
    });
    it('should render children', async () => {
        const content = 'Test';
        await renderAsync(<Badge text={content} />);
        const textContent = screen.getByText(content);
        expect(textContent).toHaveTextContent(content);
    });
});
