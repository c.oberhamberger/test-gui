/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ConfigManager } from '@archibald/core';
import { renderAsync, screen } from '@archibald/testing';

import Link from 'shop/client/components/atoms/link/Link';

const defaultProps = {
    className: 'test-class',
    to: '/test'
};

describe('<Link /> component:', () => {
    const content = 'Content';

    it('should exist', async () => {
        await renderAsync(<Link {...defaultProps}>{content}</Link>);
        const link = screen.getByRole('link', { name: content });
        expect(link).toBeDefined();
    });

    it('should render children', async () => {
        ConfigManager.set({ defaultLanguage: 'de', supportedLanguages: ['de'] });
        await renderAsync(<Link {...defaultProps}>{content}</Link>);
        const link = screen.getByRole('link', { name: content });
        expect(link).toHaveClass(defaultProps.className);
        expect(link).toHaveTextContent(content);
    });

    it('should navigate to /de/test', async () => {
        ConfigManager.set({ defaultLanguage: 'de', supportedLanguages: ['de'] });
        await renderAsync(<Link {...defaultProps}>{content}</Link>);
        const link = screen.getByRole('link', { name: content });
        expect(link).toHaveAttribute('href', `/de${defaultProps.to}`);
    });
});
