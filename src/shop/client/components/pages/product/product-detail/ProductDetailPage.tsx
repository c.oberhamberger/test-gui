/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { useParams } from '@archibald/core';
import { CMSProvider } from '@archibald/storefront';

import { PageLabel } from 'shop/client/constants';

import type { ProductRouteParams } from 'shop/client/interfaces/base';

import { usePageFetch } from 'shop/client/hooks/cms';
import { useProduct } from 'shop/client/hooks/product';

import HeadManager from 'shop/client/components/support/head-manager/HeadManager';
import PageTemplate from 'shop/client/components/support/templates/PageTemplate';

function ProductDetailPage() {
    const { data: page, isLoading, isError } = usePageFetch(PageLabel.PRODUCT_DETAIL);
    const { productId } = useParams<ProductRouteParams>();
    const { isError: productError } = useProduct(productId);

    return (
        <CMSProvider page={page} loading={isLoading} error={isError || productError}>
            <HeadManager />
            <PageTemplate />
        </CMSProvider>
    );
}

export default ProductDetailPage;
