/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { actionSetHttpStatus, HttpCode } from '@archibald/core';

import { useDispatch } from 'react-redux';

import NotFound from 'shop/client/components/molecules/error/not-found/NotFound';
import Container from 'shop/client/features/layout/components/container/Container';

function NotFoundTemplate() {
    const dispatch = useDispatch();

    dispatch(actionSetHttpStatus(HttpCode.CODE_404));

    return (
        <Container>
            <NotFound />
        </Container>
    );
}

export default NotFoundTemplate;
