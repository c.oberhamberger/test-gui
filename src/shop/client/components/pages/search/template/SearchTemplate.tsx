/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSPosition } from 'shop/client/constants';

import Slot from 'shop/client/components/support/slot/Slot';
import Column from 'shop/client/features/layout/components/column/Column';
import Container from 'shop/client/features/layout/components/container/Container';
import Row from 'shop/client/features/layout/components/row/Row';

function SearchTemplate() {
    return (
        <Container>
            <Row>
                <Column>
                    <Slot position={CMSPosition.LEFT_CONTENT} />
                </Column>

                <Column>
                    <Slot position={CMSPosition.CONTENT} />
                </Column>

                <Column>
                    <Slot position={CMSPosition.RIGHT_CONTENT} />
                </Column>
            </Row>
            <Row>
                <Column>
                    <Slot position={CMSPosition.BOTTOM_CONTENT} />
                </Column>
            </Row>
        </Container>
    );
}

export default SearchTemplate;
