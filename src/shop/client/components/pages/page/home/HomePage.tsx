/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CMSProvider } from '@archibald/storefront';

import { PageLabel } from 'shop/client/constants';

import { usePageFetch } from 'shop/client/hooks/cms';

import HeadManager from 'shop/client/components/support/head-manager/HeadManager';
import PageTemplate from 'shop/client/components/support/templates/PageTemplate';

function HomePage() {
    const { data, isLoading, isError } = usePageFetch(PageLabel.FRONTPAGE);

    return (
        <CMSProvider page={data} loading={isLoading} error={isError}>
            <HeadManager />
            <PageTemplate />
        </CMSProvider>
    );
}

export default HomePage;
