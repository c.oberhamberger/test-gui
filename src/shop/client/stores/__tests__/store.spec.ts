/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2022 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { JsonCompressionHelper } from '@archibald/core';
import { actionSetCmsTicketId } from '@archibald/storefront';

import StoreHelper from 'shop/client/stores/store';

const originalAppCode = process.env.APP_CODE;

describe('redux store configuration', () => {
    beforeEach(() => {
        process.env.APP_CODE = 'client';
        // @ts-ignore
        delete window.__INITIAL_STATE__;
        // @ts-ignore
        delete StoreHelper['_store'];
    });

    it('should share state on client', async () => {
        const store = StoreHelper.getStore();
        expect(store.getState().cms.cmsTicketId).toEqual(null);
        StoreHelper.getStore().dispatch(actionSetCmsTicketId('123'));
        const store2 = StoreHelper.getStore();
        expect(store2.getState().cms.cmsTicketId).toEqual('123');
        expect(store).toBe(store2);
    });

    it('should always return new state on server', async () => {
        process.env.APP_CODE = 'server';
        const store = StoreHelper.getStore();
        const store2 = StoreHelper.getStore();
        expect(store).not.toBe(store2);
    });

    it('should load transfer state on client compressed', async () => {
        window.__INITIAL_STATE__ = JsonCompressionHelper.compress({ cms: { cmsTicketId: '123' } });
        const store = StoreHelper.getStore();
        expect(store.getState().cms.cmsTicketId).toEqual('123');
    });

    it('should load transfer state on client uncompressed', async () => {
        window.__INITIAL_STATE__ = { cms: { cmsTicketId: '123' } } as any;
        const store = StoreHelper.getStore();
        expect(store.getState().cms.cmsTicketId).toEqual('123');
    });

    afterEach(() => {
        process.env.APP_CODE = originalAppCode;
    });
});
