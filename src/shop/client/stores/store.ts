/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { JsonCompressionHelper } from '@archibald/core';
import type { DefaultStore } from '@archibald/core';

import type { CustomState } from 'shop/client/reducers';

import configureStore from 'shop/client/stores/configure';

class StoreHelper {
    private static _store: DefaultStore<CustomState>;
    private static _initialState: CustomState;

    private static get initialState() {
        let initialState = {} as CustomState | undefined;
        if (process.env.APP_CODE === 'client') {
            if (JsonCompressionHelper.isCompressed(window.__INITIAL_STATE__)) {
                initialState = JsonCompressionHelper.decompress<CustomState>(window.__INITIAL_STATE__);
            } else {
                initialState = window.__INITIAL_STATE__;
            }
        }
        return initialState;
    }

    private static init() {
        if (process.env.APP_CODE === 'server') {
            return configureStore(this.initialState);
        }
        this._store = configureStore(this.initialState);
        this._initialState = this._store.getState();
        this._store['getInitialState'] = () => this._initialState;
        return this._store;
    }

    public static getStore() {
        if (!this._store) {
            return this.init();
        }
        return this._store;
    }
}

export default StoreHelper;
