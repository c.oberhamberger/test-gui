/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DefaultResponseError } from '@archibald/core';
import type { DefaultCart, DefaultCartEntry } from '@archibald/storefront';

import type { Error, Product, User } from 'shop/client/interfaces/base';

export interface Cart extends DefaultCart {
    entries: CartEntry[];
    user: User;
}

export interface CartEntry extends DefaultCartEntry {
    product: Product;
}

export interface CartErrors extends DefaultResponseError {
    errors?: Error[];
}

export interface CartModification {
    deliveryModeChanged: boolean;
    entry: CartEntry;
    quantity: number;
    quantityAdded: number;
    statusCode: string;
    statusMessage: string;
    errors: Error[];
}
