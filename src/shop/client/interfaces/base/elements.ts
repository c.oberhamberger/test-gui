/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { IconType } from 'shop/client/interfaces/base/icon';

export interface IconInterface {
    type?: IconType;
    className?: string;
    width?: string;
    height?: string;
    testId?: string;
}

export interface DropdownOption {
    label: string;
    value: string;
}

export type RadioOption = DropdownOption;
