/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DefaultProduct } from '@archibald/storefront';

export interface ProductBrand {
    code: string;
    name: string;
    url: string;
    image: ProductImage;
}

export interface ProductCategory {
    code: string;
    name: string;
    slug: string;
    urlHash: string;
}

export interface ProductFeature {
    label: string;
    labelCode: string;
    values: { value: string }[];
    topFact?: boolean;
}

export interface ProductImage {
    url: string;
    alt?: string;
}

export interface ProductPrice {
    currencyIso: string;
    value: number;
    formattedValue: string;
}

export interface Product extends DefaultProduct {
    brand?: ProductBrand;
    categories: ProductCategory[];
    code: string;
    custom?: string;
    description?: string;
    features?: ProductFeature[];
    images: ProductImage[];
    price: ProductPrice;
    summary?: string;
    url?: string;
}

export type ProductRouteParams = {
    productId: string;
};

export type CategoryRouteParams = {
    categoryId: string;
};
