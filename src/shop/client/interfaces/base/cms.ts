/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DefaultCMSPage } from '@archibald/core';
import type { DefaultCMSComponent, DefaultCMSSlot, DefaultCMSState, LoadableComponent, DefaultCMSPages } from '@archibald/storefront';

import type { ComponentType } from 'react';
import type { ColumnsSizeType } from 'shop/client/features/layout/interfaces';

import type { CMSComponentTypeCode, CMSPosition, CMSTemplate } from 'shop/client/constants';

export interface StaticSlotComponentInterface {
    ProductGridComponent?: {
        brandName: string;
    };
}

export interface CMSComponent extends DefaultCMSComponent {
    index?: number;
    uuid?: string;
    className?: string;
    contentType?: string;
    title?: string;
    typeCode: CMSComponentTypeCode;
    columns?: ColumnsSizeType;
    staticSlotComponentProp?: StaticSlotComponentInterface;
}

export type CMSComponentClass = LoadableComponent<ComponentType<CMSComponent>>;

export interface CMSPage extends DefaultCMSPage {
    uid: string;
    name: string;
    catalogVersionUuid?: string;
    template: CMSTemplate;
    contentSlots: CMSSlot[];
}

export interface CMSPages extends DefaultCMSPages {
    [key: string]: CMSPage;
}

export type CMSState = DefaultCMSState;

export interface CMSSlot extends DefaultCMSSlot {
    components: CMSComponent[];
    position: CMSPosition;
}

export interface SmarteditPreviewTicket {
    redirectUrl: string;
}

export interface ISmartEditAttributes {
    'data-smartedit-component-type': string;
    'data-smartedit-component-id': string;
    'data-smartedit-component-uuid': string;
    'data-smartedit-catalog-version-uuid': string;
}
