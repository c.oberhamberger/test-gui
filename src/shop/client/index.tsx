/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { registerServiceWorker } from '@archibald/client';
import { Logger, HistoryManager, StoreManager, SuspenseRouter } from '@archibald/core';
import { Cookies } from '@archibald/server';
import { ClientProvider, Renderer } from '@archibald/storefront';

import DataClient from 'shop/client/api/creators/data';

import Config from 'shop/client/config/config';

import App from 'shop/client/components/App';

import StoreHelper from 'shop/client/stores/store';

import 'shop/resources/scss/base/global.scss';

const cookies = new Cookies();
const store = StoreHelper.getStore();
const history = HistoryManager.get();

StoreManager.init(store);

Logger.setLogLevel(cookies.get('LogLevel') ?? Config.get('app.logLevel'));

const app = (
    <ClientProvider store={store} dataClient={DataClient}>
        <SuspenseRouter history={history} scrollToTop>
            <App />
        </SuspenseRouter>
    </ClientProvider>
);

const root = document.getElementById('app');

Renderer.render(app, root, { topLevelSuspense: false });

registerServiceWorker().then((worker) => Logger.info(`Service-worker is ${!worker ? 'not ' : ''}registered.`));
