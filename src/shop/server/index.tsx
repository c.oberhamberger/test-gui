/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ConfigManager } from '@archibald/core';
import { Server } from 'shop/server/module/server';
import routes from 'shop/server/routes';
import type { VercelRequest, VercelResponse } from '@vercel/node';

ConfigManager.set({
    custom: {
        validUrlParams: ['page', 'q', 'cmsTicketId', 'sort']
    }
});
async function bootstrap(req: VercelRequest, res: VercelResponse) {
    // Override ConfigService example

    console.log('creating APP');

    const app = new Server();
    await app.init();
    await app.start();

    console.log('injecting request on APP: ', app);


    const internalResponse = await app.internal.inject(req as any);



    return res.send(internalResponse.result);
}

export default async function handler(req: VercelRequest, res: VercelResponse) {
    console.log('routes: ', routes);

    console.log('trying to bootstrap server on request: ');
    return bootstrap(req, res);
}


// import { HeadManager } from '@archibald/client';
// import { LoadingType, AppClient, type RouterContextInterface, StaticRouter } from '@archibald/core';
// import { RenderHelper, Renderer, FileHelper, ServerProvider } from '@archibald/server';
// import type { VercelRequest, VercelResponse } from '@vercel/node';

// import { join } from 'path';

// import DataClient from 'shop/client/api/creators/data';

// import App from 'shop/client/components/App';

// import StoreHelper from 'shop/client/stores/store';

// export default async function handler(req: VercelRequest, res: VercelResponse) {
//     console.log('requesting: ', req.url);
//     const template = `
//     <!DOCTYPE html>
//     <html dir="ltr" lang="{{ page.language }}" class="ie11">
//     <head>
//         <meta charSet="UTF-8"/>
//         <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
//         {{ page.tags }}
//         {{ state.head }}
//         <meta name="apple-mobile-web-app-capable" content="yes">
//         <meta name="apple-mobile-web-app-status-bar-style" content="default">
//         <meta name="apple-touch-fullscreen" content="yes">
//         <link type="image/x-icon" href="/favicon.ico" rel="shortcut icon">
//         {{ assets.css }}
//         {{ assets.preload.css }}
//         {{ assets.preload.js }}
//         {{ assets.critical.css }}
//         {{ assets.runtime }}
//     </head>
//     <body class="theme">
//     <div id="app" {{ hydrate }}>{{ markup }}</div>
//     {{ state.body }}
//     {{ assets.modules }}
//     {{ assets.critical.js }}
//     {{ assets.js }}
//     </body>
//     </html>
// `;
//     let html: string;

//     try {
//         const manager = new HeadManager();
//         const appClient = new AppClient();

//         const store = StoreHelper.getStore();
//         const renderer = new Renderer({ dataClient: DataClient, store, topLevelSuspense: false });
//         const context: Partial<RouterContextInterface> = {};

//         const initialLocation = `${req.url}`;
//         const app = (
//             <ServerProvider store={store} head={manager} app={appClient}>
//                 <StaticRouter location={initialLocation} context={context}>
//                     <App />
//                 </StaticRouter>
//             </ServerProvider>
//         );

//         const root = join(process.cwd(), 'dist');
//         FileHelper.setRoot(root, false);

//         const renderHelper = new RenderHelper({
//             loading: LoadingType.DEFER,
//             critical: { css: true, js: true, modules: true, runtime: true },
//             template: template
//         });
//         const markup = await renderer.render(app);
//         const page = { title: 'Test', language: 'de' };
//         const {
//             contexts: { hydrate }
//         } = renderer.getContext();

//         html = await renderHelper.getHTML({
//             page,
//             markup,
//             hydrate
//         });
//     } catch (err) {
//         console.error('Render error:', err);
//         return new Response(
//             `<!doctype html><h1>Internal application error</h1>
//         <p>The app failed to render. Check your Function logs.</p>`,
//             {
//                 status: 500,
//                 headers: {
//                     'Content-Type': 'text/html; charset=utf-8'
//                 }
//             }
//         );
//     }

//     res.status(200);
//     return res.send(html);
// }
