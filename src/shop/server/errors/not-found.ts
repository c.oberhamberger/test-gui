/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode } from '@archibald/core';

class NotFoundError extends Error {
    override name = 'NotFoundError';
    public url = '';
    public markup = '';
    public statusCode = HttpCode.CODE_404;

    constructor(message: string, markup: string, url?: string) {
        super(message);
        this.markup = markup;
        if (url) {
            this.url = url;
        }
    }
}

export default NotFoundError;
