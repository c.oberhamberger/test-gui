/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import ServerError from 'shop/server/errors/server';

describe('ServerError', () => {
    it('should have the name set ServerError', () => {
        const error = new ServerError('', '');
        expect(error.name).toEqual('ServerError');
    });

    it('should save the url and markup set ServerError', () => {
        const error = new ServerError('<Test/>', 'markup', 'testUrl');
        expect(error.name).toEqual('ServerError');
        expect(error.message).toEqual('<Test/>');
        expect(error.markup).toEqual('markup');
        expect(error.markup).toEqual('markup');
        expect(error.url).toEqual('testUrl');
    });
});
