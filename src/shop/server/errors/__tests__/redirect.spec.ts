/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode } from '@archibald/core';

import RedirectError from 'shop/server/errors/redirect';

describe('RedirectError', () => {
    it('should have the name set RedirectError', () => {
        const error = new RedirectError('');
        expect(error.name).toEqual('RedirectError');
    });

    it('should have the default statusCode set to 301 RedirectError', () => {
        const error = new RedirectError('');
        expect(error.statusCode).toEqual(HttpCode.CODE_302);
    });

    it('should save the url and statusCode to RedirectError', () => {
        const error = new RedirectError('testUrl', HttpCode.CODE_301);
        expect(error.name).toEqual('RedirectError');
        expect(error.url).toEqual('testUrl');
        expect(error.statusCode).toEqual(HttpCode.CODE_301);
    });
});
