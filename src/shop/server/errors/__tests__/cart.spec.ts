/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ErrorType } from '@archibald/core';

import CartError from 'shop/server/errors/cart';

import { ErrorReason } from 'shop/client/constants';

describe('CartError', () => {
    it('should have the name set CartError', () => {
        const error = new CartError();
        expect(error.name).toEqual('CartError');
    });

    it('should save the error CartError', () => {
        const testError = { type: ErrorType.ERROR, message: 'test', reason: ErrorReason.NOT_FOUND };
        const error = new CartError(testError);
        expect(error.name).toEqual('CartError');
        expect(error.errors).toEqual([testError]);
    });
});
