/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode } from '@archibald/core';

import NotFoundError from 'shop/server/errors/not-found';

describe('NotFoundError', () => {
    it('should have the name set NotFoundError', () => {
        const error = new NotFoundError('', '');
        expect(error.name).toEqual('NotFoundError');
        expect(error.statusCode).toEqual(HttpCode.CODE_404);
    });

    it('should save the url and markup set ServerError', () => {
        const error = new NotFoundError('<Test/>', 'markup', 'testUrl');
        expect(error.name).toEqual('NotFoundError');
        expect(error.message).toEqual('<Test/>');
        expect(error.statusCode).toEqual(HttpCode.CODE_404);
        expect(error.markup).toEqual('markup');
        expect(error.url).toEqual('testUrl');
    });
});
