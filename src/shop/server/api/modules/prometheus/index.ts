/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { RouteMethod } from '@archibald/core';
import { BaseModule, type CoreServer, Inject } from '@archibald/server';
import type { Request } from '@hapi/hapi';

import { PrometheusController } from 'shop/server/api/controllers';
import { PrometheusService } from 'shop/server/api/services';

interface PrometheusRequest extends Request {
    app: {
        requestStartedAt?: number;
    };
}

export class PrometheusModule extends BaseModule {
    @Inject()
    private readonly prometheusService: PrometheusService;

    constructor() {
        super('PrometheusModule');
    }

    override async register(server: CoreServer) {
        await server.registerControllers([PrometheusController]);
        await server.registerServices([PrometheusService]);
        await server.registerRoutes([
            {
                method: RouteMethod.GET,
                path: '/metrics',
                handler: 'PrometheusController.index'
            }
        ]);

        // Register a timer when a new request arrives:
        server.internal.ext('onRequest', (request: PrometheusRequest, h) => {
            request.app.requestStartedAt = new Date().getTime();
            return h.continue;
        });

        // Once the handler has processed the request we need to register a new
        // observation:
        server.internal.ext('onPostHandler', (request: PrometheusRequest, h) => {
            const requestTime = new Date().getTime() - (request.app?.requestStartedAt ?? 0);
            this.prometheusService.observe(request, requestTime);
            return h.continue;
        });
    }
}
