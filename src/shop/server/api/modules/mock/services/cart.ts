/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2021 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ANONYMOUS_USER_ID } from '@archibald/auth';
import { BaseService } from '@archibald/server';
import { DefaultAddress, PriceType, ProductType } from '@archibald/storefront';

import { v4 } from 'uuid';

import type { Cart, CartEntry, Product } from 'shop/client/interfaces/base';

const cartDatabase = new Map<string, any>();

export class MockCartService extends BaseService {
    constructor() {
        super('MockCartService');
    }

    private static createEmptyCart(userId: string) {
        const loggedIn = userId !== 'anonymous';
        return {
            code: loggedIn ? '123456789' : null,
            guid: !loggedIn ? v4() : null,
            entries: [],
            totalItems: 0,
            totalPrice: MockCartService.getPrice(),
            user: {
                id: loggedIn ? '123' : ANONYMOUS_USER_ID,
                uid: loggedIn ? '123' : ANONYMOUS_USER_ID,
                name: loggedIn ? 'Archibald' : 'Anonymous'
            }
        } as Cart;
    }

    private static getPrice(value = 0) {
        return {
            currencyIso: 'EUR',
            priceType: PriceType.BUY,
            formattedValue: `EUR ${value.toFixed(2)}`,
            value
        };
    }

    private static buildCartEntry(product: Product, entryNumber = 1, quantity = 1) {
        return {
            entryNumber: entryNumber,
            productType: ProductType.PRODUCT,
            quantity,
            product: product ?? null,
            basePrice: MockCartService.getPrice(product?.price?.value ?? 0),
            totalPrice: MockCartService.getPrice((product?.price?.value ?? 0) * quantity)
        };
    }

    public calculateCart(cart: Cart) {
        let quantity = 0;
        cart.entries.forEach((entry) => (quantity += entry.quantity));
        cart.totalItems = quantity;

        let priceValue = 0;
        cart.entries.forEach((entry) => (priceValue += entry.totalPrice.value));
        cart.totalPrice = MockCartService.getPrice(priceValue);
    }

    public saveCart(cart: Cart) {
        // TODO better handling of loggedin and not loggedin customers
        cartDatabase.set(cart.code ?? cart.guid, cart);
    }

    public getCart(cartId: string) {
        return cartDatabase.get(cartId);
    }

    public async createCart(userId: string) {
        const cart = MockCartService.createEmptyCart(userId);
        this.calculateCart(cart);
        this.saveCart(cart);
        return cart;
    }

    public async getCartForUser(cartId: string, userId: string) {
        const loggedIn = userId !== ANONYMOUS_USER_ID;
        if ((cartId.includes('-') && loggedIn) || (!cartId.includes('-') && !loggedIn)) {
            throw new Error('Tried to access users cart as anonymous or tried to access anonymous cart as loggedIn user');
        }
        return cartDatabase.get(cartId);
    }

    public async addProductToCart(cartId: string, userId: string, product: Product, quantity: number) {
        let cart = (await this.getCartForUser(cartId, userId)) || MockCartService.createEmptyCart(userId);

        const entry = cart.entries.find((entry) => entry.product.code === product.code);

        if (entry) {
            const productsNo = entry.quantity + quantity;
            const updatedEntry = {
                ...entry,
                quantity: productsNo,
                totalPrice: MockCartService.getPrice(entry.product.price.value * productsNo)
            };
            const updatedEntries = cart.entries.map((entry): CartEntry => (entry.product.code === product.code ? updatedEntry : entry));

            cart = {
                ...cart,
                entries: updatedEntries,
                totalItems: cart.totalItems + updatedEntry.quantity,
                totalPrice: MockCartService.getPrice((product.price?.value ?? 0) * quantity)
            } as Cart;
        } else {
            cart.entries.push(MockCartService.buildCartEntry(product, cart.entries.length + 1, quantity));
        }

        this.calculateCart(cart);
        this.saveCart(cart);
        return cart;
    }

    public async updateProductInCart(cartId: string, userId: string, product: Product, quantity: number) {
        let cart = (await this.getCartForUser(cartId, userId)) || MockCartService.createEmptyCart(userId);

        const entry = cart.entries.find((entry) => entry.product.code === product.code);

        if (entry) {
            const productsNo = entry.quantity + quantity;
            const updatedEntry = {
                ...entry,
                quantity: productsNo,
                totalPrice: MockCartService.getPrice(entry.product.price.value * productsNo)
            };
            const updatedEntries = cart.entries.map((entry): CartEntry => (entry.product.code === product.code ? updatedEntry : entry));

            cart = {
                ...cart,
                entries: updatedEntries,
                totalItems: cart.totalItems + updatedEntry.quantity,
                totalPrice: MockCartService.getPrice((product.price?.value ?? 0) * quantity)
            } as Cart;
        } else {
            throw new Error('Entry not found');
        }

        this.calculateCart(cart);
        this.saveCart(cart);
        return cart;
    }

    public async removeProductFromCart(cartId: string, userId: string, entryNumber: number) {
        const oldCart = await this.getCartForUser(cartId, userId);
        if (!oldCart) {
            return null;
        }
        oldCart.entries = oldCart.entries.filter((entry) => entry.entryNumber !== entryNumber);
        this.calculateCart(oldCart);
        this.saveCart(oldCart);
        return oldCart;
    }

    public async createDeliveryAddress(cartId: string, userId: string, address: DefaultAddress) {
        const cart = await this.getCartForUser(cartId, userId);
        if (!cart) {
            return null;
        }
        cart.deliveryAddress = address;
        this.saveCart(cart);
        return address;
    }
}
