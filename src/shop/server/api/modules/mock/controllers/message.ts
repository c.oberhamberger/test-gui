/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, wait } from '@archibald/core';
import { BaseController } from '@archibald/server';
import { notFound } from '@hapi/boom';
import type { ResponseToolkit, Request } from '@hapi/hapi';

export class MockMessageController extends BaseController {
    protected override readonly name: string = 'MockMessageController';

    constructor() {
        super();
        this.logControllerRegister(this.name);
    }

    async get(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'index', this.name);
            const { version = 'v2' } = request.params;
            const { lang: language = 'en' } = request.query;
            await wait(200);
            const messages = require(`../responses/${version}/${language}/misc/messages.json`);
            return h.response(messages);
        } catch (ex) {
            if (ex instanceof Error) {
                Logger.error(ex);
                throw notFound(ex);
            }
            throw ex;
        }
    }
}
