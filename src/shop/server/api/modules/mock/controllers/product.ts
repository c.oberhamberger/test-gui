/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, wait } from '@archibald/core';
import { BaseController } from '@archibald/server';
import { notFound } from '@hapi/boom';
import type { ResponseToolkit, Request } from '@hapi/hapi';

import type { Product } from 'shop/client/interfaces/base';

export class MockProductController extends BaseController {
    protected override readonly name: string = 'MockProductController';

    constructor() {
        super();
        this.logControllerRegister(this.name);
    }

    private sortProducts(products: Product[], sort: string) {
        return products.sort((p1: Product, p2: Product) => {
            if (sort === 'price:desc') {
                return p2.price.value - p1.price.value;
            }
            if (sort === 'price:asc') {
                return p1.price.value - p2.price.value;
            }
            if (sort === 'name:asc') {
                return p1.name.localeCompare(p2.name);
            }
            return p2.name.localeCompare(p1.name);
        });
    }

    private async getData(results?: { products?: Product[] }, sort?: string, query?: string) {
        try {
            let products = [...(results?.products ?? [])];

            if (sort) {
                products = this.sortProducts(products, sort);
            }
            if (query) {
                products = products.filter((product: Product) => product.name.match(new RegExp(query, 'i'))) ?? [];
            }
            return { ...results, products };
        } catch (ex) {
            Logger.error(ex);
            return {};
        }
    }

    public async get(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'index', this.name);
            const { version = 'v2', productId } = request.params;
            const { lang: language = 'en' } = request.query;
            await wait(2000);
            const product = require(`../responses/${version}/${language}/product/${productId}.json`);
            return h.response(product);
        } catch (ex) {
            if (ex instanceof Error) {
                Logger.error(ex);
                throw notFound(ex);
            }
            throw ex;
        }
    }

    public async category(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'category', this.name);
            const { version, categoryId } = request.params;
            const { lang: language, sort } = request.query;
            await wait(250);
            const results = require(`../responses/${version}/${language}/search/${categoryId}.json`);
            const response = await this.getData(results, sort);
            return h.response(response);
        } catch (ex) {
            if (ex instanceof Error) {
                Logger.error(ex);
                throw notFound(ex);
            }
            throw ex;
        }
    }

    public async search(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'search', this.name);
            const { version } = request.params;
            const { lang: language, query, sort } = request.query;
            await wait(900);
            const results = require(`../responses/${version}/${language}/search/search-results.json`);
            const response = await this.getData(results, sort, query);
            return h.response(response);
        } catch (ex) {
            if (ex instanceof Error) {
                Logger.error(ex);
                throw notFound(ex);
            }
            throw ex;
        }
    }
}
