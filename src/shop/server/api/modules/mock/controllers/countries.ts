/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, wait } from '@archibald/core';
import { BaseController } from '@archibald/server';
import type { ResponseToolkit, Request } from '@hapi/hapi';

export class MockCountriesController extends BaseController {
    protected override readonly name: string = 'MockCountriesController';

    constructor() {
        super();
        this.logControllerRegister(this.name);
    }

    async get(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'index', this.name);
            const { version = 'v2' } = request.params;
            const { lang: language = 'en' } = request.query;
            await wait(200);
            const countries = require(`../responses/${version}/${language}/misc/countries.json`);
            return h.response(countries);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }
}
