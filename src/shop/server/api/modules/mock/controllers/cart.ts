/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CommonConstants, HttpCode, Logger, wait } from '@archibald/core';
import { BaseController, HapiService, Inject } from '@archibald/server';
import { DefaultAddress } from '@archibald/storefront';
import type { Request, ResponseToolkit } from '@hapi/hapi';

import { MockCartService } from 'shop/server/api/modules/mock/services';

import type { Product } from 'shop/client/interfaces/base';

interface CartRequest extends Request {
    payload: {
        product: Product;
        quantity: number;
    };
}

interface CartAddressRequest extends Request {
    payload: DefaultAddress;
}

export class MockCartController extends BaseController {
    protected override readonly name: string = 'MockCartController';

    private cartService: MockCartService;

    @Inject()
    private readonly hapiService: HapiService;

    constructor() {
        super();
        this.cartService = new MockCartService();
        this.logControllerRegister(this.name);
    }

    public async getCart(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'get', this.name);
            const { cartId } = request.params;
            const cookieCartId = this.hapiService.getCookie(CommonConstants.CART_ID);
            const cartResponse = await this.cartService.getCart(cartId || cookieCartId);
            await wait(200);
            if (!cartResponse) {
                // return h.response({ error: 'Not found' }).code(HttpCode.CODE_404);
                return h.response({ entries: [], totalPrice: 0 }).code(HttpCode.CODE_404);
            }
            return h.response(cartResponse);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }

    public async createCart(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'post', this.name);
            const { userId } = request.params;
            await wait(600);
            const cartResponse = await this.cartService.createCart(userId);
            if (!cartResponse) {
                return h.response({ error: 'cant create' }).code(HttpCode.CODE_500);
            }
            return h.response(cartResponse);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }

    public async addEntry(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'post', this.name);
            const { payload } = request as CartRequest;
            const { product, quantity } = payload ?? {};
            const { userId, cartId } = request.params;
            await wait(600);
            const cartResponse = await this.cartService.addProductToCart(cartId, userId, product, quantity);
            if (!cartResponse) {
                return h.response({ error: 'cant update' }).code(HttpCode.CODE_500);
            }
            return h.response(cartResponse);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }

    public async updateEntry(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'patch', this.name);
            const { payload } = request as CartRequest;
            const { product, quantity } = payload ?? {};
            const { cartId, userId } = request.params;
            await wait(400);
            const cartResponse = await this.cartService.updateProductInCart(cartId, userId, product, quantity);
            if (!cartResponse) {
                return h.response({ error: 'cant update' }).code(HttpCode.CODE_500);
            }
            return h.response(cartResponse);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }

    public async deleteEntry(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'delete', this.name);
            const { params } = request as CartRequest;
            const { cartId, userId, entryNumber } = params ?? {};
            await wait(400);
            const response = await this.cartService.removeProductFromCart(cartId, userId, Number(entryNumber));
            if (!response) {
                return h.response({ error: 'cant delete' }).code(HttpCode.CODE_500);
            }
            return h.response(response);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }

    public async createDeliveryAddress(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'post', this.name);
            const { params, payload } = request as CartAddressRequest;
            const { cartId, userId } = params ?? {};
            await wait(400);
            const response = await this.cartService.createDeliveryAddress(cartId, userId, payload);
            if (!response) {
                return h.response({ error: "can't create delivery address" }).code(HttpCode.CODE_500);
            }
            return h.response(response);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }
}
