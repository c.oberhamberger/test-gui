/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode, HttpHeader, Logger, wait } from '@archibald/core';
import { BaseController } from '@archibald/server';
import Boom from '@hapi/boom';
import type { ResponseToolkit, Request } from '@hapi/hapi';

import { writeFile, readFile } from 'fs/promises';
import { join } from 'path';
import { cwd } from 'process';

interface HybrisTokenResponse {
    access_token: string;
    token_type: 'bearer';
    refresh_token?: string;
    expires_in: number;
    scope: 'basic';
}

interface TokenRequest extends Request {
    payload: {
        grant_type: 'client_credentials' | 'password' | 'refresh_token';
        scope: string;
        username: string;
        password: string;
        refresh_token: string;
        client_id: string;
        client_secret: string;
    };
}

let currentRefreshCounter: number | null = null;
const tokenPath = join(cwd(), 'node_modules', '.token');

async function writeToken() {
    try {
        await writeFile(tokenPath, JSON.stringify({ counter: currentRefreshCounter }, null, 2), { encoding: 'utf8' });
    } catch {
        Logger.error('**** Error in writing mock auth token ****');
    }
}

async function readToken() {
    try {
        const content = (await readFile(tokenPath, { encoding: 'utf8' })) ?? '';
        return JSON.parse(content)?.counter ?? 0;
    } catch {
        return 0;
    }
}

async function generateToken(request: TokenRequest, type: 'token' | 'refresh') {
    const staticProvider = request.payload.client_id === 'admin_static';
    const suffix = staticProvider ? '_STATIC' : '';
    let counter = '';
    if (type === 'refresh') {
        currentRefreshCounter = (currentRefreshCounter ?? 0) + 1;
        counter = `_${currentRefreshCounter}`;
        await writeToken();
    }
    return `HYBRIS_${type.toUpperCase()}${suffix}${counter}`;
}

function validateToken(token: string | null, type: 'token' | 'refresh') {
    const checkCounter = type === 'refresh' ? token?.includes(`_${currentRefreshCounter}`) : true;
    const valid = token?.includes(`HYBRIS_${type.toUpperCase()}`) && checkCounter;
    if (type === 'refresh') {
        return {
            valid,
            reason: !valid ? `Refresh Counter expired: sent: ${token?.split('_').pop()} vs: ${currentRefreshCounter}` : 'Bad credentials'
        };
    }
    return { valid, reason: 'Bad credentials' };
}

export class MockAuthController extends BaseController {
    protected override readonly name: string = 'MockAuthController';

    private readonly FIVE_MINUTES = 5 * 60;

    private readonly THIRTY_MINUTES = 30 * 60;

    constructor() {
        super();
        this.logControllerRegister(this.name);
    }

    private validateToken(token: string | null) {
        return validateToken(token, 'token');
    }

    private validateRefreshToken(token: string | null) {
        return validateToken(token, 'refresh');
    }

    public async loadUser(request: Request, h: ResponseToolkit) {
        await wait(500);
        const headers = new Headers(request.headers);
        const token = headers.get(HttpHeader.AUTHORIZATION.toLowerCase());
        if (!this.validateToken(token)) {
            throw Boom.unauthorized('Unauthorized');
        }
        return h.response({ id: 123, name: 'Archibald Hybris Mock' });
    }

    protected checkCredentials(request: TokenRequest) {
        const staticProvider = request.payload.client_id === 'admin_static';
        let valid;
        let reason = 'Bad credentials';
        switch (request.payload.grant_type) {
            case 'client_credentials':
                valid = staticProvider && request.payload.client_secret === 'nimda';
                break;
            case 'refresh_token':
                const response = this.validateRefreshToken(request.payload.refresh_token);
                valid = response.valid;
                reason = response.reason;
                break;
            case 'password':
            default:
                valid = request.payload.username === 'admin' && request.payload.password === 'nimda';
        }
        return {
            valid,
            reason
        };
    }

    public async token(req: Request, h: ResponseToolkit) {
        try {
            if (currentRefreshCounter === null) {
                currentRefreshCounter = await readToken();
            }
            const request = req as TokenRequest;
            const staticProvider = request.payload.client_id === 'admin_static';
            await wait(200);
            this.logControllerEntry(request, 'authorize', this.name);
            const { valid, reason } = this.checkCredentials(request);
            if (valid) {
                const response = {
                    access_token: await generateToken(request, 'token'),
                    expires_in: staticProvider ? this.THIRTY_MINUTES : this.FIVE_MINUTES,
                    scope: 'basic',
                    token_type: 'bearer'
                } as HybrisTokenResponse;
                if (!staticProvider) {
                    response['refresh_token'] = await generateToken(request, 'refresh');
                }
                return h.response(response);
            }
            return h
                .response({
                    error: 'invalid_grant',
                    error_description: reason
                })
                .code(HttpCode.CODE_401);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }
}
