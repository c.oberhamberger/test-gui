/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HttpCode, Logger, wait } from '@archibald/core';
import { BaseController } from '@archibald/server';
import { Request, ResponseToolkit } from '@hapi/hapi';

import { MockOrderService } from 'shop/server/api/modules/mock/services/order';

export class MockOrderController extends BaseController {
    protected override readonly name: string = 'MockOrderController';

    private orderService: MockOrderService;

    constructor() {
        super();
        this.orderService = new MockOrderService();
        this.logControllerRegister(this.name);
    }

    public async placeOrder(request: Request, h: ResponseToolkit) {
        try {
            this.logControllerEntry(request, 'post', this.name);
            const { userId } = request.params;
            const { cartId } = request.query;
            await wait(600);
            const placeOrderResponse = await this.orderService.placeOrder(cartId, userId);
            if (!placeOrderResponse) {
                return h.response({ error: "can't place order" }).code(HttpCode.CODE_500);
            }
            return h.response(placeOrderResponse);
        } catch (ex) {
            Logger.error(ex);
            throw ex;
        }
    }
}
