/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HeadersHelper, RouteMethod } from '@archibald/core';
import { BaseModule, Cookies, type CoreServer } from '@archibald/server';
import { runInServerContext, ServerContext } from '@archibald/storage';
import type { Request, ResponseToolkit } from '@hapi/hapi';

import Joi from 'joi';

import {
    MockAuthController,
    MockCartController,
    MockCMSController,
    MockCountriesController,
    MockMessageController,
    MockOrderController,
    MockPaymentModesController,
    MockProductController
} from 'shop/server/api/modules/mock/controllers';

const defaultSwaggerParams = {
    version: Joi.string().required().default('v2'),
    basesite: Joi.string().required().default('netconomy.net')
};

const defaultSwaggerQuery = {
    _: Joi.string().optional(),
    lang: Joi.string().allow('en', 'de').default('de').optional()
};

// this can be used during development until backend part is ready
export class MockModule extends BaseModule {
    constructor() {
        super('MockModule');
    }

    private getHybrisPath() {
        const config = {
            'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': '' } },
            'app': {
                'api': {
                    'schema': '/{base}/{version}/{url}',
                    'protocol': null,
                    'host': null,
                    'port': null,
                    'base': 'api',
                    'version': 'v2',
                    'useMiddleware': true,
                    'retry': { 'times': 1, 'delay': 100 }
                },
                'logLevel': 'debug',
                'fixLanguage': true,
                'languageParamName': 'language',
                'supportedLanguages': ['de', 'en'],
                'defaultLanguage': 'de',
                'canonicalBaseUrl': 'http://127.0.0.1:3100',
                'route': { 'prefix': '/' },
                'multiTestParam': { 'attr': '1234' },
                'tenant': 'netconomy.net',
                'environment': 'local',
                'platform': 'shop',
                'baseSite': 'netconomy.net',
                'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
            },
            'hybris': {
                'api': {
                    'schema': '/{base}/{version}/{baseSite}/{url}',
                    'protocol': 'http',
                    'host': 'localhost',
                    'port': '9001',
                    'version': 'v2',
                    'base': 'rest',
                    'retry': { 'times': 1, 'delay': 100 },
                    'mocked': true
                },
                'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
            },
            'server': {
                'host': '0.0.0.0',
                'port': '3100',
                'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
            },
            'multi': {
                'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                'electronics': {
                    'app': {
                        'api': { 'version': 'v3' },
                        'multiTestParam': { 'attr': '5678' },
                        'tenant': 'electronics',
                        'environment': 'local',
                        'platform': 'shop',
                        'baseSite': 'electronics'
                    },
                    'hybris': {
                        'api': {
                            'schema': '/{base}/{version}/{baseSite}/{url}',
                            'protocol': 'http',
                            'host': 'localhost',
                            'port': '9001',
                            'version': 'v3',
                            'base': 'rest',
                            'retry': { 'times': 1, 'delay': 100 },
                            'mocked': true
                        },
                        'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                    }
                }
            }
        };

        return `/${config.hybris.api.base}/{version}/{basesite}`;
    }

    public override async register(server: CoreServer) {
        const config = {
            'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': '' } },
            'app': {
                'api': {
                    'schema': '/{base}/{version}/{url}',
                    'protocol': null,
                    'host': null,
                    'port': null,
                    'base': 'api',
                    'version': 'v2',
                    'useMiddleware': true,
                    'retry': { 'times': 1, 'delay': 100 }
                },
                'logLevel': 'debug',
                'fixLanguage': true,
                'languageParamName': 'language',
                'supportedLanguages': ['de', 'en'],
                'defaultLanguage': 'de',
                'canonicalBaseUrl': 'http://127.0.0.1:3100',
                'route': { 'prefix': '/' },
                'multiTestParam': { 'attr': '1234' },
                'tenant': 'netconomy.net',
                'environment': 'local',
                'platform': 'shop',
                'baseSite': 'netconomy.net',
                'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
            },
            'hybris': {
                'api': {
                    'schema': '/{base}/{version}/{baseSite}/{url}',
                    'protocol': 'http',
                    'host': 'localhost',
                    'port': '9001',
                    'version': 'v2',
                    'base': 'rest',
                    'retry': { 'times': 1, 'delay': 100 },
                    'mocked': true
                },
                'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
            },
            'server': {
                'host': '0.0.0.0',
                'port': '3100',
                'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
            },
            'multi': {
                'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                'electronics': {
                    'app': {
                        'api': { 'version': 'v3' },
                        'multiTestParam': { 'attr': '5678' },
                        'tenant': 'electronics',
                        'environment': 'local',
                        'platform': 'shop',
                        'baseSite': 'electronics'
                    },
                    'hybris': {
                        'api': {
                            'schema': '/{base}/{version}/{baseSite}/{url}',
                            'protocol': 'http',
                            'host': 'localhost',
                            'port': '9001',
                            'version': 'v3',
                            'base': 'rest',
                            'retry': { 'times': 1, 'delay': 100 },
                            'mocked': true
                        },
                        'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                    }
                }
            }
        };

        const mockCartController = new MockCartController();
        const mockOrderController = new MockOrderController();
        const mockCMSController = new MockCMSController();
        const mockAuthController = new MockAuthController();
        const mockMessageController = new MockMessageController();
        const mockCountriesController = new MockCountriesController();
        const mockPaymentModesController = new MockPaymentModesController();
        const mockProductController = new MockProductController();

        if (!config.hybris.api.mocked) {
            return;
        }

        function decorateController(request: Request, response: ResponseToolkit, handler: () => any) {
            const headers = HeadersHelper.convert(request.headers);
            const cookies = new Cookies(headers.get('cookie'));
            const path = request.path;
            return runInServerContext({ request, response, cookies, headers, path } as ServerContext, handler);
        }

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/cms/pages`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCMSController.get.bind(mockCMSController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object(defaultSwaggerParams),
                        query: Joi.object({
                            ...defaultSwaggerQuery,
                            pageLabelOrId: Joi.string().example('home-page').required(),
                            fields: Joi.string().allow('full', 'basic', 'default').default('full').required(),
                            pageType: Joi.string().allow('content', 'category').default('content').required()
                        })
                    }
                }
            },
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/previewTicket/{cmsTicketId}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCMSController.preview.bind(mockCMSController)(request, response);
                    });
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/products/search/category/{categoryId}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockProductController.category.bind(mockProductController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object({ ...defaultSwaggerParams, categoryId: Joi.string().example('3699718344').required() }),
                        query: Joi.object({ ...defaultSwaggerQuery, sort: Joi.string().allow('price_asc', 'price_desc').default('').optional() })
                    }
                }
            },
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/products/{productId}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockProductController.get.bind(mockProductController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object({ ...defaultSwaggerParams, productId: Joi.string().example('785300148434').required() }),
                        query: Joi.object({ ...defaultSwaggerQuery, fields: Joi.string().allow('full', 'basic', 'default').default('full').required() })
                    }
                }
            },
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/products/search`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockProductController.search.bind(mockProductController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object(defaultSwaggerParams),
                        query: Joi.object({
                            ...defaultSwaggerQuery,
                            fields: Joi.string().allow('full', 'basic', 'default').default('full').required(),
                            currentPage: Joi.string().default('0').required(),
                            query: Joi.string().required(),
                            sort: Joi.string().optional()
                        })
                    }
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/messages`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockMessageController.get.bind(mockMessageController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object(defaultSwaggerParams),
                        query: Joi.object({
                            ...defaultSwaggerQuery
                            // date: Joi.date(),
                            // number: Joi.number().allow(1, 2),
                            // price: Joi.number().min(10).max(99),
                            // schema: Joi.array()
                            //     .items(Joi.string().required().description('Nested string type in array').default('def').example('fr'))
                            //     .optional()
                        })
                        // payload: Joi.object({
                        //     mode: Joi.string().valid('required', 'optional', 'try').example('example'),
                        //     number: Joi.number().allow(1, 2),
                        //     entity: Joi.string().valid('user', 'app', 'Joi.any').description('description payload entity'),
                        //     payload: [Joi.string().valid('required', 'optional'), Joi.boolean()]
                        // }).meta({ description: 'payload schema', test: 1 })
                    },
                    description: 'Get mocked translations',
                    notes: 'Returns translations from messages.json file'
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/countries`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCountriesController.get.bind(mockCountriesController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object(defaultSwaggerParams),
                        query: Joi.object({
                            ...defaultSwaggerQuery
                        })
                    },
                    description: 'Get mocked countries',
                    notes: 'Returns countries from countries.json file'
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/paymentmodes`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockPaymentModesController.get.bind(mockPaymentModesController)(request, response);
                    });
                },
                options: {
                    tags: ['api'],
                    validate: {
                        params: Joi.object(defaultSwaggerParams),
                        query: Joi.object({
                            ...defaultSwaggerQuery
                        })
                    },
                    description: 'Get mocked payment modes',
                    notes: 'Returns payment modes from paymentmodes.json file'
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.POST,
                path: '/authorizationserver/oauth/token',
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockAuthController.token.bind(mockAuthController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/users/{userId}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockAuthController.loadUser.bind(mockAuthController)(request, response);
                    });
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.GET,
                path: `${this.getHybrisPath()}/users/{userId}/carts/{cartId*}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.getCart.bind(mockCartController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.POST,
                path: `${this.getHybrisPath()}/users/{userId}/carts`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.createCart.bind(mockCartController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.POST,
                path: `${this.getHybrisPath()}/users/{userId}/carts/{cartId}/entries`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.addEntry.bind(mockCartController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.PATCH,
                path: `${this.getHybrisPath()}/users/{userId}/carts/{cartId}/entries/{entryNumber}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.updateEntry.bind(mockCartController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.DELETE,
                path: `${this.getHybrisPath()}/users/{userId}/carts/{cartId}/entries/{entryNumber}`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.deleteEntry.bind(mockCartController)(request, response);
                    });
                }
            },
            {
                method: RouteMethod.POST,
                path: `${this.getHybrisPath()}/users/{userId}/carts/{cartId}/addresses/delivery`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockCartController.createDeliveryAddress.bind(mockCartController)(request, response);
                    });
                }
            }
        ]);

        server.internal.route([
            {
                method: RouteMethod.POST,
                path: `${this.getHybrisPath()}/users/{userId}/orders`,
                handler: (request, response) => {
                    return decorateController(request, response, () => {
                        return mockOrderController.placeOrder.bind(mockOrderController)(request, response);
                    });
                }
            }
        ]);
    }
}
