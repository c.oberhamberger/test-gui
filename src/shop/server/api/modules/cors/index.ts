/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2020 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { BaseModule, type CoreServer } from '@archibald/server';
import type { Boom } from '@hapi/boom';
import type { ResponseObject } from '@hapi/hapi';

function isBoom(response: ResponseObject | Boom): response is Boom {
    return (response as Boom).isBoom;
}

// This can be used during development until backend part is ready
export class CorsModule extends BaseModule {
    constructor() {
        super('CorsModule');
    }

    override async register(server: CoreServer) {
        server.internal.ext('onPreResponse', async (request, h) => {
            if (isBoom(request.response)) {
                return h.continue;
            }
            const getOrigin = (port: string) => `${request.headers['x-forwarded-proto'] || request.server.info.protocol}://${request.info.hostname}:${port}`;

            const config = {
                'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': '' } },
                'app': {
                    'api': {
                        'schema': '/{base}/{version}/{url}',
                        'protocol': null,
                        'host': null,
                        'port': null,
                        'base': 'api',
                        'version': 'v2',
                        'useMiddleware': true,
                        'retry': { 'times': 1, 'delay': 100 }
                    },
                    'logLevel': 'debug',
                    'fixLanguage': true,
                    'languageParamName': 'language',
                    'supportedLanguages': ['de', 'en'],
                    'defaultLanguage': 'de',
                    'canonicalBaseUrl': 'http://127.0.0.1:3100',
                    'route': { 'prefix': '/' },
                    'multiTestParam': { 'attr': '1234' },
                    'tenant': 'netconomy.net',
                    'environment': 'local',
                    'platform': 'shop',
                    'baseSite': 'netconomy.net',
                    'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
                },
                'hybris': {
                    'api': {
                        'schema': '/{base}/{version}/{baseSite}/{url}',
                        'protocol': 'http',
                        'host': 'localhost',
                        'port': '9001',
                        'version': 'v2',
                        'base': 'rest',
                        'retry': { 'times': 1, 'delay': 100 },
                        'mocked': true
                    },
                    'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                },
                'server': {
                    'host': '0.0.0.0',
                    'port': '3100',
                    'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                    'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                    'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
                },
                'multi': {
                    'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                    'electronics': {
                        'app': {
                            'api': { 'version': 'v3' },
                            'multiTestParam': { 'attr': '5678' },
                            'tenant': 'electronics',
                            'environment': 'local',
                            'platform': 'shop',
                            'baseSite': 'electronics'
                        },
                        'hybris': {
                            'api': {
                                'schema': '/{base}/{version}/{baseSite}/{url}',
                                'protocol': 'http',
                                'host': 'localhost',
                                'port': '9001',
                                'version': 'v3',
                                'base': 'rest',
                                'retry': { 'times': 1, 'delay': 100 },
                                'mocked': true
                            },
                            'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                        }
                    }
                }
            };
            const url = request.headers['origin'] ?? getOrigin(config.server.port ?? '3100');

            request.response.header('Access-Control-Allow-Origin', url);
            request.response.header('Access-Control-Allow-Headers', 'content-type');

            return h.continue;
        });
    }
}
