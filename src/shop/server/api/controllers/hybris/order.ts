/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CommonConstants, HttpCode, HttpError, Logger } from '@archibald/core';
import { BaseController, HapiService, Inject } from '@archibald/server';
import Boom from '@hapi/boom';
import type { ResponseToolkit } from '@hapi/hapi';

import { OrderService } from 'shop/server/api/services';

import type { CartRequest } from 'shop/server/interfaces/base';

export class OrderController extends BaseController {
    @Inject()
    private readonly orderService: OrderService;

    @Inject()
    private readonly hapiService: HapiService;

    constructor() {
        super('OrderController');
    }

    protected removeCartId() {
        Logger.info('*** removeCartId erasing cartId cookie ***');
        this.hapiService.setCookie(CommonConstants.CART_ID, null);
    }

    protected handleError(exception: unknown, h: ResponseToolkit, fallbackErrorText = 'Error on processing order') {
        Logger.error(exception);
        if (exception instanceof HttpError) {
            if (exception.statusCode === HttpCode.CODE_404) {
                this.removeCartId();
            }
            throw Boom.boomify(exception, exception);
        }
        throw Boom.badRequest(fallbackErrorText);
    }

    public async placeOrder(request: CartRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'placeOrder', this.name);
        try {
            Logger.info('*** Controller OrderController placeOrder requested ***');
            const placeOrderResponse = await this.orderService.placeOrder();
            return h.response(placeOrderResponse.body);
        } catch (exception) {
            throw this.handleError(exception, h, 'Error while placing an order');
        }
    }
}
