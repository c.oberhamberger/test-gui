/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { CommonConstants, HttpCode, HttpError, Logger } from '@archibald/core';
import { BaseController, HapiService, Inject } from '@archibald/server';
import Boom from '@hapi/boom';
import type { ResponseToolkit } from '@hapi/hapi';

import { CartService } from 'shop/server/api/services';

import type { CartAddressRequest, CartModificationRequest, CartRequest } from 'shop/server/interfaces/base';

export class CartController extends BaseController {
    @Inject()
    private readonly cartService: CartService;

    @Inject()
    private readonly hapiService: HapiService;

    constructor() {
        super('CartController');
    }

    protected removeCartId() {
        Logger.info('*** removeCartId erasing cartId cookie ***');
        this.hapiService.setCookie(CommonConstants.CART_ID, null);
    }

    protected handleError(exception: unknown, fallbackErrorText = 'Error on processing cart') {
        if (exception instanceof HttpError) {
            if (exception.statusCode >= HttpCode.CODE_400) {
                this.removeCartId();
            }
            throw Boom.boomify(exception, { statusCode: exception.statusCode, message: fallbackErrorText });
        }
        throw Boom.badRequest(fallbackErrorText);
    }

    public async getCart(request: CartRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'getCart', this.name);
        try {
            const cart = await this.cartService.getCart();
            return h.response(cart.body);
        } catch (exception) {
            return this.handleError(exception, 'Error on getting cart');
        }
    }

    public async addToCart(request: CartRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'addToCart', this.name);
        try {
            let cartId = await this.cartService.getCartId();
            if (!cartId) {
                const response = await this.cartService.createCart();
                cartId = request.auth.isAuthenticated ? response.body.code : response.body.guid;
            }
            const addToCart = await this.cartService.addToCart(cartId);
            return h.response(addToCart.body);
        } catch (exception) {
            return this.handleError(exception, 'Error on adding cart');
        }
    }

    public async updateCart(request: CartModificationRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'updateCart', this.name);
        try {
            const updateCartResponse = await this.cartService.updateCart();
            return h.response(updateCartResponse.body);
        } catch (exception) {
            return this.handleError(exception, 'Error on updating cart');
        }
    }

    public async deleteFromCart(request: CartModificationRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'deleteFromCart', this.name);
        try {
            const deleteFromCartResponse = await this.cartService.deleteFromCart();
            return h.response(deleteFromCartResponse.body);
        } catch (exception) {
            return this.handleError(exception, 'Error on deleting cart');
        }
    }

    public async createDeliveryAddress(request: CartAddressRequest, h: ResponseToolkit) {
        this.logControllerEntry(request, 'createDeliveryAddress', this.name);
        try {
            const createDeliveryAddressResponse = await this.cartService.createDeliveryAddress();
            return h.response(createDeliveryAddressResponse.body);
        } catch (exception) {
            throw this.handleError(exception, 'Error while creating a cart delivery address');
        }
    }
}
