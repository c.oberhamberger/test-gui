/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger } from '@archibald/core';
import { BaseController, Inject } from '@archibald/server';
import { boomify } from '@hapi/boom';

import { HybrisProxyService } from 'shop/server/api/services';

export class HybrisProxyController extends BaseController {
    @Inject()
    private readonly hybrisProxyService: HybrisProxyService;

    constructor() {
        super('HybrisProxyController');
    }

    async proxy() {
        try {
            return await this.hybrisProxyService.proxyHandler(`${this.name}.proxy`);
        } catch (exception) {
            if (exception instanceof Error) {
                Logger.error(exception.message);
                return boomify(exception, exception);
            }
            throw exception;
        }
    }
}
