/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, DefaultMemWatchActivation } from '@archibald/core';
import { BaseController } from '@archibald/server';
import type { Request, ResponseToolkit } from '@hapi/hapi';

export class BackendController extends BaseController {
    constructor() {
        super('BackendController');
    }

    async alive(request: Request, h: ResponseToolkit) {
        this.logControllerEntry(request, 'alive', this.name);
        return h.response('is up and running');
    }

    async version(request: Request, h: ResponseToolkit) {
        this.logControllerEntry(request, 'version', this.name);
        const config = {
            'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': '' } },
            'app': {
                'api': {
                    'schema': '/{base}/{version}/{url}',
                    'protocol': null,
                    'host': null,
                    'port': null,
                    'base': 'api',
                    'version': 'v2',
                    'useMiddleware': true,
                    'retry': { 'times': 1, 'delay': 100 }
                },
                'logLevel': 'debug',
                'fixLanguage': true,
                'languageParamName': 'language',
                'supportedLanguages': ['de', 'en'],
                'defaultLanguage': 'de',
                'canonicalBaseUrl': 'http://127.0.0.1:3100',
                'route': { 'prefix': '/' },
                'multiTestParam': { 'attr': '1234' },
                'tenant': 'netconomy.net',
                'environment': 'local',
                'platform': 'shop',
                'baseSite': 'netconomy.net',
                'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
            },
            'hybris': {
                'api': {
                    'schema': '/{base}/{version}/{baseSite}/{url}',
                    'protocol': 'http',
                    'host': 'localhost',
                    'port': '9001',
                    'version': 'v2',
                    'base': 'rest',
                    'retry': { 'times': 1, 'delay': 100 },
                    'mocked': true
                },
                'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
            },
            'server': {
                'host': '0.0.0.0',
                'port': '3100',
                'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
            },
            'multi': {
                'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                'electronics': {
                    'app': {
                        'api': { 'version': 'v3' },
                        'multiTestParam': { 'attr': '5678' },
                        'tenant': 'electronics',
                        'environment': 'local',
                        'platform': 'shop',
                        'baseSite': 'electronics'
                    },
                    'hybris': {
                        'api': {
                            'schema': '/{base}/{version}/{baseSite}/{url}',
                            'protocol': 'http',
                            'host': 'localhost',
                            'port': '9001',
                            'version': 'v3',
                            'base': 'rest',
                            'retry': { 'times': 1, 'delay': 100 },
                            'mocked': true
                        },
                        'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                    }
                }
            }
        };
        return h.response(config.server.git);
    }

    async logger(request: Request, h: ResponseToolkit) {
        this.logControllerEntry(request, 'logger', this.name);
        const level = request.params.level || 'error';
        const newLevel = Logger.setLogLevel(level);
        const msg = `log level set to ${newLevel}`;
        // test the logger
        Logger.warn(msg);
        return h.response(msg);
    }

    async memwatch(request: Request, h: ResponseToolkit) {
        const reqPrint = request.params.print;
        const config = {
            'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': true } },
            'app': {
                'api': {
                    'schema': '/{base}/{version}/{url}',
                    'protocol': null,
                    'host': null,
                    'port': null,
                    'base': 'api',
                    'version': 'v2',
                    'useMiddleware': true,
                    'retry': { 'times': 1, 'delay': 100 }
                },
                'logLevel': 'debug',
                'fixLanguage': true,
                'languageParamName': 'language',
                'supportedLanguages': ['de', 'en'],
                'defaultLanguage': 'de',
                'canonicalBaseUrl': 'http://127.0.0.1:3100',
                'route': { 'prefix': '/' },
                'multiTestParam': { 'attr': '1234' },
                'tenant': 'netconomy.net',
                'environment': 'local',
                'platform': 'shop',
                'baseSite': 'netconomy.net',
                'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
            },
            'hybris': {
                'api': {
                    'schema': '/{base}/{version}/{baseSite}/{url}',
                    'protocol': 'http',
                    'host': 'localhost',
                    'port': '9001',
                    'version': 'v2',
                    'base': 'rest',
                    'retry': { 'times': 1, 'delay': 100 },
                    'mocked': true
                },
                'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
            },
            'server': {
                'host': '0.0.0.0',
                'port': '3100',
                'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
            },
            'multi': {
                'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                'electronics': {
                    'app': {
                        'api': { 'version': 'v3' },
                        'multiTestParam': { 'attr': '5678' },
                        'tenant': 'electronics',
                        'environment': 'local',
                        'platform': 'shop',
                        'baseSite': 'electronics'
                    },
                    'hybris': {
                        'api': {
                            'schema': '/{base}/{version}/{baseSite}/{url}',
                            'protocol': 'http',
                            'host': 'localhost',
                            'port': '9001',
                            'version': 'v3',
                            'base': 'rest',
                            'retry': { 'times': 1, 'delay': 100 },
                            'mocked': true
                        },
                        'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                    }
                }
            }
        };
        // note as we access the global runtime config, we change it here; so its used as kind of state
        // of the current instance
        // but as the use case is very simple here, this should be OK
        const configMemwatch = config.general.memwatch;

        if (reqPrint === 'check') {
            return h.response(
                `memwatch.active=${configMemwatch.active}, memwatch.print=${configMemwatch.print}` +
                    `, process.memoryUsage()=${JSON.stringify(process.memoryUsage())}`
            );
        }

        try {
            const memwatchActive = configMemwatch.active;
            const print = reqPrint !== '0';
            // memwatchActive see MemWatchActivation.*
            const proceedWithActivation = print && memwatchActive !== DefaultMemWatchActivation.ACTIVE;
            if (proceedWithActivation) {
                // activate memwatch hook
                configMemwatch.active = DefaultMemWatchActivation.FAILED; // initialize with "2: failed"

                Logger.info(`Trying to activate memwatch hook. Current directory: ${process.cwd()}`);
                // note that for distribution build to work,
                // modules "@airbnb/node-memwatch" and "bindings" need to be put in nodejs search path
                // in our use case for stage/Q/P best would be /home/retail/nodejs/node_modules
                const memwatch = __non_webpack_require__('@airbnb/node-memwatch') as any;
                memwatch.on('stats', (stats: any) => {
                    const memwatchPrint = config.general.memwatch.print;
                    if (memwatchPrint) {
                        Logger.info(
                            `GC stats: heap size:${(stats.total_heap_size / 1000000).toFixed(1)}M, ` +
                                `exec.:${(stats.total_heap_size_executable / 1000000).toFixed(1)}M, ` +
                                `gc time:${(stats.gc_time / 1000000).toFixed(0)}ms, ` +
                                `stats:${JSON.stringify(stats)}, ` +
                                `process.memoryUsage(): ${JSON.stringify(process.memoryUsage())}`
                        );
                    }
                });
                Logger.info('Activation of memwatch seems to be OK.');
                configMemwatch.active = DefaultMemWatchActivation.ACTIVE; // OK
            }

            let msg;
            if (configMemwatch.active) {
                configMemwatch.print = print;
                msg = `memwatch.print set to ${print}`;
            } else {
                msg = 'memwatch not active';
            }
            return h.response(msg);
        } catch (error) {
            Logger.error(`Memwatch error: ${JSON.stringify(error)}`);
            throw error;
        }
    }
}
