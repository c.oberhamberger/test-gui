/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, HttpCode, HttpHeader, HttpMimeType } from '@archibald/core';
import { BaseController, Inject } from '@archibald/server';
import type { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';

import NotFoundError from 'shop/server/errors/not-found';
import RedirectError from 'shop/server/errors/redirect';
import ServerError from 'shop/server/errors/server';

import { RenderService } from 'shop/server/api/services';

export class RenderController extends BaseController {
    @Inject()
    private readonly renderService: RenderService;

    constructor() {
        super('RenderController');
    }

    async index(request: Request, h: ResponseToolkit): Promise<ResponseObject> {
        this.logControllerEntry(request, 'index', this.name);

        const { pathname, search, href } = request.url;
        const path = `${pathname}${search}`;
        const hostname = request?.info.hostname;

        Logger.info(`${this.name}: route: host ${hostname}, path ${href}`);

        Logger.debug(`${this.name}: route: initializing server side rendering of ${href}`);
        try {
            const markup = await this.renderService.render({ url: path, request, response: h });
            return this.convertResponse(h, markup);
        } catch (error) {
            if (error instanceof ServerError) {
                // render fatal error component on ssr
                Logger.debug(`hapi rendering: internal server error ${error.url}, serving fatal error component`);
                const serverErrorPage = await this.renderService.renderServerErrorPage({ url: path, request, response: h });
                return this.convertResponse(h, serverErrorPage.markup);
            } else if (error instanceof RedirectError) {
                Logger.info(`hapi rendering: redirect to ${error.url}`);
                const responseRedirect = h.redirect(error.url);
                return responseRedirect.code(error.statusCode);
            } else if (error instanceof NotFoundError) {
                Logger.info(`hapi rendering: 404 page for ${request.path}`);
                const responseNotFound = h.response((error as NotFoundError).markup);
                return responseNotFound.code(HttpCode.CODE_404);
            }
            Logger.error(`hapi rendering: error: ${(error as Error).stack}`);
            const responseServerError = h.response('Unexpected condition was encountered');
            // this is usually JS error
            return responseServerError.code(HttpCode.CODE_500);
        }
    }

    private convertResponse(h: ResponseToolkit, markup: string) {
        const response = h.response(markup);
        response.header(HttpHeader.CONTENT_TYPE, HttpMimeType.TEXT_HTML);
        return response;
    }
}
