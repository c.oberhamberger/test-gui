/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ANONYMOUS_USER_ID, CURRENT_ID, AuthService } from '@archibald/auth';
import { Logger, DefaultResponse, ParamsHelper, CommonConstants } from '@archibald/core';
import { BaseService, getRequest, HapiService, Inject, Service } from '@archibald/server';

import { HybrisHttpService, ErrorService, HybrisService } from 'shop/server/api/services';

import type { Cart, CartModification, Error } from 'shop/client/interfaces/base';
import type { User, CartModificationRequest, CartRequest, CartAddressRequest } from 'shop/server/interfaces/base';

const EMPTY_CART_RESPONSE = { body: {} } as DefaultResponse<Cart>;

@Service()
export class CartService extends BaseService {
    @Inject()
    private readonly authService: AuthService<User>;

    @Inject()
    private readonly hapiService: HapiService;

    @Inject()
    private readonly hybrisHttpService: HybrisHttpService;

    @Inject()
    private readonly hybrisService: HybrisService;

    @Inject()
    private readonly errorService: ErrorService;

    constructor() {
        super('CartService');
    }

    public async getCartId() {
        const request = getRequest<CartRequest>();
        if (request.params.cartId === CURRENT_ID) {
            return this.hapiService.getCookie(CommonConstants.CART_ID);
        }
        return request.params.cartId ?? null;
    }

    public async getUserId() {
        const request = getRequest<CartRequest>();
        if (request.params.userId === CURRENT_ID) {
            return this.authService.getUserId();
        }
        return request.params.cartId ?? ANONYMOUS_USER_ID;
    }

    protected getBaseCartUrl(userId: string, cartId: string | null) {
        if (!cartId) {
            return `users/${userId}/carts`;
        }

        return `users/${userId}/carts/${cartId}`;
    }

    private constructErrorResponse<T>(response: DefaultResponse<T>, errors: Error[]): DefaultResponse<T> {
        if (!response.ok) {
            return {
                ...response,
                body: {
                    ...response.body,
                    errors: this.errorService.mergeErrors(response.body.errors as Error[], errors)
                }
            };
        }
        return response;
    }

    /**
     * Get cart with existing id.
     */
    public async getCart(): Promise<DefaultResponse<Cart>> {
        const request = getRequest<CartRequest>();
        const cartId = await this.getCartId();

        if (!cartId) {
            return EMPTY_CART_RESPONSE;
        }

        const userId = await this.getUserId();
        const baseCartUrl = this.getBaseCartUrl(userId, cartId);
        const hybris = await this.hybrisService.mapUrlToHybris(baseCartUrl, request, { fields: 'FULL' });

        Logger.info(`*** actionGetCart cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(`*** actionGetCart is getting a cart by GET ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);
        }

        const response = await this.hybrisHttpService.get<Cart>(hybris.url, hybris);
        Logger.debug('*** actionGetCart got a cart ***');

        return response;
    }

    public async createCart() {
        const request = getRequest<CartRequest>();
        const userId = await this.getUserId();
        const baseCartUrl = this.getBaseCartUrl(userId, null);
        const hybris = await this.hybrisService.mapUrlToHybris(baseCartUrl, request);

        Logger.info(`*** createCart userId=${userId}, ${request.method.toUpperCase()} ***`);
        Logger.debug(() => `*** createCart requesting new cart id by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);

        const response = await this.hybrisHttpService.post<Cart>(hybris.url, {}, hybris);
        return this.finishRequest(hybris.url, response, []);
    }

    public async addToCart(cartId: string, errors: Error[] = []) {
        const request = getRequest<CartRequest>();
        const userId = await this.getUserId();
        const baseCartUrl = this.getBaseCartUrl(userId, cartId);
        const url = ParamsHelper.create(`${baseCartUrl}/entries`, { fields: 'DEFAULT' });

        const hybris = await this.hybrisService.mapUrlToHybris(url, request);

        Logger.info(`*** addToCart cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(`*** addToCart is adding entry to a cart by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);
        }

        const response = await this.hybrisHttpService.post<CartModification>(hybris.url, request.payload, hybris);
        return this.finishRequest(url, response, errors);
    }

    public async updateCart(errors: Error[] = []) {
        const request = getRequest<CartModificationRequest>();
        const userId = await this.getUserId();
        const cartId = await this.getCartId();
        const entryNumber = request.params.entryNumber;
        const hybris = await this.hybrisService.mapUrlToHybris(`${this.getBaseCartUrl(userId, cartId)}/entries/${entryNumber}`, request);

        Logger.info(`*** updateCart cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(`*** updateCart is changing entry #${entryNumber} by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);
        }

        const response = await this.hybrisHttpService.patch<CartModification>(hybris.url, request.payload, hybris);
        return this.finishRequest(hybris.url, response, errors);
    }

    public async deleteFromCart(errors: Error[] = []) {
        const request = getRequest<CartModificationRequest>();
        const userId = await this.getUserId();
        const cartId = await this.getCartId();
        const entryNumber = request.params.entryNumber;
        const hybris = await this.hybrisService.mapUrlToHybris(`${this.getBaseCartUrl(userId, cartId)}/entries/${entryNumber}`, request);

        Logger.info(`*** removeFromCart cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(`*** removeFromCart is changing entry #${entryNumber} by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);
        }

        const response = await this.hybrisHttpService.delete<CartModification>(hybris.url, { headers: hybris.headers, throwOnError: false });
        return this.finishRequest(hybris.url, response, errors);
    }

    public async createDeliveryAddress(errors: Error[] = []) {
        const request = getRequest<CartAddressRequest>();
        const userId = await this.getUserId();
        const cartId = await this.getCartId();
        const hybris = await this.hybrisService.mapUrlToHybris(`${this.getBaseCartUrl(userId, cartId)}/addresses/delivery`, request);

        Logger.info(`*** createDeliveryAddress cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(
                `*** createDeliveryAddress is adding a delivery address to a cart by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`
            );
        }

        const response = await this.hybrisHttpService.post<CartModification>(hybris.url, request.payload, { headers: hybris.headers });
        return await this.finishRequest(hybris.url, response, errors);
    }

    private async finishRequest<T>(url: string, response: DefaultResponse<T>, errors: Error[] = []) {
        const errorResponse = this.constructErrorResponse(response, errors);

        if (this.errorService.hasErrors(errorResponse)) {
            this.errorService.logErrors(response, url);
            return errorResponse;
        }

        Logger.debug(`Calling (${url}) was successfully finished`);

        return response;
    }
}
