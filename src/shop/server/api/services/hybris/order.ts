/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2024 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ANONYMOUS_USER_ID, CURRENT_ID, AuthService } from '@archibald/auth';
import { Logger, DefaultResponse, CommonConstants } from '@archibald/core';
import { BaseService, getRequest, HapiService, Inject, Service } from '@archibald/server';

import { HybrisHttpService, ErrorService, HybrisService } from 'shop/server/api/services';

import type { CartModification, Error } from 'shop/client/interfaces/base';
import type { User, OrderRequest } from 'shop/server/interfaces/base';

@Service()
export class OrderService extends BaseService {
    @Inject()
    private readonly authService: AuthService<User>;

    @Inject()
    private readonly hapiService: HapiService;

    @Inject()
    private readonly hybrisHttpService: HybrisHttpService;

    @Inject()
    private readonly hybrisService: HybrisService;

    @Inject()
    private readonly errorService: ErrorService;

    constructor() {
        super('OrderService');
    }

    public async getCartId() {
        const request = getRequest<OrderRequest>();
        if (request.params.cartId === CURRENT_ID) {
            return this.hapiService.getCookie(CommonConstants.CART_ID);
        }
        return request.query.cartId;
    }

    public async getUserId() {
        const request = getRequest<OrderRequest>();
        if (request.params.userId === CURRENT_ID) {
            return this.authService.getUserId();
        }
        return request.params.userId ?? ANONYMOUS_USER_ID;
    }

    protected getBaseOrderUrl(userId: string) {
        return `users/${userId}/orders`;
    }

    private constructErrorResponse<T>(response: DefaultResponse<T>, errors: Error[]): DefaultResponse<T> {
        if (!response.ok) {
            return {
                ...response,
                body: {
                    ...response.body,
                    errors: this.errorService.mergeErrors(response.body.errors as Error[], errors)
                }
            };
        }
        return response;
    }

    public async placeOrder(errors: Error[] = []) {
        const request = getRequest<OrderRequest>();
        const userId = await this.getUserId();
        const cartId = await this.getCartId();
        const hybris = await this.hybrisService.mapUrlToHybris(`${this.getBaseOrderUrl(userId)}/addresses/delivery`, request, { cartId: cartId });

        Logger.info(`*** placeOrder cartId=${cartId}, userId=${userId}, ${request.method.toUpperCase()} ***`);
        if (Logger.isDebugEnabled()) {
            Logger.debug(`*** placeOrder is placing an order by POST ${hybris.url}, headers=${JSON.stringify(hybris.headers)} ***`);
        }

        const response = await this.hybrisHttpService.post<CartModification>(hybris.url, request.payload, { headers: hybris.headers });
        return await this.finishRequest(hybris.url, response, errors);
    }

    private async finishRequest<T>(url: string, response: DefaultResponse<T>, errors: Error[] = []) {
        const errorResponse = this.constructErrorResponse(response, errors);

        if (this.errorService.hasErrors(errorResponse)) {
            this.errorService.logErrors(response, url);
            return errorResponse;
        }

        Logger.debug(`Calling (${url}) was successfully finished`);

        return response;
    }
}
