/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { ApiServiceToken, AuthService } from '@archibald/auth';
import { HttpMethod, type RequestOptions } from '@archibald/core';
import { BaseService, HttpService, Inject, Service } from '@archibald/server';

import type { User } from 'shop/client/interfaces/base';

interface AuthOptions<Payload = unknown> extends RequestOptions<Payload> {
    userId?: string;
}

@Service()
export class HybrisHttpService extends BaseService {
    @Inject()
    private readonly httpService: HttpService;

    @Inject()
    private readonly authService: AuthService<User>;

    constructor() {
        super('HybrisHttpService');
    }

    public async request<Response = unknown, Payload = unknown>(method: HttpMethod, url: string, options: AuthOptions<Payload>) {
        const claim = (await this.authService.getClaimFromToken<ApiServiceToken>('hybris')) ?? (await this.authService.getStaticClaim('hybris-static'));
        const authHeaders = await this.authService.getAuthHeaders(claim, options.headers);
        return this.httpService.request<Response, Payload>(method, url, { ...options, headers: authHeaders });
    }

    public async get<Response = unknown, Payload = unknown>(url: string, options?: AuthOptions<Payload>) {
        return this.request<Response, Payload>(HttpMethod.GET, url, { ...options });
    }

    public async post<Response = unknown, Payload = unknown>(url: string, body: Payload, options?: AuthOptions) {
        return this.request<Response, Payload>(HttpMethod.POST, url, { ...options, body });
    }

    public async put<Response = unknown, Payload = unknown>(url: string, body: Payload, options?: AuthOptions) {
        return this.request<Response, Payload>(HttpMethod.PUT, url, { ...options, body });
    }

    public async delete<Response = unknown>(url: string, options?: AuthOptions) {
        return this.request<Response>(HttpMethod.DELETE, url, { ...options });
    }

    public async patch<Response = unknown, Payload = unknown>(url: string, body: Payload, options?: AuthOptions) {
        return this.request<Response, Payload>(HttpMethod.PATCH, url, { ...options, body });
    }
}
