/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type DefaultResponse, HttpCode, HttpHeader, HttpMethod, Logger } from '@archibald/core';
import { BaseService, getHeaders, getRequest, getResponse, Inject, Service } from '@archibald/server';
import { Boom } from '@hapi/boom';

import { HybrisHttpService, HybrisService } from 'shop/server/api/services';

@Service()
export class HybrisProxyService extends BaseService {
    @Inject()
    private readonly hybrisHttpService: HybrisHttpService;

    @Inject()
    private readonly hybrisService: HybrisService;

    constructor() {
        super('HybrisProxyService');
    }

    /**
     * Log some info about entry in a controller method.
     * @param {string} handlerName Optional method name.
     */
    private static logHandlerEntry(handlerName: string) {
        const request = getRequest();
        // "raw.req.url" is incl. query string
        Logger.debug(`### ${handlerName}: ${request.method.toUpperCase()} ${request.raw.req.url}`);
    }

    /**
     * This method is used to see if the forwarded request was already proxied and then bailout to prevent a loop
     * @param {Headers} requestHeaders
     * @param {Headers} responseHeaders
     * @return {boolean} proxied header was set
     */
    private checkProxyLoop(requestHeaders: Headers, responseHeaders: Headers) {
        if (requestHeaders.get(HttpHeader.X_PROXIED) === 'true') {
            throw new Boom('Redirect Loop Detected', { statusCode: HttpCode.CODE_500 });
        } else {
            responseHeaders.set(HttpHeader.X_PROXIED, 'true');
        }
    }

    /**
     * Delete few sensitive headers we got from hybris and don't want to forward it further.
     * Old code name: cleanupHybrisResponseHeaders().
     *
     * @param {ResponseToolkit} h.
     * @param {DefaultResponse} hybrisResponse Current response.
     *
     * @return Updated response.
     */
    public forwardWhitelistedResponseHeaders(hybrisResponse: DefaultResponse<any>) {
        const h = getResponse();
        // leave out cookies set in hybris, as we manage cookies on nodejs server level
        // leave out "content-length" as it may not be the same as content-length of payload from nodejs server
        // as in nodejs server layer we sometimes adjust the content
        const response = h.response(hybrisResponse?.body);
        const setCookieHeader = String(HttpHeader.SET_COOKIE).toLowerCase();
        const contentLengthHeader = String(HttpHeader.CONTENT_LENGTH).toLowerCase();
        const headers = response.headers;
        if (!headers) {
            return response;
        }
        for (const name in headers) {
            if (headers.hasOwnProperty(name) && name !== setCookieHeader && name !== contentLengthHeader) {
                const value: string = headers[name] as string;
                response.header(name, value);
            }
        }
        if (!hybrisResponse.ok) {
            return new Boom(hybrisResponse.statusText, {
                statusCode: hybrisResponse.status,
                data: hybrisResponse.body,
                message: hybrisResponse.statusText
            });
        }
        return response;
    }

    /**
     * This is a simple 1:1 http request proxied to Hybris with filtered headers and without authorization.
     * Adds filtering of response headers. All requests which are sent to hybris and returned 1:1 as response should use this method.
     *
     * @param handler Controller name
     */
    public async proxyHandler(handler: string) {
        HybrisProxyService.logHandlerEntry(handler);
        const request = getRequest();
        const requestHeaders = getHeaders();

        const { path, method } = request;
        const { url, headers } = await this.hybrisService.mapUrlToHybris(path, request);
        this.checkProxyLoop(requestHeaders, headers);

        Logger.debug(`${handler} is proxying ${path} to ${String(method).toUpperCase()} ${url}`);

        const response = await this.hybrisHttpService.request(method as HttpMethod, url, { body: request.payload, headers });

        return this.forwardWhitelistedResponseHeaders(response);
    }
}
