/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2018 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { HeadManager } from '@archibald/client';
import {
    Logger,
    selectLanguage,
    type DefaultStore,
    LoadingType,
    selectLastHttpStatus,
    HttpCode,
    ParamsHelper,
    type RouterContextInterface,
    StaticRouter,
    AppClient
} from '@archibald/core';
import { BaseService, Renderer, ServerConfigHelper, RenderHelper, Service, ServerProvider } from '@archibald/server';
import type { Request, ResponseToolkit } from '@hapi/hapi';

import NotFoundError from 'shop/server/errors/not-found';
import RedirectError from 'shop/server/errors/redirect';
import ServerError from 'shop/server/errors/server';

import DataClient from 'shop/client/api/creators/data';

import App from 'shop/client/components/App';

import type { CustomState } from 'shop/client/reducers';

import StoreHelper from 'shop/client/stores/store';

interface RenderInput {
    url: string | undefined;
    request: Request;
    response: ResponseToolkit;
}

@Service()
export class RenderService extends BaseService {
    private readonly renderHelper = new RenderHelper({ loading: LoadingType.ASYNC, critical: { css: true, modules: true } });

    constructor() {
        super('RenderService');
    }

    private async getMarkup(markup: string, renderer: Renderer, store: DefaultStore<CustomState>, headManager = new HeadManager(), nonInteractive = false) {
        Logger.info('Rendering: markup started');
        const state = store.getState();
        const language = selectLanguage(state);
        const {
            assets: { excluded, rendered },
            contexts: { dataClient, hydrate }
        } = renderer.getContext();

        const tags = headManager.render();
        const optimizedState = ServerConfigHelper.prepareState({
            state,
            config: {
                'app': {
                    'api': {
                        'schema': '/{base}/{version}/{url}',
                        'protocol': null,
                        'host': null,
                        'port': null,
                        'base': 'api',
                        'version': 'v2',
                        'useMiddleware': true,
                        'retry': { 'times': 1, 'delay': 100 }
                    },
                    'logLevel': 'debug',
                    'fixLanguage': true,
                    'languageParamName': 'language',
                    'supportedLanguages': ['de', 'en'],
                    'defaultLanguage': 'de',
                    'canonicalBaseUrl': 'http://127.0.0.1:3100',
                    'route': { 'prefix': '/' },
                    'multiTestParam': { 'attr': '1234' },
                    'tenant': 'netconomy.net',
                    'environment': 'local',
                    'platform': 'shop',
                    'baseSite': 'netconomy.net',
                    'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
                }
            },
            dataClient
        });
        return this.renderHelper.getHTML(
            {
                page: { tags, language },
                markup,
                hydrate,
                state: optimizedState
            },
            { modules: rendered, excluded: { js: excluded }, nonInteractive }
        );
    }

    private static isNotFoundPage(state: CustomState) {
        const error = selectLastHttpStatus(state);
        return error === HttpCode.CODE_404;
    }

    private static isServerError(state: CustomState) {
        const error = selectLastHttpStatus(state);
        return error >= HttpCode.CODE_400 && error !== HttpCode.CODE_404;
    }

    public async renderServerErrorPage({ request, response }: RenderInput) {
        const manager = new HeadManager();
        const appClient = new AppClient({ request });
        const store = StoreHelper.getStore();
        const renderer = new Renderer({ dataClient: DataClient, store });
        const context: Partial<RouterContextInterface> = {};
        const initialLocation = `${request.url.pathname}${ParamsHelper.filterSearch(request.url.search)}`;
        const fatalErrorComponent = (
            <ServerProvider store={store} server={{ request, response }} head={manager} app={appClient}>
                <StaticRouter location={initialLocation} context={context}>
                    <App />
                </StaticRouter>
            </ServerProvider>
        );
        const markup = await renderer.render(fatalErrorComponent);
        const pageMarkup = await this.getMarkup(markup, renderer, store, manager);
        const state = store.getState();
        Logger.debug('Rendering: FatalError Component');
        return { markup: pageMarkup, state };
    }

    public async render({ request, response }: RenderInput) {
        const manager = new HeadManager();
        const appClient = new AppClient({ request });

        const store = StoreHelper.getStore();
        const renderer = new Renderer({ dataClient: DataClient, store, topLevelSuspense: false });
        const context: Partial<RouterContextInterface> = {};

        const initialLocation = `${request.url.pathname}${ParamsHelper.filterSearch(request.url.search)}`;
        const app = (
            <ServerProvider server={{ request, response }} store={store} head={manager} app={appClient}>
                <StaticRouter location={initialLocation} context={context}>
                    <App />
                </StaticRouter>
            </ServerProvider>
        );

        Logger.info('Render: started');
        const markup = await renderer.render(app);
        Logger.info('Render: finished');

        if (context.url) {
            Logger.debug(`Rendering: redirect url: ${context.url} status: ${context.statusCode}`);
            throw new RedirectError(context.url, context.statusCode);
        }

        const pageMarkup = await this.getMarkup(markup, renderer, store, manager);
        const state = store.getState();

        if (RenderService.isNotFoundPage(state)) {
            Logger.debug('Rendering NotFound page');
            throw new NotFoundError('Notfound', pageMarkup, initialLocation);
        }

        if (RenderService.isServerError(state)) {
            Logger.debug('Rendering Server Error page');
            throw new ServerError('Server Error', pageMarkup, initialLocation);
        }

        Logger.info('Rendering: markup finished');
        return pageMarkup;
    }
}

export default RenderService;
