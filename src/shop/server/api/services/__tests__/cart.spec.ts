/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2023 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { BaseService, ConfigService, Container, HapiService, Service } from '@archibald/server';
import { runInServerContext, ServerContext } from '@archibald/storage';
import { registerTestBed } from '@archibald/testing';

import { CartService, ErrorService, HybrisHttpService, HybrisService, ProjectConfigService } from 'shop/server/api/services';

import { CartRequest } from 'shop/server/interfaces/base';

@Service()
class HybrisServiceMock extends BaseService {
    mapUrlToHybris(url, _request, options) {
        return { headers: {}, url: url || '', ...options };
    }

    getUserId(_request) {
        return 'anonymous';
    }
}

@Service()
class HybrisHttpServiceMock extends BaseService {
    get(response) {
        return Promise.resolve(response);
    }

    post(response) {
        return Promise.resolve(response);
    }

    getUserId(request) {
        return request.state.ncu;
    }
}

describe('Cart Service', () => {
    beforeAll(() => {
        registerTestBed({
            services: [
                ErrorService,
                HapiService,
                {
                    provide: HybrisHttpService,
                    value: HybrisHttpServiceMock
                },
                {
                    provide: ConfigService,
                    value: ProjectConfigService
                },
                {
                    provide: HybrisService,
                    value: HybrisServiceMock
                }
            ]
        });
    });

    const defaultCartRequest = {
        method: 'GET',
        query: {},
        params: { version: 'v1' },
        payload: {
            code: 'cart-code',
            entry: { product: { code: 'code' } },
            softRemoval: false,
            email: 'test@test.com',
            deliveryModeId: 'delivery-mode-id',
            user: {}
        },
        state: {
            ncc: '67wx7-xhvjw78-qx87q'
        }
    } as unknown as CartRequest;

    test('should return empty response if no cart Id is provided', async () => {
        // Will be made nicer in the future
        const request = { ...defaultCartRequest, query: {}, state: {} } as CartRequest;
        const cart = await runInServerContext({ request } as unknown as ServerContext, async () => {
            return new CartService().getCart();
        });

        expect(cart).toMatchObject({ body: {} });
    });

    test('should return empty response when cart is not found', async () => {
        // Will be made nicer in the future
        const cart = await runInServerContext({ request: defaultCartRequest } as unknown as ServerContext, async () => {
            return new CartService().getCart();
        });

        expect(cart).toMatchObject({ body: {} });
    });

    afterAll(() => {
        Container.reset();
    });
});
