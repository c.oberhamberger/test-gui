/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, HttpMethod } from '@archibald/core';
import { BaseService, Service } from '@archibald/server';
import type { Request, ResponseObject } from '@hapi/hapi';

import Prometheus, { LabelValues, Summary } from 'prom-client';

// We want access to the default metrics provided by the library with a
// resampling every 5 seconds:
Prometheus.collectDefaultMetrics({
    eventLoopMonitoringPrecision: 5000
});

enum ObserveType {
    REQUEST = 'request',
    HYBRIS = 'hybris'
}

interface Data {
    status?: number;
    method: HttpMethod;
    path: string;
}

@Service()
export class PrometheusService extends BaseService {
    private readonly requestTimeMetric: Summary<string>;
    private readonly requestSAPTimeMetric: Summary<string>;

    constructor() {
        super('PrometheusService');

        Prometheus.register.clear();

        this.requestTimeMetric = new Prometheus.Summary({
            name: 'http_request_duration_ms',
            help: 'Summary of request times (duration) per endpoint'
        });

        this.requestSAPTimeMetric = new Prometheus.Summary({
            name: 'http_request_sap_duration_ms',
            help: 'Summary of SAP request times (duration) per endpoint'
        });
    }

    private isRequest = (request): request is Partial<Request> => {
        return 'response' in request && 'route' in request;
    };

    /**
     * Prometheus Observe
     */
    public async observe(request: Data | Partial<Request>, requestTime: number, type: ObserveType = ObserveType.REQUEST) {
        let data: LabelValues<string>;
        if (this.isRequest(request)) {
            data = {
                status: (request.response as ResponseObject).statusCode,
                method: request.route?.method ?? HttpMethod.GET,
                path: request.route?.path ?? ''
            };
        } else {
            data = {
                status: request.status,
                method: request.method,
                path: request.path
            };
        }

        switch (type) {
            case ObserveType.REQUEST:
                this.requestTimeMetric.observe(data, requestTime);
                break;
            case ObserveType.HYBRIS:
                this.requestSAPTimeMetric.observe(data, requestTime);
                break;
            default:
                Logger.trace('This shouldn`t happen');
        }
    }

    public async metrics() {
        return Prometheus.register.metrics();
    }
}
