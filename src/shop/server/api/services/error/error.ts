/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { Logger, type DefaultResponse, HttpCode, uniqSimpleArray } from '@archibald/core';
import { BaseService, Service } from '@archibald/server';

import { ErrorType } from 'shop/client/constants';

import type { Error } from 'shop/client/interfaces/base';

@Service()
export class ErrorService extends BaseService {
    constructor() {
        super('ErrorService');
    }

    public hasErrors<T>(response: DefaultResponse<T>): boolean {
        if (!response.ok) {
            return true;
        }
        return this.getAllErrors(response).some((error) => error.type !== ErrorType.INFO);
    }

    public getAllErrors(response: DefaultResponse<any>): Error[] {
        return response?.body?.errors ?? [];
    }

    public mergeErrors(errors?: Error[], ...restErrors: Error[][]): Error[] {
        return uniqSimpleArray([...(errors ?? []), ...restErrors.flat()]);
    }

    public logErrors<T>(response: DefaultResponse<T>, action: string, info?: string): DefaultResponse<T> {
        const statusCode = response?.status ?? HttpCode.CODE_400;

        const errors = this.getAllErrors(response).map((error: Error) => {
            if (statusCode !== HttpCode.CODE_200) {
                const additionalInfo = info ? `| ${info} ` : '';
                Logger.error(`*** ${action} failed with code ${statusCode} and errors ${JSON.stringify(error)} ${additionalInfo}***`);
            }

            if (error.type === ErrorType.TOKEN_ERROR) {
                return { ...error, recoverable: true };
            }

            return error;
        });

        return {
            ...response,
            body: {
                ...response.body,
                errors
            }
        };
    }
}
