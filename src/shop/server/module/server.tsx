/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { AuthModule, HybrisUserAuthProvider, HybrisStaticAuthProvider } from '@archibald/auth';
import { Logger } from '@archibald/core';
import { CoreServer, DefaultPlugins, TokenService, SwaggerPlugin } from '@archibald/server';
import { preloadAll } from '@archibald/storefront';

import {
    BackendController,
    CartController,
    OrderController,
    DebugController,
    HybrisProxyController,
    RenderController,
    ResourceController
} from 'shop/server/api/controllers';
import { CorsModule, MockModule, PrometheusModule } from 'shop/server/api/modules';
import {
    CartService,
    OrderService,
    ErrorService,
    RenderService,
    TranslationService,
    HybrisProxyService,
    HybrisHttpService,
    HybrisService
} from 'shop/server/api/services';

import ServerRoutes from 'shop/server/routes';

export class Server extends CoreServer {

    constructor() {
        super();
        this.logCreateServer();
    }

    protected async initServer() {
        await this.createServer();
        await preloadAll();
    }

    public async initPlugins() {
        this.logSeparator();
        await this.registerPlugins([...DefaultPlugins, ...SwaggerPlugin]);
    }

    public async initModules() {
        this.logSeparator();
        const { hybris, server } = {
            'hybris': {
                'api': {
                    'schema': '/{base}/{version}/{baseSite}/{url}',
                    'protocol': 'http',
                    'host': 'localhost',
                    'port': '9001',
                    'version': 'v2',
                    'base': 'rest',
                    'retry': { 'times': 1, 'delay': 100 },
                    'mocked': true
                },
                'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
            },
            'server': {
                'host': '0.0.0.0',
                'port': '3100',
                'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
            }
        };

        const hybrisUserAuthProvider = new HybrisUserAuthProvider({ config: hybris.api });
        const hybrisStaticAuthProvider = new HybrisStaticAuthProvider({
            config: hybris.api,
            credentials: hybris.oauth
        });
        await this.registerModules([
            new AuthModule({
                strategy: {
                    type: 'cookie'
                },
                providers: [hybrisUserAuthProvider, hybrisStaticAuthProvider],
                token: { secret: server.credentials?.token?.secret },
                refresh: { secret: server.credentials?.token?.secret }
            }),
            new CorsModule(),
            new MockModule(),
            new PrometheusModule()
        ]);
    }

    public async initServices() {
        this.logSeparator();
        await this.registerServices([
            TranslationService,
            TokenService,
            RenderService,
            HybrisService,
            HybrisProxyService,
            ErrorService,
            CartService,
            OrderService,
            HybrisHttpService
        ]);
    }

    public async initRoutes() {
        this.logSeparator();
        await this.registerRoutes(ServerRoutes);
    }

    public async initControllers() {
        this.logSeparator();
        await this.registerControllers([
            DebugController,
            ResourceController,
            RenderController,
            HybrisProxyController,
            CartController,
            OrderController,
            BackendController
        ]);
    }

    public override async init() {
        this.logSeparator();
        await this.initServices();
        // await this.initConfig();
        await this.initServer();
        await this.initPlugins();
        await this.initControllers();
        await this.initModules();
        await this.initRoutes();
        await super.init();
    }

    public override async start() {
        try {
            await this.server.start();

            const config = {
                'general': { 'useSecureCookies': false, 'memwatch': { 'active': 0, 'print': '' } },
                'app': {
                    'api': {
                        'schema': '/{base}/{version}/{url}',
                        'protocol': null,
                        'host': null,
                        'port': null,
                        'base': 'api',
                        'version': 'v2',
                        'useMiddleware': true,
                        'retry': { 'times': 1, 'delay': 100 }
                    },
                    'logLevel': 'debug',
                    'fixLanguage': true,
                    'languageParamName': 'language',
                    'supportedLanguages': ['de', 'en'],
                    'defaultLanguage': 'de',
                    'canonicalBaseUrl': 'http://127.0.0.1:3100',
                    'route': { 'prefix': '/' },
                    'multiTestParam': { 'attr': '1234' },
                    'tenant': 'netconomy.net',
                    'environment': 'local',
                    'platform': 'shop',
                    'baseSite': 'netconomy.net',
                    'image': { 'protocol': 'http', 'host': '10.99.82.110', 'port': '3100' }
                },
                'hybris': {
                    'api': {
                        'schema': '/{base}/{version}/{baseSite}/{url}',
                        'protocol': 'http',
                        'host': 'localhost',
                        'port': '9001',
                        'version': 'v2',
                        'base': 'rest',
                        'retry': { 'times': 1, 'delay': 100 },
                        'mocked': true
                    },
                    'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                },
                'server': {
                    'host': '0.0.0.0',
                    'port': '3100',
                    'credentials': { 'token': { 'secret': 'zH4NRP1HMALxxCFnRZABFA7GOJtzU_gI' } },
                    'connection': { 'keepAliveTimeout': '4000', 'keepAliveMaxTimeout': '30000', 'pipelining': '1', 'rejectUnauthorized': false },
                    'git': { 'version': 'develop-SNAPSHOT', 'hash': '' }
                },
                'multi': {
                    'netconomy.net': { 'app': { 'tenant': 'netconomy.net', 'environment': 'local', 'platform': 'shop', 'baseSite': 'netconomy.net' } },
                    'electronics': {
                        'app': {
                            'api': { 'version': 'v3' },
                            'multiTestParam': { 'attr': '5678' },
                            'tenant': 'electronics',
                            'environment': 'local',
                            'platform': 'shop',
                            'baseSite': 'electronics'
                        },
                        'hybris': {
                            'api': {
                                'schema': '/{base}/{version}/{baseSite}/{url}',
                                'protocol': 'http',
                                'host': 'localhost',
                                'port': '9001',
                                'version': 'v3',
                                'base': 'rest',
                                'retry': { 'times': 1, 'delay': 100 },
                                'mocked': true
                            },
                            'oauth': { 'client_id': 'admin_static', 'client_secret': 'nimda' }
                        }
                    }
                }
            };
            const hybris = config.hybris;

            this.logServerStart(() => {
                Logger.info(`HYBRIS_API=${hybris.api.base}`);
                Logger.info(`HYBRIS_URL=${hybris.api.protocol}://${hybris.api.host}:${hybris.api.port}`);
            });
        } catch (error) {
            if (error) {
                Logger.error(error);
                throw error;
            }
        }
    }

    public override async stop() {
        await this.server.stop();
        this.logStopServer();
        process.exit(0);
    }

    public override async dispose() {
        await this.server.stop();
    }
}
