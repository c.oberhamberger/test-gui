/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import type { DefaultRouteConfig } from '@archibald/core';

import CartServerRouteConfig from 'shop/server/routes/cart';
import HybrisServerRouteConfig from 'shop/server/routes/hybris';
import OrderServerRouteConfig from 'shop/server/routes/order';
import ResourceServerRouteConfig from 'shop/server/routes/resource';
import SystemServerRouteConfig from 'shop/server/routes/system';

const ServerRouteConfig: DefaultRouteConfig[] = [
    ...SystemServerRouteConfig,
    ...ResourceServerRouteConfig,
    ...HybrisServerRouteConfig,
    ...CartServerRouteConfig,
    ...OrderServerRouteConfig
];

export default ServerRouteConfig;
