/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type DefaultRouteConfig, RouteMethod, CommonHelper } from '@archibald/core';

import Config from 'shop/client/config/config';

const routePrefix = CommonHelper.removeTrailingSlash(Config.get('app.route.prefix', ''));

const SystemServerRouteConfig: DefaultRouteConfig[] = [
    {
        method: RouteMethod.GET,
        raw: true,
        path: `${routePrefix}/`,
        handler: 'RenderController.index',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.GET,
        raw: true,
        path: `${routePrefix}/{language}`,
        handler: 'RenderController.index',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.GET,
        raw: true,
        path: `${routePrefix}/{language}/{path*}`,
        handler: 'RenderController.index',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.GET,
        raw: true,
        path: '/backend/alive',
        handler: 'BackendController.alive'
    },
    {
        method: RouteMethod.GET,
        raw: true,
        path: '/backend/version/api',
        handler: 'BackendController.version'
    }
];

export default SystemServerRouteConfig;
