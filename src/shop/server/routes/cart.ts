/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { type DefaultRouteConfig, RouteMethod } from '@archibald/core';

const CartServerRouteConfig: DefaultRouteConfig[] = [
    {
        method: RouteMethod.GET,
        path: 'users/{userId}/carts/{cartId}',
        handler: 'CartController.getCart',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.POST,
        path: 'users/{userId}/carts',
        handler: 'CartController.addToCart',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.POST,
        path: 'users/{userId}/carts/{cartId}/entries',
        handler: 'CartController.addToCart',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.PATCH,
        path: 'users/{userId}/carts/{cartId}/entries/{entryNumber}',
        handler: 'CartController.updateCart',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.DELETE,
        path: 'users/{userId}/carts/{cartId}/entries/{entryNumber}',
        handler: 'CartController.deleteFromCart',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    },
    {
        method: RouteMethod.POST,
        path: 'users/{userId}/carts/{cartId}/addresses/delivery',
        handler: 'CartController.createDeliveryAddress',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    }
];

export default CartServerRouteConfig;
