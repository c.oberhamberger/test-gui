/*********************************************************************
 * The Initial Developer of the content of this file is NETCONOMY.
 * All portions of the code written by NETCONOMY are property of
 * NETCONOMY. All Rights Reserved.
 *
 * NETCONOMY Software & Consulting GmbH
 * Hilmgasse 4, A-8010 Graz (Austria)
 * FN 204360 f, Landesgericht fuer ZRS Graz
 * Tel: +43 (316) 815 544
 * Fax: +43 (316) 815544-99
 * www.netconomy.net
 *
 * (c) 2019 by NETCONOMY Software & Consulting GmbH
 ********************************************************************/
import { RouteMethod, type DefaultRouteConfig } from '@archibald/core';

const HybrisServerRouteConfig: DefaultRouteConfig[] = [
    {
        method: [RouteMethod.GET, RouteMethod.PUT, RouteMethod.POST, RouteMethod.DELETE],
        path: '{url*}',
        handler: 'HybrisProxyController.proxy',
        options: {
            auth: {
                strategy: 'jwt',
                mode: 'try'
            }
        }
    }
];

export default HybrisServerRouteConfig;
